<?php
// +----------------------------------------------------------------------
// | Swoole客户端
// +----------------------------------------------------------------------

namespace helper;

class SwooleClient
{
    private $client;

    public function __construct()
    {

    }

    public function coonect($host = '127.0.0.1', $port = '9501', $timeout = 1)
    {
        $client = new \Swoole\Client(SWOOLE_SOCK_TCP);
        if (!$client->connect($host, $port, $timeout)) {
            throw new \Exception("connect failed. Error: {$client->errCode}\n");
        }
        $this->client = $client;
        return $this;
    }

    /**
     * 同步阻塞客户端
     * @param $data
     * @return $this
     * @throws \Exception
     */
    public function send($data)
    {
        $this->client->send($data);
        return $this;
    }

    public function recv()
    {
        return $this->client->recv();
    }

    public function __destroy()
    {
        $this->client->close();
    }
}