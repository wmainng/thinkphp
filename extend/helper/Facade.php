<?php

namespace helper;

use helper\facade\IMap;
use helper\facade\IOcr;
use helper\facade\ISms;

class Facade
{
    /**
     * @var array 缓存的实例
     */
    private static array $instance = [];

    /**
     * 静态工厂模式
     * @param string $name  类名或文件名\类名
     * @param array $config 配置
     * @return mixed
     */
    public static function factory(string $name = '', array $config = [])
    {
        $name  = str_replace('/', '\\', $name);
        if (!isset(self::$instance[$name])) {
            $class = '\\helper\\' . $name;
            self::$instance[$name] = new $class($config);
        }
        return self::$instance[$name];
    }

    /**
     * 地图
     * @param string $name
     * @param array $config
     * @return IMap
     */
    public static function map(string $name = '', array $config = []): IMap
    {
        return self::factory($name . '\\Map', $config);
    }

    /**
     * OCR
     * @param string $name
     * @param array $config
     * @return IOcr
     */
    public static function ocr(string $name = '', array $config = []): IOcr
    {
        return self::factory($name . '\\Ocr', $config);
    }

    /**
     * 短信
     * @param string $name
     * @param array $config
     * @return ISms
     */
    public static function sms(string $name = '', array $config = []): ISms
    {
        return self::factory($name . '\\Sms', $config);
    }
}
