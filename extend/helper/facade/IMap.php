<?php
// +----------------------------------------------------------------------
// | 地图
// +----------------------------------------------------------------------

namespace helper\facade;

interface IMap
{
    /**
     * 地理编码
     * @param string $address
     * @return mixed
     */
    public function getLocation(string $address);
    /**
     * 逆地理编码
     * @param string $location 经纬度坐标
     * @param string $batch 是否批量
     * @return mixed
     */
    public function getAddress(string $location, string $batch = 'false');

    /**
     * 关键词输入提升
     * @param string $region  城市
     * @param string $keyword 关键词
     * @return mixed
     */
    public function getSuggestion(string $region, string $keyword);
}
