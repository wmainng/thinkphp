<?php
// +----------------------------------------------------------------------
// | 短信
// +----------------------------------------------------------------------

namespace helper\facade;

interface ISms
{
    /**
     * 短信发送
     * @param $mobile string
     * @param $templateCode string
     * @param $templateParam array
     * @return array
     */
    public function sendSms(string $mobile, string $templateCode, array $templateParam): array;
}
