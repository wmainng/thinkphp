<?php

namespace helper\facade;

interface IOcr
{
    /**
     * 通用文字识别
     * @param string $image
     * @param int $type 1:标准版,2:标准含位置版,3:高精度版,4:高精度含位置版
     * @return array
     */
    public function general(string $image, int $type = 1): array;

    /**
     * 身份证识别 阿里云计算有限公司
     * @param string $image image: 图片 默认本地图片
     * @param string $side 身份证正反面类型:face/back
     * @return array
     */
    public function idCard(string $image, string $side = 'face'): array;

    /**
     * 营业执照识别 阿里云计算有限公司
     * @param string $image image 营业执照照片
     * @return array
     */
    public function businessLicense(string $image): array;
}
