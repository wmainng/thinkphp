<?php

namespace helper\facade;

/**
 * 支付基础类
 */
interface IPayment
{
    /**
     * APP支付
     * @param string $subject      商品描述
     * @param string $out_trade_no 商户订单号
     * @param float $total_amount 商品金额
     * @return mixed
     */
    public function app(string $subject, string $out_trade_no, float $total_amount);

    /**
     * 企业转账
     * @param string $out_trade_no 订单号
     * @param float $amount       金额
     * @param string $account      账号
     * @param string $name         姓名
     * @param string $remark       备注
     * @return array
     */
    public function transfer(string $out_trade_no, float $amount, string $account, string $name, string $remark): array;

    /**
     * 异步通知验签
     * @param array $post
     * @return array
     */
    public function verifyNotify(array $post): array;

    /**
     * 退款
     * @param string $out_trade_no
     * @param $refund_amount
     * @param $total_amount
     * @return bool
     */
    public function refund(string $out_trade_no, $refund_amount, $total_amount): bool;

    /**
     * 查询退款
     * @param string $out_trade_no out_trade_no
     * @return array
     */
    public function refund_query(string $out_trade_no): array;
}
