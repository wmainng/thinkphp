<?php

namespace helper\jd\sdk;

class ConfigUtil {
	public static function get_val_by_key($key) {
		$settings = new SettingsINI();
		$settings->load ( __DIR__ . '/../config/config.ini' );
		return $settings->get ( "wepay." . $key );
	}
	public static function get_trade_num() {
		return ConfigUtil::get_val_by_key ( 'merchantNum' ) . ConfigUtil::getMillisecond ();
	}
	public static function getMillisecond() {
		list ( $s1, $s2 ) = explode ( ' ', microtime () );
		return ( float ) sprintf ( '%.0f', (floatval ( $s1 ) + floatval ( $s2 )) * 1000 );
	}

    function load($file)
    {
        if (file_exists($file)) {
            $this->_settings = parse_ini_file($file, true);
        }
    }
}

//echo ConfigUtil::get_val_by_key("merchantNum");
//echo ConfigUtil::get_trade_num();

?>