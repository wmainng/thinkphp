<?php

namespace helper\jd\sdk;

class SettingsINI extends Settings
{
    function load($file)
    {
        if (file_exists($file)) {
            $this->_settings = parse_ini_file($file, true);
        }
    }
}