<?php

namespace helper\jd;

class Pays
{
    private array $config;

    private string $host = "https://openapi.duolabao.com/";

    public function __construct($config)
    {
        $this->config = [
            //'merchant' => $config['merchant'],
            'notify_url' => $config['notify_url']
        ];
    }

    //公钥(accessKey):31c823518fe044238f2e1dd4e85c9713023e8fc0
    //私钥(secretKey):d12e1d0f4c3e45ee9ed25e83359346fccd853027
    public function pay()
    {
        $path = "v3/order/pay/create";
        $body = [
            "customerNum" => "10001114504252653551690",
            "shopNum" => "10001214504254906302068",
            "requestNum" => "201903141611435c8a0cbf50a3e2250",
            "amount" => 0.1,
            "bankType" => "WX",
            "authId" => "ooxj_s5XqeNjO5aruFLHneMMGB8s",
            "callbackUrl" => "http://webtestm.laiu8.cn/paytest/dlbNotify",
            "extendParams" => "{\"food_order_type\":\"qr_order\"}",
            "subOrderType" => "LEDGER",
            "ledgerRule" => [
                "ledgerType" => "FIXED",
                "ledgerFeeAssume" => "RECEIVER",
                "list" => [
                    ["customerNum"=>"10001115199597283447316", "amount"=>"0.01"],
                    ["customerNum"=>"10001114504252653551690", "amount"=>"0.01"]
                ]
            ],
            "goodsInfo" => '{"detail":[{"id": 1, "name": "单品营销", "price": 100, "quantity": 1}],"cost_price":200,"receipt_id":"wx1234"}'
        ];
    }

    /**
     * 签名
     * @return string
     */
    private function signature()
    {
        //secretKey（私钥）+ timestamp（时间戳）+ 请求路径（不需要拼接域名）+ body（请求参数）
        $arr = [
            'secretKey' => $this->config['secretKey'],
            'timestamp' => '',
            'path' => '',
            'body' => ''
        ];
        $str = http_build_query($arr);
        return sha1($str);
    }

    /**
     * 请求头
     * @return array
     */
    private function header()
    {
        $headers   = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "secretKey: " . $this->config['secretKey'];
        $headers[] = "timestamp: " . time();
        $headers[] = "token: " . $this->config['accesskey'];
        return $headers;
    }
}