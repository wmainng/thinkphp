<?php
// +----------------------------------------------------------------------
// | 阿里云市场
// | 卡证 1) 500 0.01 2) 10000 702.80
// +----------------------------------------------------------------------
// | Copyright (c) 2018-2022 wmainng All rights reserved.
// +----------------------------------------------------------------------
// | Author: wmainng <wmainng@qq.com>
// +----------------------------------------------------------------------

namespace helper\ali;

use helper\Client;
use helper\facade\IOcr;

class Ocr implements IOcr
{
    private array $host = [
        'general'   => 'https://tysbgpu.market.alicloudapi.com',
        'business_license' => 'https://bizlicense.market.alicloudapi.com',
        'idcard' => 'https://cardnumber.market.alicloudapi.com',
        'card_pack' => 'https://cardpack.market.alicloudapi.com',
    ];

    /**
     * @var array
     */
    private array $config;

    /**
     * @var Client
     */
    protected Client $client;

    /**
     * 初始化
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->client = new Client();
    }

    /**
     * 通用文字识别 阿里云计算有限公司
     * @param string $image image: 图片 默认本地图片
     * int $type 1:标准版,2:标准含位置版,3:高精度版,4:高精度含位置版
     * @return array
     */
    public function general(string $image, int $type = 1): array
    {
        $path = "/api/predict/ocr_general";
        $image = $this->client->file($image);
        $body = "{\"image\":\"$image\",\"configure\":{\"min_size\":16,\"output_prob\":true,\"output_keypoints\":false,
                \"skip_detection\":false,\"without_predicting_direction\":false}}";
        $response = $this->client->post($this->host['general'] . $path, ['headers' => $this->header(), 'body' => $body]);
        if ($response->getStatusCode() != 200) {
            return [];
        }
        $res = $response->getBody();
        if (isset($res['success'])) {
            $word = array_column($res['ret'],  'word');
            $res['content'] = implode("\n", $word);
        }
        return $res;
    }

    /**
     * 身份证识别 阿里云计算有限公司
     * @param string $image image: 图片 默认本地图片
     * @param string $side 身份证正反面类型:face/back
     * @return array
     */
    public function idCard(string $image, string $side = 'face'): array
    {
        $path = "/rest/160601/ocr/ocr_idcard.json";
        $image = $this->client->file($image);
        $body = "{\"image\":\"$image\",\"configure\":{\"side\":\"$side\"}}";
        $response = $this->client->post($this->host['card_pack'] . $path, ['headers' => $this->header(), 'body' => $body]);
        if ($response->getStatusCode() != 200) {
            return [];
        }
        return $response->getBody();
    }

    /**
     * 营业执照识别 阿里云计算有限公司
     * @param string $image image 营业执照照片
     * @return array
     */
    public function businessLicense(string $image): array
    {
        $path  = "/rest/160601/ocr/ocr_business_license.json";
        $image = $this->client->file($image);
        $body  = "{\"image\":\"$image\"}";
        $response = $this->client->post($this->host['card_pack'] . $path, ['headers' => $this->header(), 'body' => $body]);
        if ($response->getStatusCode() != 200) {
            return [];
        }
        return $response->getBody();
    }

    /**
     * 银行卡识别 阿里云计算有限公司
     * @param string $image 图片二进制数据的base64编码或者图片url
     * @return array
     */
    public function bankCard(string $image): array
    {
        $path  = "/rest/160601/ocr/ocr_bank_card.json";
        $image = $this->client->file($image);
        $body  = "{\"image\":\"$image\",\"configure\":\"{\"card_type\":true}\"}";
        $response = $this->client->post($this->host['card_pack'] . $path, ['headers' => $this->header(), 'body' => $body]);
        if ($response->getStatusCode() != 200) {
            return [];
        }
        return $response->getBody();
    }

    /**
     * 驾驶证识别 阿里云计算有限公司
     * @param string $image 图片二进制数据的base64编码或者图片url
     * @param string $side 首页/副页:face/back
     * @return array
     */
    public function driverLicense(string $image, string $side = 'face'): array
    {
        $path  = "/rest/160601/ocr/ocr_driver_license.json";
        $image = $this->client->file($image);
        $body  = "{\"image\":\"$image\",\"configure\":{\"side\":\"$side\"}}";
        $response = $this->client->post($this->host['card_pack'] . $path, ['headers' => $this->header(), 'body' => $body]);
        if ($response->getStatusCode() != 200) {
            return [];
        }
        return $response->getBody();
    }

    /**
     * 行驶证识别 阿里云计算有限公司
     * @param string $image 图片二进制数据的base64编码或者图片url
     * @param string $side 正反面类型face/back
     * @return array
     */
    public function vehicle(string $image, string $side = 'face'): array
    {
        $path  = "/rest/160601/ocr/ocr_vehicle.json";
        $image = $this->client->file($image);
        $body  = "{\"image\":\"$image\",\"configure\":{\"side\":\"$side\"}}";
        $response = $this->client->post($this->host['card_pack'] . $path, ['headers' => $this->header(), 'body' => $body]);
        if ($response->getStatusCode() != 200) {
            return [];
        }
        return $response->getBody();
    }

    /**
     * 企业工商信息 艾科瑞特科技
     */
    public function businessLicenses($string)
    {
        //API产品路径
        $host = "http://enterprise.market.alicloudapi.com";
        $path = "/ai_market/ai_enterprise_knowledge/enterprise_simple/v1";
        $method = "GET";
        //参数配置
        //用户自定义标识符，如：企业名称/企业统一社会信用代码
        $querys = "STRING=" . urlencode("$string");
        $bodys = "";
        $url = $host . $path . "?" . $querys;
        $response = $this->client->get($url);
        if ($response->getStatusCode() != 200) {
            return [];
        }
        return $response->getBody();
    }

    /**
     * 头部
     * @return array
     */
    private function header()
    {
        $headers   = array();
        $headers[] = "Authorization:APPCODE " . $this->config['appCode'];
        $headers[] = "Content-Type:application/json; charset=UTF-8";
        return $headers;
    }
}
