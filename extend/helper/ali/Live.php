<?php
// +----------------------------------------------------------------------
// | 视频直播
// +----------------------------------------------------------------------
namespace helper\ali;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;

class Live
{
    /**
     * @var array|string[]
     */
    private array $config = [
        'accessKeyId' => '',
        'accessKeySecret' => '',
        'push_cdn' => '',
        'push_key' => '',
        'play_cdn' => '',
        'play_key' => ''
    ];

    public function __construct($config)
    {
        $this->config['accessKeyId'] = $config['accessKeyId'] ?? '';
        $this->config['accessKeySecret'] = $config['accessKeySecret'] ?? '';
        $this->config['push_cdn'] = $config['push_cdn'] ?? '';
        $this->config['push_key'] = $config['push_key'] ?? '';
        $this->config['play_cdn'] = $config['play_cdn'] ?? '';
        $this->config['play_key'] = $config['play_key'] ?? '';
        if (isset($config['accessKeyId'])) {
            try {
                AlibabaCloud::accessKeyClient($config['accessKeyId'], $config['accessKeySecret'])->asDefaultClient();
            } catch (ClientException $e) {

            }
        }
    }

    /**
     * 地址生成器
     * @param string $appName app名称
     * @param string $streamName 直播流名称
     * @param string $time 过期时间
     * @return array
     */
    public function urlBuilder(string $appName = 'AppName', string $streamName = 'StreamName', string $time = ''): array
    {
        $push_cdn = $this->config['push_cdn'];
        $push_key = $this->config['push_key']; //推流主key
        $play_cdn = $this->config['play_cdn'];
        $play_key = $this->config['play_key']; //播流主key

        $timestamp   = $time ?? time() + 1800;   //生成鉴权URL的时间
        $rand = 0;// 随机数
        $uid = 0;// 附加参数

        $md5hash = md5("/{$appName}/{$streamName}-{$timestamp}-$rand-$uid-{$push_key}");
        $auth_key = $timestamp . '-' . $rand . '-' . $uid . '-' . $md5hash;

        // 推流
        $push['rtmp'] = "rtmp://{$push_cdn}/{$appName}/{$streamName}?auth_key={$auth_key}";
        $push['rts'] = "artc://{$push_cdn}/{$appName}/{$streamName}?auth_key={$auth_key}";

        // 播流
        $auth_str = $timestamp . '-' . $rand . '-' . $uid . '-' . $play_key;
        $rtmp_md5  = md5("/{$appName}/{$streamName}-$auth_str");
        $flv_md5   = md5("/{$appName}/{$streamName}.flv-$auth_str");
        $m3u8_md5  = md5("/{$appName}/{$streamName}.m3u8-$auth_str");
        // rtmp 要装flash
        $play['rtmp'] = "rtmp://{$play_cdn}/{$appName}/{$streamName}?auth_key={$timestamp}-$rand-$uid-$rtmp_md5";
        // flv  电脑
        $play['flv'] = "http://{$play_cdn}/{$appName}/{$streamName}.flv?auth_key={$timestamp}-$rand-$uid-$flv_md5";
        // m3u8 电脑，手机
        $play['m3u8'] = "http://{$play_cdn}/{$appName}/{$streamName}.m3u8?auth_key={$timestamp}-$rand-$uid-$m3u8_md5";
        $play['rts'] = "artc://{$play_cdn}/{$appName}/{$streamName}?auth_key={$timestamp}-$rand-$uid-$rtmp_md5";
        return [
            'push' => $push['rtmp'],
            'play' => $play['m3u8']
        ];
    }

    public function describeCdnService()
    {
        try {
            $result = AlibabaCloud::rpc()
                ->product('live')
                ->version('2020-05-28')
                ->action('DescribeCdnService')
                ->method('POST')
                ->request();

            print_r($result->toArray());

        } catch (ClientException|ServerException $exception) {
            print_r($exception->getErrorMessage());
        }
    }
}
