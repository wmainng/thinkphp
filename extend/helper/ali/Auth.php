<?php
// +----------------------------------------------------------------------
// | 实人认证服务
// +----------------------------------------------------------------------
// | Copyright (c) 2018-2022 wmainng All rights reserved.
// +----------------------------------------------------------------------
// | Author: wmainng <wmainng@qq.com>
// +----------------------------------------------------------------------

namespace helper\ali;

use AlibabaCloud\SDK\Cloudauth\V20200618\Cloudauth;
use AlibabaCloud\SDK\Cloudauth\V20200618\Models\DescribeSmartVerifyRequest;
use Darabonba\OpenApi\Models\Config;
use helper\alibaba\VerifyMaterialRequest;

class Auth
{
    /**
     * 实人认证
     * @param string $faceImageUrl 人脸照
     * @param string $name 姓名
     * @param string $idCardNumber 身份证号码
     * @return array
     */
    public function verifyMaterial(string $faceImageUrl, string $name, string $idCardNumber): array
    {
        $verifyMaterialRequest = new VerifyMaterialRequest([
            'faceImageUrl'        => $faceImageUrl,
            'bizType'             => 'verifyMaterial',
            'bizId'               => date('YmdHis'),
            'name'                => $name,
            'idCardNumber'        => $idCardNumber,
        ]);
        $response = self::createClient('cloudauth')->verifyMaterial($verifyMaterialRequest);
        if ($response->body->verifyStatus == 1) {
            return ['code' => 0, 'message' => $response->body->material];
        }
        return ['code' => 1, 'message' => $response->body];
    }
}