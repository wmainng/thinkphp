<?php
// +----------------------------------------------------------------------
// | 淘宝客API
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2020 http://www.taoshangapp.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: wmainng
// +----------------------------------------------------------------------
namespace plugins\taobao\service;
use think\Db;

class Taobao
{
    /**
     * 获取配置
     */
    private function config()
    {
        $result = Db::name('plugin')->field('config')->where('name', 'Taobao')->find();
        $config = json_decode($result['config'], true);
        return $config;
    }

    /**
     * 淘宝客-推广者-选品库宝贝列表
     */
    public function getFavorites()
    {
        include('plugins/taobao/lib/TopSdk.php');
        $c = new \TopClient;
        $config = $this->config();
        $c->appkey = $config['AppKey'];
        $c->secretKey = $config['AppSecret'];
        $req = new \TbkUatmFavoritesGetRequest;
        $req->setPageNo("1");
        $req->setPageSize("20");
        $req->setFields("favorites_title,favorites_id,type");
        $req->setType("1");
        $resp = $c->execute($req);
        $favorites = object_array($resp->results);
        return $favorites['tbk_favorites'];
    }

    /**
     * 淘宝客-推广者-选品库宝贝信息
     * @param $param array
     * @return array
     */
    public function getFavoritesItem($param)
    {
        include('plugins/taobao/lib/TopSdk.php');
        $c = new \TopClient;
        $config = $this->config();
        $c->appkey = $config['AppKey'];
        $c->secretKey = $config['AppSecret'];
        $req = new \TbkUatmFavoritesItemGetRequest;
        $param['page'] = !empty($param['page']) ? $param['page'] : 1;
        $param['size'] = !empty($param['size']) ? $param['size'] : 20;
        $req->setPlatform("1");//链接形式：1：PC，2：无线，默认：１
        $req->setPageSize($param['size']);//页大小，默认20，1~100
        $req->setAdzoneId($config['adzone_id']);//推广位id
        $req->setUnid($config['unid']);//自定义输入串
        $req->setFavoritesId($param['favorites_id']);//选品库的id 19720279
        $req->setPageNo($param['page']);//第几页，默认：1，从1开始计数
        //商品ID,商品标题,商品主图,商品小图列表,商品一口价格,商品折扣价格,卖家类型,淘客地址,商品优惠券推广链接,优惠券面额,优惠券结束时间
        $req->setFields("num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,click_url,coupon_click_url,coupon_info,coupon_end_time");
        $resp = $c->execute($req);
        return object_array($resp);
    }

    /**
     * 淘宝客-公用-长链转短链
     */
    public function getSpread()
    {
        include('plugins/taobao/lib/TopSdk.php');
        $c = new \TopClient;
        $config = $this->getPlugin()->getConfig();
        $c->appkey = $config['AppKey'];
        $c->secretKey = $config['AppSecret'];
        $req = new \TbkSpreadGetRequest;
        $requests = new \TbkSpreadRequest;
        $requests->url = "http://temai.taobao.com";
        $req->setRequests(json_encode($requests));
        $resp = $c->execute($req);

    }

    /**
     * 获取商品详情
     * http://www.99api.com 1.50元/100次
     * @param $itemId string 淘宝的商品id
     * @return string
     */
    public function getItemDesc($itemId)
    {
        $method = "GET";
        $url = "http://api01.6bqb.com/taobao/detail?apikey=9EE029B7175B9F9507B6E275B8E971CE&itemid={$itemId}";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_ENCODING, "gzip");
        $result = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($result, true);
        return $result['retcode'] == "0000" ? str_replace("//","https://",$result['data']['item']['desc']) : '';
    }

    /**
     * 获取商品评论
     * http://www.99api.com 2.00元/100次
     * @param $param array
     * @return array
     */
    public function getComment($param)
    {
        $method = "GET";
        $param['page'] = !empty($param['page']) ? intval($param['page']) : 1;//分页
        $param['sort'] = !empty($param['sort']) ? intval($param['sort']) : 10;//筛选10[全部],-1[差评],0[中评],1[好评],2[追评]
        $url = "http://api01.6bqb.com/taobao/comment?apikey=9EE029B7175B9F9507B6E275B8E971CE&itemId={$param['num_iid']}&page={$param['page']}&sort={$param['sort']}";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_ENCODING, "gzip");
        $results = curl_exec($curl);
        curl_close($curl);
        $results = json_decode($results, true);
        if ($results['retcode'] == '0000') {
            $data['list'] = $results['data'];
            $data['page'] = $results['page'];
            $data['hasNext'] = $results['hasNext'];
        } else {
            $data = [];
        }
        return json_encode($data);
    }
}

/**
 * SimpleXMLElement Object转为数组
 * @param $array object
 * @return array
*/
function object_array($array)
{
    //对象直接转换
    if (is_object($array)) {
        $array = (array)$array;
    }
    //数组遍历转换
    if (is_array($array)) {
        foreach ($array as $key => $value) {
            $array[$key] = object_array($value);
        }
    }
    return $array;
}