<?php
// +----------------------------------------------------------------------
// | 阿里推送
// +----------------------------------------------------------------------

namespace helper\ali;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;

// Download：https://github.com/aliyun/openapi-sdk-php
// Usage：https://github.com/aliyun/openapi-sdk-php/blob/master/README.md
class Push
{
    public static function request()
    {

        AlibabaCloud::accessKeyClient('<accessKeyId>', '<accessSecret>')
            ->regionId('cn-hangzhou')
            ->asDefaultClient();
        try {
            $result = AlibabaCloud::rpc()
                ->product('Push')
                // ->scheme('https') // https | http
                ->version('2016-08-01')
                ->action('Push')
                ->method('POST')
                ->host('cloudpush.aliyuncs.com')
                ->options([
                    'query' => [
                        'RegionId' => "cn-hangzhou",
                        'AppKey' => "appKey",
                        'PushType' => "NOTICE",
                        'DeviceType' => "ALL",
                        'Target' => "DEVICE",
                        'TargetValue' => "deviceIds",
                        'Body' => "ALi Push Title",
                        'Title' => "Ali Push Body",
                    ],
                ])
                ->request();
            print_r($result->toArray());
        } catch (ClientException $e) {
            echo $e->getErrorMessage() . PHP_EOL;
        } catch (ServerException $e) {
            echo $e->getErrorMessage() . PHP_EOL;
        }
    }
}
