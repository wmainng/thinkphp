<?php
// +----------------------------------------------------------------------
// | 百度云
// +----------------------------------------------------------------------

namespace helper\storage\driver;

use helper\storage\Driver;
use BaiduBce\BceClientConfigOptions;
use BaiduBce\Util\MimeTypes;
use BaiduBce\Http\HttpHeaders;
use BaiduBce\Services\Bos\BosClient;

class Bos implements Driver
{
    private $config;

    private $handler;

    public function __construct($config)
    {
        $this->config  = $config;
        $this->handler = new BosClient([
            'credentials' => array(
                'accessKeyId' => $config['accessKeyId'],
                'secretAccessKey' => $config['secretAccessKey'],
                'sessionToken' => ''
            ),
            'endpoint' => $config['endpoint'],
            'stsEndpoint' => '',
        ]);
    }

    /**
     * 上传文件
     * @param string $object 文件名称
     * @param string $filePath 文件路径
     * @return array
     */
    public function putFile(string $object, string $filePath): array
    {
        try {
            $filePath = $filePath . '/' . $object;
            $fileSize = filesize($filePath);
            if ($fileSize < 20 * 1024 * 1024) {
                $result = $this->handler->putObjectFromFile($this->config['bucket'], $object, $filePath);
            } else {
                // 初始化Multipart Upload
                $response = $this->handler->initiateMultipartUpload($this->config['bucket'], $object);
                $uploadId = $response->uploadId;

                // 上传分块
                //设置分块的开始偏移位置
                $offset = 0;
                $partNumber = 1;
                //设置每块为5MB
                $partSize = 5 * 1024 * 1024;
                $length = $partSize;
                $partList = array();
                $bytesLeft = $fileSize;

                //分块上传
                while ($bytesLeft > 0) {
                    $length = ($length > $bytesLeft) ? $bytesLeft : $length;
                    $response = $this->handler->uploadPartFromFile($this->config['bucket'], $object, $uploadId,  $partNumber, $filePath, $offset, $length);
                    array_push($partList, array("partNumber"=>$partNumber, "eTag"=>$response->metadata["etag"]));
                    $offset += $length;
                    $partNumber++;
                    $bytesLeft -= $length;
                }

                // 完成分块上传
                $response = $this->handler->completeMultipartUpload($this->config['bucket'], $object, $uploadId, $partList);
                $result = $response->location;
            }
            return ['code' => 0, 'msg'=> '', 'data' => $result];
        } catch (\Exception $e) {
            return ['code' => 1, 'msg'=> $e->getMessage()];
        }
    }

    /**
     * 删除文件
     * @param string $object 文件名称
     * @return array
     */
    public function deleteFile(string $object): array
    {
        try {
            $result = $this->handler->deleteObject($this->config['bucket'], $object);
            return ['code' => 0, 'msg'=> '', 'data' => $result];
        } catch (\Exception $e) {
            return ['code' => 1, 'msg'=> $e->getMessage()];
        }
    }
}