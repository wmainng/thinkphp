<?php

namespace helper\storage;

/**
 * 存储基础类
 */
interface Driver
{
    /**
     * 上传文件
     * @param string $object 文件名称
     * @param string $filePath 文件路径
     * @return array
     */
    public function putFile(string $object, string $filePath): array;

    /**
     * 删除文件
     * @param string $object 文件名称
     * @return array
     */
    public function deleteFile(string $object): array;

    /**
     * 授权密钥
     * @return mixed
     */
    public function sts();
}