<?php

namespace helper\getui;

interface Driver
{
    /**
     * request_id
     * @param $request_id
     */
    public function setRequestId($request_id);

    /**
     * 消息离线时间
     * @param int $ttl
     */
    public function setSettings(int $ttl = 3600000);

    /**
     * 任务组名
     * @param string $group_name
     */
    public function setGroupName(string $group_name);

    /**
     * 推送目标用户
     * @param string $tag
     */
    public function setAudience(string $tag = '');

    /**
     * cid
     * @param array $cid
     */
    public function setCid(array $cid);

    /**
     * 标题
     * @param string $title
     */
    public function setTitle(string $title);

    /**
     * 内容
     * @param string $body
     */
    public function setBody(string $body);

    /**
     * @param string $key
     * @param string $value
     */
    public function setClickType(string $key = 'payload', string $value = '');

    /**
     * 推送消息参数
     */
    public function setPushMessage();

    /**
     * 厂商推送消息参数
     */
    public function setPushChannel();

    /**
     * 单推
     * @return mixed
     */
    public function single();

    /**
     * 批量推
     * @return mixed
     */
    public function list();

    /**
     * 群推
     * @return mixed
     */
    public function app();
}
