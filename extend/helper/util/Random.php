<?php
// +----------------------------------------------------------------------
// | 随机数工具库
// +----------------------------------------------------------------------

namespace helper\util;

class Random
{
    /**
     * 返回一个随机的 int值，该值介于 [0,n)
     * @param int $n
     * @return int
     */
    public static function nextInt(int $n): int
    {
        return random_int(0, $n);
    }

    /**
     * 返回一个随机浮点型数字
     * @param int $n
     * @return float
     */
    public static function nextFloat(int $n): float
    {
        return mt_rand()/mt_getrandmax() * $n;
    }

    /**
     * 返回一个随机字符串
     * @param int $length
     * @return string
     */
    public static function nextString(int $length = 10): string
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $chars = str_shuffle($chars);
        return substr($chars, 0, $length);
    }

    /**
     * 返回一个随机的 boolean 值
     * @return bool
     */
    public static function nextBoolean(): bool
    {
        $chars = [true, false];
        return $chars[mt_rand(0, 1)];
    }
}