<?php

namespace helper\util;

class System
{
    /**
     * 统计程序运行区间的时间内存情况
     * @param array $param
     * @return array
     */
    function getRangeTime($param = [])
    {
        return [
            'time' => isset($param['time']) ? round((microtime(true) - $param['time']), 3) . 's' : microtime(true),
            'memory' => isset($param['memory']) ? round((memory_get_usage() - $param['memory']) / 1024, 2) . 'KB' : memory_get_usage()
        ];
    }
}