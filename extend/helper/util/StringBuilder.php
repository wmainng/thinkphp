<?php
// +----------------------------------------------------------------------
// | PHP实现 Java StringBuffer 和 StringBuilder 类
// | 当对字符串进行修改的时候，需要使用 StringBuffer 和 StringBuilder 类。
// | 和 String 类不同的是，StringBuffer 和 StringBuilder 类的对象能够被多次的修改，并且不产生新的未使用对象。
// +----------------------------------------------------------------------

namespace helper\util;

class StringBuilder
{
    private array $value = [];

    private int $count;

    /**
     * @param int|string $value
     */
    public function __construct($value = null)
    {
        if (is_string($value)) {
            $this->append($value);
        }
    }

    /**
     * 将指定的字符串追加到此字符序列
     * @param string|int $s
     * @return StringBuilder
     */
    public function append($s): StringBuilder
    {
        $arrays = is_int($s) ? [$s] : str_split($s);
        $this->value = array_merge($this->value, $arrays);
        return $this;
    }

    /**
     * 将 str 参数的字符串插入此序列中
     * @param int $offset
     * @param string|int $str
     * @return void
     */
    public function insert(int $offset, $str)
    {
        $replacement = is_int($str) ? [$str] : str_split($str);
        array_splice($this->value, $offset, 0, $replacement);
    }

    /**
     * 使用给定 String 中的字符替换此序列的子字符串中的字符
     * @param int $start
     * @param int $end
     * @param string|int $str
     * @return void
     */
    public function replace(int $start, int $end, $str)
    {
        $replacement = is_int($str) ? [$str] : str_split($str);
        array_splice($this->value, $start, $end - $start, $replacement);
    }

    /**
     * 移除此序列的子字符串中的字符
     * @param int $start
     * @param int $end
     * @return void
     */
    public function delete(int $start, int $end)
    {
        array_splice($this->value,$start,$end - $start);
    }

    /**
     * 返回长度（字符数）
     * @return void
     */
    public function length(): int
    {
        return count($this->value);
    }

    /**
     * 返回此序列中数据的字符串表示形式
     * @return String
     */
    public function toString(): String
    {
        return implode($this->value);
    }
}