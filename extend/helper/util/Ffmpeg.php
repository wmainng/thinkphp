<?php

namespace helper\util;

class Ffmpeg
{
    /**
     * 音频文件格式转换
     * 1)服务器安装ffmpeg
     * 2)php.ini中disable_functions 中把 exec 函数给禁用了，需要将其在PHP.ini 中删去，并重启php服务或者集成环境
     * @param $filepath string 文件路径
     * @param $to string
     * @return string
     */
    private function convert(string $filepath, string $to = 'pcm')
    {
        $pathInfo = pathinfo($filepath);
        $dirname = $pathInfo['dirname'];//文件路径
        $filename = $pathInfo['filename'];//文件名
        $extension = $pathInfo['extension'];//文件后缀
        //如果现在的扩展名与转换后扩展名一样，则直接返回
        if (!file_exists($filepath)) {
            return $filepath;
        }
        if ($extension == $to || !in_array($extension, ['pcm', 'wav', 'amr', 'mp3'])) {
            return $filepath;
        }
        $new_name = "$dirname/$filename.$to";
        //要执行的cmd命令
        if ($to == 'pcm') {
            $cmd = "ffmpeg -i $filepath -acodec pcm_s16le -f s16le -ac 1 -ar 16000 $new_name";//pcm
        } elseif ($to == 'wav') {
            $cmd = "ffmpeg -i $filepath -f wav $new_name";//wav
        } elseif ($to == 'amr') {
            $cmd = "ffmpeg -i $filepath -ac 1 -ar 16000 $new_name";//amr
        } else {
            $cmd = "ffmpeg -i $filepath $new_name";
        }
        //执行cmd命令
        $info = null;
        $status = null;
        exec($cmd, $info, $status);
        sleep(1);
        chmod($new_name, 0777);

        //删除接收到的文件
        //unlink($filepath);
        return $new_name;
    }
}