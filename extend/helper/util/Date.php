<?php

namespace helper\util;

trait Date
{
    /**
     * 获取毫秒数
     * @return string
     */
    function getMicroTime(): string
    {
        list($usec, $sec) = explode(" ", microtime());
        return $sec . substr($usec, 2, 3);
    }

    function microtime_float(): float
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * 日期差
     * 例如:2007-2-5 ~ 2007-3-6 的日期差数
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    function dateday($begin, $end)
    {
        return (strtotime($begin) - strtotime($end)) / (24 * 3600);
    }

    /**
     * 计算年龄
     * @param string $birth 日期 如1981-1-1
     * @return integer
     */
    function age(string $birth): int
    {
        list($b_year, $b_month, $b_day) = explode('-', $birth);
        $age   = date('Y') - $b_year - 1;
        $month = date('n');
        $day   = date('j');
        if ($month > $b_month || $month == $b_month && $day > $b_day) $age++;
        return $age;
    }

    /**
     * 今天开始的Y-m-d H:i:s
     * @return string
     */
    public function beginToday(): string
    {
        return date('Y-m-d') . ' 00:00:00';
    }

    /**
     * 今天结束的Y-m-d H:i:s
     * @return string
     */
    public function endToday(): string
    {
        return date('Y-m-d') . ' 23:59:59';
    }

    /**
     * 本周的开始日期
     * @param bool $His 是否展示时分秒 默认true
     * @return false|string
     */
    public function beginWeek($His = true)
    {
        //$timestamp = mktime(0, 0, 0, date('m'), date('d') - date('w') + 1, date('Y'));
        return $His ? date('Y-m-d H:i:s', 'this week') : date('Y-m-d', 'this week');
    }

    /**
     * 本周的结束日期
     * @param bool $His 是否展示时分秒 默认true
     * @return false|string
     */
    public function endWeek($His = true)
    {
        //$timestamp = mktime(23, 59, 59, date('m'), date('d') - date('w') + 7, date('Y'));
        return $His ? date('Y-m-d H:i:s', 'last day next week') : date('Y-m-d', 'last day next week');
    }

    /**
     * 本月的开始日期
     * @param bool $His 是否展示时分秒 默认true
     * @return false|string
     */
    public function beginMonth($His = true)
    {
        $timestamp = mktime(0, 0, 0, date('m'), 1, date('Y'));
        return $His ? date('Y-m-d H:i:s', $timestamp) : date('Y-m-d', $timestamp);
    }

    /**
     * 本月的结束日期
     * @param bool $His 是否展示时分秒 默认true
     * @return false|string
     */
    public function endMonth($His = true)
    {
        $timestamp = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
        return $His ? date('Y-m-d H:i:s', $timestamp) : date('Y-m-d', $timestamp);
    }

    /**
     * 几年的开始日期
     * @param bool $His 是否展示时分秒 默认true
     * @return false|string
     */
    public function beginYear($His = true)
    {
        $timestamp = mktime(0, 0, 0, 1, 1, date('Y'));
        return $His ? date('Y-m-d H:i:s', $timestamp) : date('Y-m-d', $timestamp);
    }

    /**
     * 本月的结束日期
     * @param bool $His 是否展示时分秒 默认true
     * @return false|string
     */
    public function endYear($His = true)
    {
        $timestamp = mktime(23, 59, 59, 12, 31, date('Y'));
        return $His ? date('Y-m-d H:i:s', $timestamp) : date('Y-m-d', $timestamp);
    }

    /**
     * 获取本周的起止日期
     * @return array
     */
    public function getWeekDate()
    {
        $timestamp = $this->beginWeek();
        $date = [];
        for ($i = 0; $i < 7; $i++) {
            $date[] = date('Y-m-d',strtotime("+{$i} day", $timestamp));
        }
        return $date;
    }

    /**
     * 获取本年起止日期
     * @return array
     */
    public function getYearDate()
    {
        $year = date('Y');
        $date = [];
        for ($i = 1; $i <= 12; $i++) {
            $date[] =  $year . '-' . sprintf("%02d", $i);
        }
        return $date;
    }

    // 获取当月第一天及最后一天
    function getthemonth($date)
    {
        $firstday = date('Y-m-01', strtotime($date));
        $lastday = date('Y-m-d', strtotime("$firstday +1 month -1 day"));
        return array($firstday,$lastday);
    }

//上个月第一天:
//echo date('Y-m-d', strtotime(date('Y-m-01') . ' -1 month')); // 计算出本月第一天再减一个月
//上个月最后一天:
//echo date('Y-m-d', strtotime(date('Y-m-01') . ' -1 day')); // 计算出本月第一天再减一天
}