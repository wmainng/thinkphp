<?php
// +----------------------------------------------------------------------
// | 福禄开放平台
// | https://docs.open.fulu.com/
// +----------------------------------------------------------------------

namespace helper;

class FuLu
{
    private array $config = [
        "host" => "http://openapi.fulu.com/api/getway",
        "appKey" => "",
        "appSecret" => ""
    ];

    private string $method;

    private array $biz_content;

    public function __construct(array $config)
    {
        $this->config["appKey"] = $config['appKey'];
        $this->config["appSecret"] = $config['appSecret'];
    }

    /**
     * 直充下单
     *  商品编号 product_id
     *  充值账号 charge_account
     *  购买数量 buy_num
     *  外部订单号 customer_order_no
     */
    public function direct($param)
    {
        $this->setMethod('fulu.order.direct.add');
        $this->setContent([
            "product_id" => $param['product_id'] ?? "",
            "customer_order_no" => $param['customer_order_no'] ?? "",
            "charge_account" => $param['charge_account'] ?? "",
            "buy_num" => $param['buy_num'] ?? "",
            "charge_game_name" => $param['charge_game_name'] ?? '',
            "charge_game_region" => $param['charge_game_region'] ?? '',
            "charge_game_srv" => $param['charge_game_srv'] ?? "",
            "charge_type" => $param['charge_type'] ?? "",
            "charge_password" => $param['charge_password'] ?? "",
            "charge_ip" => $param['charge_ip'] ?? "",
            "contact_qq" => $param['contact_qq'] ?? "",
            "contact_tel" => $param['contact_tel'] ?? "",
            "remaining_number" => $param['remaining_number'] ?? "",
            "charge_game_role" => $param['charge_game_role'] ?? "",
            "customer_price" => $param['customer_price'] ?? "",
            "shop_type" => $param['shop_type'] ?? "",
            "external_biz_id" => $param['external_biz_id'] ?? ""
        ]);
        return $this->getResponse();
    }

    /**
     * 卡密下单
     *  product_id 商品编号
     *  购买数量 buy_num
     *  外部订单号 customer_order_no
     */
    public function card($param)
    {
        $this->setMethod('fulu.order.direct.add');
        $this->setContent([
            "product_id" => $param['product_id'],
            "buy_num" => $param['buy_num'],
            "customer_order_no" => $param['customer_order_no'],
            "customer_price" => "",
            "shop_type" => "",
            "external_biz_id" => ""
        ]);
        return $this->getResponse();
    }

    /**
     * 话费下单
     *   手机号码 charge_phone
     *   充值数额 charge_value
     *   外部订单号 customer_order_no
     */
    public function mobile($param)
    {
        $this->setMethod('fulu.order.mobile.add');
        $this->setContent([
            "charge_phone" => $param['charge_phone'],
            "charge_value" => (double)$param['charge_value'],
            "customer_order_no" => $param['customer_order_no'],
            "customer_price" => "",
            "shop_type" => "",
            "external_biz_id" => ""
        ]);
        return $this->getResponse();
    }

    /**
     * 订单查询
     * @param array $param 外部订单号
     */
    public function order($param)
    {
        $this->setMethod('fulu.order.info.get');
        $this->setContent([
            "customer_order_no" => $param['customer_order_no'],
        ]);
        return $this->getResponse();
    }

    /**
     * 请求类型
     * @param string $method
     * @return void
     */
    public function setMethod(string $method)
    {
        $this->method = $method;
    }

    /**
     * 请求内容
     * @param array $content
     * @return void
     */
    public function setContent(array $content)
    {
        $this->biz_content = $content;
    }

    /**
     * 公共相应
     * @return mixed
     */
    public function getResponse()
    {
        $params = [
            "app_key" => $this->config["appKey"],
            "method" => $this->method,
            "timestamp" => date('Y-m-d H:i:s'),
            "version" => "2.0",
            "format" => "json",
            "charset" => "utf-8",
            "sign_type" => "md5",
            "sign" => "",
            "app_auth_token" => "",
            "biz_content" => json_encode($this->biz_content)
        ];
        unset($params['sign']);
        $params['sign'] = $this->getSign($params);

        $client = new Client();
        $response = $client->request('post', $this->config["host"], [
            'headers' => ['Content-Type: application/json'],
            'json' => $params
        ]);
        return $response->getBody();
    }

    /**
     * php签名方法
     */
    private function getSign($Parameters)
    {
        //签名步骤一：把字典json序列化
        $json = json_encode($Parameters, 320);
        //签名步骤二：转化为数组
        $jsonArr = $this->mb_str_split($json);
        //签名步骤三：排序
        sort($jsonArr);
        //签名步骤四：转化为字符串
        $string = implode('', $jsonArr);
        //签名步骤五：在string后加入secret
        $string = $string . $this->config["appSecret"];
        //签名步骤六：MD5加密
        return strtolower(md5($string));
    }

    /**
     * 可将字符串中中文拆分成字符数组
     */
    private function mb_str_split($str)
    {
        return preg_split('/(?<!^)(?!$)/u', $str);
    }
}