<?php
// +----------------------------------------------------------------------
// | 腾讯地图
// +----------------------------------------------------------------------

namespace helper\tencent;

use helper\facade\IMap;

class Map implements IMap
{
    private string $host = 'http://apis.map.qq.com/ws';

    private string $key;

    public function __construct($config)
    {
        $this->key = $config['key'];
    }

    /**
     * 地点搜索
     */
    public function getSearch()
    {
        $url = "https://apis.map.qq.com/ws/place/v1/search";
    }

    /**
     * 关键词输入提示
     * @param $region string 城市
     * @param $keyword string 关键词
     * @return array
     */
    public function getSuggestion(string $region, string $keyword)
    {
        $key = $this->key;
        $url = "http://apis.map.qq.com/ws/place/v1/suggestion/?region=$region&keyword=$keyword&key=$key";
        $res = file_get_contents($url);
        $arr = json_decode($res, true);
        if ($arr['status'] == 0) {
            return $arr;
        } else {
            return $arr['message'];
        }
    }

    /**
     * 逆地址解析(坐标位置描述)
     * @param string $address
     * @return array
     */
    public function getLocation(string $address)
    {
        $url = "https://apis.map.qq.com/ws/geocoder/v1/?address=$address&key=$this->key";
        if ($result = file_get_contents($url)) {
            $result = json_decode($result, true);
            if (empty($result['status'])) {
                return $result['result'];
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    /**
     * 逆地址解析(坐标位置描述)
     * @param string $location
     * @return array
     */
    public function getAddress(string $location, string $batch = 'false'): array
    {
        $url = "https://apis.map.qq.com/ws/geocoder/v1/?location=$location&key=$this->key";
        if ($result = file_get_contents($url)) {
            $result = json_decode($result, true);
            if (empty($result['status'])) {
                return $result['result']['ad_info'];
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    /**
     * 行政区划
     * @param $id string 父级行政区划ID
     * @return array
     */
    public function getDistrict($id)
    {
        $key = $this->key;
        if ($id) {
            $url = "http://apis.map.qq.com/ws/district/v1/getchildren?&id=$id&key=$key";
        } else {
            $url = "http://apis.map.qq.com/ws/district/v1/list?key=$key";

        }
        //https://apis.map.qq.com/ws/district/v1/search
        $res = file_get_contents($url);
        $arr = json_decode($res, true);
        return $arr;
    }

    /**
     * 距离计算 (一对多)
     * @param $lat1 string
     * @param $lng1 string
     * @param $lat2 string
     * @param $lng2 string
     * @param $mode string driving（驾车）、walking（步行）
     * @return array $distance 距离，单位：米;$duration:驾车时间，单位：默认秒转换成分钟
     */
    public function getDistance($lat1, $lng1, $lat2, $lng2, $mode = 'driving')
    {
        $key = $this->key;
        $location = $lat1 . "," . $lng1;
        $destination = $lat2 . "," . $lng2;
        $url = "http://apis.map.qq.com/ws/distance/v1/?mode=$mode&from=$location&to=$destination&key=$key";
        $res = file_get_contents($url);
        $arr = json_decode($res, true);
        $distance = $arr['result']['elements']['0']['distance'];
        $duration = $arr['result']['elements']['0']['duration'];
        return array('distance' => $distance, 'duration' => round($duration / 60, 2));
    }

    /**
     * 坐标转换
     * @param $lat string 其它图商坐标
     * @param $lng string 其它图商坐标
     * @return array
     */
    public function getTranslate($lat, $lng)
    {
        $key = $this->key;
        $location = $lat . "," . $lng;
        $url = "http://apis.map.qq.com/ws/coord/v1/translate?locations=$location&type=3&key=$key";
        $res = file_get_contents($url);
        $arr = json_decode($res, true);
        $lng = $arr['locations']['lng'];//纬度
        $lat = $arr['locations']['lat'];//经度
        return array('lat' => $lat, 'lng' => $lng);
    }

    /**
     * IP定位
     * @param $ip string
     * @return array
     */
    public function getIp($ip = '')
    {
        $url = "https://apis.map.qq.com/ws/location/v1/ip?ip=$ip&key=$this->key";
        if ($result = file_get_contents($url)) {
            $result = json_decode($result, true);
            if (empty($result['status'])) {
                return $result['result']['ad_info'];
            } else {
                return [];
            }
        } else {
            return [];
        }
    }
}
