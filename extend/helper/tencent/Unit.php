<?php

namespace helper\tencent;

class Unit
{
    /**
     * 获取QQ昵称和头像
     * @param $qq
     * @return array
     */
    function getQQInfo($qq): array
    {
        $data = file_get_contents('https://r.qzone.qq.com/fcg-bin/cgi_get_portrait.fcg?uins=' . $qq);
        $data = mb_convert_encoding($data, "UTF-8", "GBK");
        $name = json_decode(substr($data, 17, -1), true);
        $nickname = $name["$qq"][6];
        $headimg = "https://q1.qlogo.cn/g?b=qq&nk=".$qq."&s=100";
        return compact('nickname', 'headimg');
    }
}