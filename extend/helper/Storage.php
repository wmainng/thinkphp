<?php
// +----------------------------------------------------------------------
// | 存储
// +----------------------------------------------------------------------

namespace helper;

use helper\storage\Driver;

class Storage
{
    /**
     * @var array 缓存的实例
     */
    public static array $instance = [];

    /**
     * 上传限制,超时限制
     * @param string $name
     * @param array $config
     * @return Driver
     */
    public static function factory(string $name = 'oss', array $config = []): Driver
    {
        $name = strtolower($name);
        if (!isset(self::$instance[$name])) {
            $class = '\\helper\\storage\\driver\\' . ucwords($name);
            self::$instance[$name] = new $class($config);
        }
        return self::$instance[$name];
    }
}