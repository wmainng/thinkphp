<?php
// +----------------------------------------------------------------------
// | 百度云
// | 卡证 1) 身份证 10000 200.00 2) 营业执照 10000 280.00
// +----------------------------------------------------------------------
// | Copyright (c) 2018-2022 wmainng All rights reserved.
// +----------------------------------------------------------------------
// | Author: wmainng <wmainng@qq.com>
// +----------------------------------------------------------------------
namespace helper\baidu;

use helper\Client;
use helper\facade\IOcr;

class Ocr implements IOcr
{
    private string $host = 'https://aip.baidubce.com';

    private array $config;

    private string $access_token;

    /**
     * @var Client
     */
    protected Client $client;

    /**
     * 初始化
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->client = new Client();
    }

    /**
     * 获取 Access Token
     * @param string $path
     * @return string
     */
    public function setAccessToken(string $path)
    {
        $filename = $path . $this->config['apiKey'] . '.json'; //用于保存access token
        if (!file_exists($filename)) {
            file_put_contents($filename, '');
        }
        $data = json_decode(file_get_contents($filename));
        if (empty($data) || $data->expire_time < time()) {
            $path = '/oauth/2.0/token';
            $post_data['grant_type']    = 'client_credentials';
            $post_data['client_id']     = $this->config['apiKey'];
            $post_data['client_secret'] = $this->config['secretKey'];

            $o = "";
            foreach ( $post_data as $k => $v ) {
                $o.= "$k=" . urlencode( $v ). "&" ;
            }
            $post_data = substr($o,0,-1);
            $response = $this->client->post($this->host . $path, ['body' => $post_data]);
            $data = json_decode($response->getContents());
            $access_token = $data->access_token;
            if ($access_token) {
                $data->expire_time = time() + $data->expires_in;
                $data->access_token = $access_token;
                file_put_contents($filename, json_encode($data));
            }
        } else {
            $access_token = $data->access_token;
        }
        $this->access_token = $access_token;
        return $access_token;
    }

    /**
     * 通用文字识别
     * @param string $image
     * @param int $type 1:标准版,2:标准含位置版,3:高精度版,4:高精度含位置版
     * @return array
     */
    public function general(string $image, int $type = 1): array
    {
        switch ($type) {
            case 2:
                $type = 'general';
                break;
            case 3:
                $type = 'accurate_basic';
                break;
            case 4:
                $type = 'accurate';
                break;
            default:
                $type = 'general_basic';
        }
        $path = '/rest/2.0/ocr/v1/' . $type . '?access_token=' . $this->access_token;
        $img = $this->client->file($image);
        $bodys = array(
            // 'image' => $img // 图像数据
            'url' => $img // 图片完整url
        );
        $response = $this->client->post($this->host . $path, ['body' => $bodys]);
        if ($response->getStatusCode() != 200) {
            return [];
        }
        return $response->getBody();
    }

    /**
     * 身份证识别
     * @param string $image image: 图片 默认本地图片
     * @param string $side 身份证正反面类型:face/back
     * @return array
     */
    public function idCard(string $image, string $side = 'face'): array
    {

    }

    /**
     * 营业执照识别 阿里云计算有限公司
     * @param string $image image 营业执照照片
     * @return array
     */
    public function businessLicense(string $image): array
    {

    }
}
