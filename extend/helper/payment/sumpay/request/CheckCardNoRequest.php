<?php

namespace helper\payment\sumpay\request;

class CheckCardNoRequest
{
    private string $service;

    private string $mer_no;

    private string $business_code;

    private string $card_no;

    private string $order_amount;

    private array $bizContent;

    /**
     * @return string
     */
    public function getService(): string
    {
        return $this->service;
    }

    /**
     * @param string $service
     */
    public function setService(string $service): void
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function getMerNo(): string
    {
        return $this->mer_no;
    }

    /**
     * @param string $mer_no
     */
    public function setMerNo(string $mer_no): void
    {
        $this->mer_no = $mer_no;
    }

    /**
     * @return string
     */
    public function getBusinessCode(): string
    {
        return $this->business_code;
    }

    /**
     * @param string $business_code
     */
    public function setBusinessCode(string $business_code): void
    {
        $this->business_code = $business_code;
    }

    /**
     * @return string
     */
    public function getCardNo(): string
    {
        return $this->card_no;
    }

    /**
     * @param string $card_no
     */
    public function setCardNo(string $card_no): void
    {
        $this->card_no = $card_no;
    }

    /**
     * @return string
     */
    public function getOrderAmount(): string
    {
        return $this->order_amount;
    }

    /**
     * @param string $order_amount
     */
    public function setOrderAmount(string $order_amount): void
    {
        $this->order_amount = $order_amount;
    }

    /**
     * @return array
     */
    public function getBizContent(): array
    {
        return $this->bizContent;
    }

    /**
     * @param array $bizContent
     */
    public function setBizContent(array $bizContent): void
    {
        $this->bizContent = $bizContent;
    }
}