<?php

namespace helper\payment\sumpay\request;

class AvailableBankRequest
{
    private string $service;

    private string $mer_no;

    private string $user_id;

    private string $user_id_type;

    private string $business_code;

    private array $bizContent;

    /**
     * @return string
     */
    public function getService(): string
    {
        return $this->service;
    }

    /**
     * @param string $service
     */
    public function setService(string $service): void
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function getMerNo(): string
    {
        return $this->mer_no;
    }

    /**
     * @param string $mer_no
     */
    public function setMerNo(string $mer_no): void
    {
        $this->mer_no = $mer_no;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->user_id;
    }

    /**
     * @param string $user_id
     */
    public function setUserId(string $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getUserIdType(): string
    {
        return $this->user_id_type;
    }

    /**
     * @param string $user_id_type
     */
    public function setUserIdType(string $user_id_type): void
    {
        $this->user_id_type = $user_id_type;
    }

    /**
     * @return string
     */
    public function getBusinessCode(): string
    {
        return $this->business_code;
    }

    /**
     * @param string $business_code
     */
    public function setBusinessCode(string $business_code): void
    {
        $this->business_code = $business_code;
    }

    /**
     * @return array
     */
    public function getBizContent(): array
    {
        return $this->bizContent;
    }

    /**
     * @param array $bizContent
     */
    public function setBizContent(array $bizContent): void
    {
        $this->bizContent = $bizContent;
    }
}