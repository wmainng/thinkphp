<?php
// +----------------------------------------------------------------------
// | 统统付
// +----------------------------------------------------------------------

namespace helper\payment\sumpay;

use helper\payment\sumpay\request\AvailableBankRequest;
use helper\payment\sumpay\request\CheckCardNoRequest;

class QuickPay
{
    private array $config;

    public function __construct(array $config)
    {
        $this->config = [
            'appId' => $config['appId'],
            'publicKey' => $config['publicKey'],
            'privateKey' => $config['privateKey'],
            'password' => $config['password'],
        ];
    }

    /**
     * 5.2.1.1 获取已绑卡和可用银行
     * @param string $user_id
     * @return array
     */
    public function availableBank(string $user_id = ''): array
    {
        $aop = new AopClient();
        $aop->gatewayUrl = "https://test.sumpay.cn/entrance/gateway.htm";
        $aop->appId = $this->config['appId'];
        $aop->publicKey = $this->config['publicKey'];
        $aop->privateKey = $this->config['privateKey'];
        $aop->password = $this->config['password'];

        $request = new AvailableBankRequest();
        $request->setBizContent([
            'service' => 'fosun.sumpay.api.quickpay.avaliable.bank',
            'mer_no' => $this->config['appId'],
            'user_id' => $user_id,
            'user_id_type' => "0",
            'business_code' => '03',
        ]);
        return $aop->execute($request);
    }

    /**
     * 5.2.1.2 校验银行卡信息接口
     * @param string $card_no 卡号
     * @param string $order_amount 金额
     * @return array
     */
    public function checkCardNo(string $card_no, string $order_amount = ''): array
    {
        $aop = new AopClient();
        $aop->gatewayUrl = "https://test.sumpay.cn/entrance/gateway.htm";
        $aop->appId = $this->config['appId'];
        $aop->publicKey = $this->config['publicKey'];
        $aop->privateKey = $this->config['privateKey'];
        $aop->password = $this->config['privateKeyPassWord'];

        $request = new CheckCardNoRequest();
        $request->setBizContent([
            'service' => 'fosun.sumpay.api.quickpay.check.card.no',
            'mer_no' => $this->config['appId'],
            'card_no' => $card_no,
            'business_code' => '03',
            'order_amount' => $order_amount
        ]);
        return $aop->execute($request);
    }

    /**
     * 5.2.1.3 申请快捷支付接口
     */

    /**
     * 5.2.1.4 提交验证码接口
     */

    /**
     * 5.2.1.5 解绑绑定卡
     */


    /**
     * 5.4.1 独立绑卡第一步
     * @return void
     */
    public function sendMessageForSign()
    {
        $param = [
            'service' => 'fosun.sumpay.api.quickpay.send.message.for.sign',
            'mer_no' => '',
            'user_id' => '',
            'order_no' => date('YmdHis'),
            'card_no' => '6217231303001013864',
            'bank_code' => 'BOC',
            'card_type' => "0",
            'mobile_no' => "18905527538",
            'realname' => "王敏",
            'id_type' => 1,
            "id_no" => "6217231303001013864",
            "cvv2" => "",
            "valid_year" => "",
            "valid_month" => "",
            "return_url" => "",
            "repeat_sign" => ""
        ];
    }

    /**
     * 5.4.2 独立绑卡第二步
     */
    public function validMessageForSign()
    {

    }
}