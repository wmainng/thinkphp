<?php

namespace helper\payment\ecpss\request;

class ScanMerchantInQueryRequest
{
    private String $MerNo;
    private String $Version;
    private String $CompanyNo;
    private String $PayType;
    private String $RandomStr;
    private String $SignInfo;

    /**
     * @return String
     */
    public function getMerNo(): string
    {
        return $this->MerNo;
    }

    /**
     * @param String $MerNo
     */
    public function setMerNo(string $MerNo): void
    {
        $this->MerNo = $MerNo;
    }

    /**
     * @return String
     */
    public function getVersion(): string
    {
        return $this->Version;
    }

    /**
     * @param String $Version
     */
    public function setVersion(string $Version): void
    {
        $this->Version = $Version;
    }

    /**
     * @return String
     */
    public function getCompanyNo(): string
    {
        return $this->CompanyNo;
    }

    /**
     * @param String $CompanyNo
     */
    public function setCompanyNo(string $CompanyNo): void
    {
        $this->CompanyNo = $CompanyNo;
    }

    /**
     * @return String
     */
    public function getPayType(): string
    {
        return $this->PayType;
    }

    /**
     * @param String $PayType
     */
    public function setPayType(string $PayType): void
    {
        $this->PayType = $PayType;
    }

    /**
     * @return String
     */
    public function getRandomStr(): string
    {
        return $this->RandomStr;
    }

    /**
     * @param String $RandomStr
     */
    public function setRandomStr(string $RandomStr): void
    {
        $this->RandomStr = $RandomStr;
    }

    /**
     * @return String
     */
    public function getSignInfo(): string
    {
        return $this->SignInfo;
    }

    /**
     * @param String $SignInfo
     */
    public function setSignInfo(string $SignInfo): void
    {
        $this->SignInfo = $SignInfo;
    }
}