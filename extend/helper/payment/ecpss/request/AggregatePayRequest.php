<?php

namespace helper\payment\ecpss\request;

class AggregatePayRequest
{
    private String $MerchantNo;
    private String $MerchantOrderNo;
    private String $PayType;
    private String $Amount;
    private String $Subject;
    private String $Desc;
    private String $CompanyNo;
    private String $RandomStr;
    private String $SignInfo;
    private String $AdviceUrl;
    private String $ReturnUrl;
    private String $SubAppid;
    private String $UserId;
    private String $LimitPayChannel;
    private String $EnablePayChannel;
    private String $OrderTime;

    /**
     * @return String
     */
    public function getMerchantNo(): string
    {
        return $this->MerchantNo;
    }

    /**
     * @param String $MerchantNo
     */
    public function setMerchantNo(string $MerchantNo): void
    {
        $this->MerchantNo = $MerchantNo;
    }

    /**
     * @return String
     */
    public function getMerchantOrderNo(): string
    {
        return $this->MerchantOrderNo;
    }

    /**
     * @param String $MerchantOrderNo
     */
    public function setMerchantOrderNo(string $MerchantOrderNo): void
    {
        $this->MerchantOrderNo = $MerchantOrderNo;
    }

    /**
     * @return String
     */
    public function getPayType(): string
    {
        return $this->PayType;
    }

    /**
     * @param String $PayType
     */
    public function setPayType(string $PayType): void
    {
        $this->PayType = $PayType;
    }

    /**
     * @return String
     */
    public function getAmount(): string
    {
        return $this->Amount;
    }

    /**
     * @param String $Amount
     */
    public function setAmount(string $Amount): void
    {
        $this->Amount = $Amount;
    }

    /**
     * @return String
     */
    public function getSubject(): string
    {
        return $this->Subject;
    }

    /**
     * @param String $Subject
     */
    public function setSubject(string $Subject): void
    {
        $this->Subject = $Subject;
    }

    /**
     * @return String
     */
    public function getDesc(): string
    {
        return $this->Desc;
    }

    /**
     * @param String $Desc
     */
    public function setDesc(string $Desc): void
    {
        $this->Desc = $Desc;
    }

    /**
     * @return String
     */
    public function getCompanyNo(): string
    {
        return $this->CompanyNo;
    }

    /**
     * @param String $CompanyNo
     */
    public function setCompanyNo(string $CompanyNo): void
    {
        $this->CompanyNo = $CompanyNo;
    }

    /**
     * @return String
     */
    public function getRandomStr(): string
    {
        return $this->RandomStr;
    }

    /**
     * @param String $RandomStr
     */
    public function setRandomStr(string $RandomStr): void
    {
        $this->RandomStr = $RandomStr;
    }

    /**
     * @return String
     */
    public function getSignInfo(): string
    {
        return $this->SignInfo;
    }

    /**
     * @param String $SignInfo
     */
    public function setSignInfo(string $SignInfo): void
    {
        $this->SignInfo = $SignInfo;
    }

    /**
     * @return String
     */
    public function getAdviceUrl(): string
    {
        return $this->AdviceUrl;
    }

    /**
     * @param String $AdviceUrl
     */
    public function setAdviceUrl(string $AdviceUrl): void
    {
        $this->AdviceUrl = $AdviceUrl;
    }

    /**
     * @return String
     */
    public function getReturnUrl(): string
    {
        return $this->ReturnUrl;
    }

    /**
     * @param String $ReturnUrl
     */
    public function setReturnUrl(string $ReturnUrl): void
    {
        $this->ReturnUrl = $ReturnUrl;
    }

    /**
     * @return String
     */
    public function getSubAppid(): string
    {
        return $this->SubAppid;
    }

    /**
     * @param String $SubAppid
     */
    public function setSubAppid(string $SubAppid): void
    {
        $this->SubAppid = $SubAppid;
    }

    /**
     * @return String
     */
    public function getUserId(): string
    {
        return $this->UserId;
    }

    /**
     * @param String $UserId
     */
    public function setUserId(string $UserId): void
    {
        $this->UserId = $UserId;
    }

    /**
     * @return String
     */
    public function getLimitPayChannel(): string
    {
        return $this->LimitPayChannel;
    }

    /**
     * @param String $LimitPayChannel
     */
    public function setLimitPayChannel(string $LimitPayChannel): void
    {
        $this->LimitPayChannel = $LimitPayChannel;
    }

    /**
     * @return String
     */
    public function getEnablePayChannel(): string
    {
        return $this->EnablePayChannel;
    }

    /**
     * @param String $EnablePayChannel
     */
    public function setEnablePayChannel(string $EnablePayChannel): void
    {
        $this->EnablePayChannel = $EnablePayChannel;
    }

    /**
     * @return String
     */
    public function getOrderTime(): string
    {
        return $this->OrderTime;
    }

    /**
     * @param String $OrderTime
     */
    public function setOrderTime(string $OrderTime): void
    {
        $this->OrderTime = $OrderTime;
    }
}