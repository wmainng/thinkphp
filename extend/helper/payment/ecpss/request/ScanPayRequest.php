<?php

namespace helper\payment\ecpss\request;

class ScanPayRequest
{
    private String $MerNo;
    private String $BillNo;
    private String $payType;
    private String $Amount;
    private String $OrderTime;
    private String $ReturnUrl;
    private String $AdviceUrl;
    private String $LimitPayChannel;
    private String $ScanpayMerchantCode;
    private String $SignInfo;
    private String $products;
    private String $remark;
    private String $NeedCheckInfo;
    private String $PayerName;
    private String $PayerMobile;
    private String $CertType;
    private String $CertNo;
    private String $MerName;
    private String $TimeoutExpress;
    private String $Areainfo;
    private String $TerminalId;
    private String $subMerchantNo;
    private String $busType;
    private String $behalfFee;
    private String $DsAdviceUrl;

    private function makeOrderTime(): String
    {
        return date("YmdHis");
    }

    private function makeBillNo(): String
    {
        return date("YmdHis");
    }

    /**
     * @return String
     */
    public function getMerNo(): string
    {
        return $this->MerNo;
    }

    /**
     * @param String $MerNo
     */
    public function setMerNo(string $MerNo): void
    {
        $this->MerNo = $MerNo;
    }

    /**
     * @return String
     */
    public function getBillNo(): string
    {
        return $this->BillNo;
    }

    /**
     * @param String $BillNo
     */
    public function setBillNo(string $BillNo): void
    {
        $this->BillNo = $BillNo;
    }

    /**
     * @return String
     */
    public function getPayType(): string
    {
        return $this->payType;
    }

    /**
     * @param String $payType
     */
    public function setPayType(string $payType): void
    {
        $this->payType = $payType;
    }

    /**
     * @return String
     */
    public function getAmount(): string
    {
        return $this->Amount;
    }

    /**
     * @param String $Amount
     */
    public function setAmount(string $Amount): void
    {
        $this->Amount = $Amount;
    }

    /**
     * @return String
     */
    public function getOrderTime(): string
    {
        return $this->OrderTime;
    }

    /**
     * @param String $OrderTime
     */
    public function setOrderTime(string $OrderTime): void
    {
        $this->OrderTime = $OrderTime;
    }

    /**
     * @return String
     */
    public function getReturnUrl(): string
    {
        return $this->ReturnUrl;
    }

    /**
     * @param String $ReturnUrl
     */
    public function setReturnUrl(string $ReturnUrl): void
    {
        $this->ReturnUrl = $ReturnUrl;
    }

    /**
     * @return String
     */
    public function getAdviceUrl(): string
    {
        return $this->AdviceUrl;
    }

    /**
     * @param String $AdviceUrl
     */
    public function setAdviceUrl(string $AdviceUrl): void
    {
        $this->AdviceUrl = $AdviceUrl;
    }

    /**
     * @return String
     */
    public function getLimitPayChannel(): string
    {
        return $this->LimitPayChannel;
    }

    /**
     * @param String $LimitPayChannel
     */
    public function setLimitPayChannel(string $LimitPayChannel): void
    {
        $this->LimitPayChannel = $LimitPayChannel;
    }

    /**
     * @return String
     */
    public function getScanpayMerchantCode(): string
    {
        return $this->ScanpayMerchantCode;
    }

    /**
     * @param String $ScanpayMerchantCode
     */
    public function setScanpayMerchantCode(string $ScanpayMerchantCode): void
    {
        $this->ScanpayMerchantCode = $ScanpayMerchantCode;
    }

    /**
     * @return String
     */
    public function getSignInfo(): string
    {
        return $this->SignInfo;
    }

    /**
     * @param String $SignInfo
     */
    public function setSignInfo(string $SignInfo): void
    {
        $this->SignInfo = $SignInfo;
    }

    /**
     * @return String
     */
    public function getProducts(): string
    {
        return $this->products;
    }

    /**
     * @param String $products
     */
    public function setProducts(string $products): void
    {
        $this->products = $products;
    }

    /**
     * @return String
     */
    public function getRemark(): string
    {
        return $this->remark;
    }

    /**
     * @param String $remark
     */
    public function setRemark(string $remark): void
    {
        $this->remark = $remark;
    }

    /**
     * @return String
     */
    public function getNeedCheckInfo(): string
    {
        return $this->NeedCheckInfo;
    }

    /**
     * @param String $NeedCheckInfo
     */
    public function setNeedCheckInfo(string $NeedCheckInfo): void
    {
        $this->NeedCheckInfo = $NeedCheckInfo;
    }

    /**
     * @return String
     */
    public function getPayerName(): string
    {
        return $this->PayerName;
    }

    /**
     * @param String $PayerName
     */
    public function setPayerName(string $PayerName): void
    {
        $this->PayerName = $PayerName;
    }

    /**
     * @return String
     */
    public function getPayerMobile(): string
    {
        return $this->PayerMobile;
    }

    /**
     * @param String $PayerMobile
     */
    public function setPayerMobile(string $PayerMobile): void
    {
        $this->PayerMobile = $PayerMobile;
    }

    /**
     * @return String
     */
    public function getCertType(): string
    {
        return $this->CertType;
    }

    /**
     * @param String $CertType
     */
    public function setCertType(string $CertType): void
    {
        $this->CertType = $CertType;
    }

    /**
     * @return String
     */
    public function getCertNo(): string
    {
        return $this->CertNo;
    }

    /**
     * @param String $CertNo
     */
    public function setCertNo(string $CertNo): void
    {
        $this->CertNo = $CertNo;
    }

    /**
     * @return String
     */
    public function getMerName(): string
    {
        return $this->MerName;
    }

    /**
     * @param String $MerName
     */
    public function setMerName(string $MerName): void
    {
        $this->MerName = $MerName;
    }

    /**
     * @return String
     */
    public function getTimeoutExpress(): string
    {
        return $this->TimeoutExpress;
    }

    /**
     * @param String $TimeoutExpress
     */
    public function setTimeoutExpress(string $TimeoutExpress): void
    {
        $this->TimeoutExpress = $TimeoutExpress;
    }

    /**
     * @return String
     */
    public function getAreainfo(): string
    {
        return $this->Areainfo;
    }

    /**
     * @param String $Areainfo
     */
    public function setAreainfo(string $Areainfo): void
    {
        $this->Areainfo = $Areainfo;
    }

    /**
     * @return String
     */
    public function getTerminalId(): string
    {
        return $this->TerminalId;
    }

    /**
     * @param String $TerminalId
     */
    public function setTerminalId(string $TerminalId): void
    {
        $this->TerminalId = $TerminalId;
    }

    /**
     * @return String
     */
    public function getSubMerchantNo(): string
    {
        return $this->subMerchantNo;
    }

    /**
     * @param String $subMerchantNo
     */
    public function setSubMerchantNo(string $subMerchantNo): void
    {
        $this->subMerchantNo = $subMerchantNo;
    }

    /**
     * @return String
     */
    public function getBusType(): string
    {
        return $this->busType;
    }

    /**
     * @param String $busType
     */
    public function setBusType(string $busType): void
    {
        $this->busType = $busType;
    }

    /**
     * @return String
     */
    public function getBehalfFee(): string
    {
        return $this->behalfFee;
    }

    /**
     * @param String $behalfFee
     */
    public function setBehalfFee(string $behalfFee): void
    {
        $this->behalfFee = $behalfFee;
    }

    /**
     * @return String
     */
    public function getDsAdviceUrl(): string
    {
        return $this->DsAdviceUrl;
    }

    /**
     * @param String $DsAdviceUrl
     */
    public function setDsAdviceUrl(string $DsAdviceUrl): void
    {
        $this->DsAdviceUrl = $DsAdviceUrl;
    }
}