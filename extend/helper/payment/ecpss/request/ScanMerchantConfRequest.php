<?php

namespace helper\payment\ecpss\request;

class ScanMerchantConfRequest
{
    private String $MerNo;
    private String $Version;
    private String $CompanyNo;
    private String $MethodName;
    private String $SubAppid;
    private String $PayPath;
    private String $RandomStr;
    private String $SignInfo;

    /**
     * @return String
     */
    public function getMerNo(): string
    {
        return $this->MerNo;
    }

    /**
     * @param String $MerNo
     */
    public function setMerNo(string $MerNo): void
    {
        $this->MerNo = $MerNo;
    }

    /**
     * @return String
     */
    public function getVersion(): string
    {
        return $this->Version;
    }

    /**
     * @param String $Version
     */
    public function setVersion(string $Version): void
    {
        $this->Version = $Version;
    }

    /**
     * @return String
     */
    public function getCompanyNo(): string
    {
        return $this->CompanyNo;
    }

    /**
     * @param String $CompanyNo
     */
    public function setCompanyNo(string $CompanyNo): void
    {
        $this->CompanyNo = $CompanyNo;
    }

    /**
     * @return String
     */
    public function getMethodName(): string
    {
        return $this->MethodName;
    }

    /**
     * @param String $MethodName
     */
    public function setMethodName(string $MethodName): void
    {
        $this->MethodName = $MethodName;
    }

    /**
     * @return String
     */
    public function getSubAppid(): string
    {
        return $this->SubAppid;
    }

    /**
     * @param String $SubAppid
     */
    public function setSubAppid(string $SubAppid): void
    {
        $this->SubAppid = $SubAppid;
    }

    /**
     * @return String
     */
    public function getPayPath(): string
    {
        return $this->PayPath;
    }

    /**
     * @param String $PayPath
     */
    public function setPayPath(string $PayPath): void
    {
        $this->PayPath = $PayPath;
    }

    /**
     * @return String
     */
    public function getRandomStr(): string
    {
        return $this->RandomStr;
    }

    /**
     * @param String $RandomStr
     */
    public function setRandomStr(string $RandomStr): void
    {
        $this->RandomStr = $RandomStr;
    }

    /**
     * @return String
     */
    public function getSignInfo(): string
    {
        return $this->SignInfo;
    }

    /**
     * @param String $SignInfo
     */
    public function setSignInfo(string $SignInfo): void
    {
        $this->SignInfo = $SignInfo;
    }
}