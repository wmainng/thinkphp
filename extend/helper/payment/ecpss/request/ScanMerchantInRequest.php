<?php

namespace helper\payment\ecpss\request;

class ScanMerchantInRequest
{
    private String $MerNo;
    private String $Version;
    private String $SignInfo;
    private String $PayType;
    private String $RandomStr;
    private String $ChannelNo;
    private String $MerName;
    private String $ShortName;
    private String $ContactName;
    private String $ServicePhone;
    private String $Business;
    private String $Mcc;
    private String $ContactTag;
    private String $ContactType;
    private String $City;
    private String $District;
    private String $Address;
    private String $Province;
    private String $CardNo;
    private String $CardName;
    private String $ContactPhone;
    private String $ContactMobile;
    private String $BusinessLicense;
    private String $BusinessLicenseType;
    private String $IdCardNo;
    private String $SubAppID;
    private String $PayPath;
    private String $CompanyNo;

    /**
     * @return String
     */
    public function getMerNo(): string
    {
        return $this->MerNo;
    }

    /**
     * @param String $MerNo
     */
    public function setMerNo(string $MerNo): void
    {
        $this->MerNo = $MerNo;
    }

    /**
     * @return String
     */
    public function getVersion(): string
    {
        return $this->Version;
    }

    /**
     * @param String $Version
     */
    public function setVersion(string $Version): void
    {
        $this->Version = $Version;
    }

    /**
     * @return String
     */
    public function getSignInfo(): string
    {
        return $this->SignInfo;
    }

    /**
     * @param String $SignInfo
     */
    public function setSignInfo(string $SignInfo): void
    {
        $this->SignInfo = $SignInfo;
    }

    /**
     * @return String
     */
    public function getPayType(): string
    {
        return $this->PayType;
    }

    /**
     * @param String $PayType
     */
    public function setPayType(string $PayType): void
    {
        $this->PayType = $PayType;
    }

    /**
     * @return String
     */
    public function getRandomStr(): string
    {
        return $this->RandomStr;
    }

    /**
     * @param String $RandomStr
     */
    public function setRandomStr(string $RandomStr): void
    {
        $this->RandomStr = $RandomStr;
    }

    /**
     * @return String
     */
    public function getChannelNo(): string
    {
        return $this->ChannelNo;
    }

    /**
     * @param String $ChannelNo
     */
    public function setChannelNo(string $ChannelNo): void
    {
        $this->ChannelNo = $ChannelNo;
    }

    /**
     * @return String
     */
    public function getMerName(): string
    {
        return $this->MerName;
    }

    /**
     * @param String $MerName
     */
    public function setMerName(string $MerName): void
    {
        $this->MerName = $MerName;
    }

    /**
     * @return String
     */
    public function getShortName(): string
    {
        return $this->ShortName;
    }

    /**
     * @param String $ShortName
     */
    public function setShortName(string $ShortName): void
    {
        $this->ShortName = $ShortName;
    }

    /**
     * @return String
     */
    public function getContactName(): string
    {
        return $this->ContactName;
    }

    /**
     * @param String $ContactName
     */
    public function setContactName(string $ContactName): void
    {
        $this->ContactName = $ContactName;
    }

    /**
     * @return String
     */
    public function getServicePhone(): string
    {
        return $this->ServicePhone;
    }

    /**
     * @param String $ServicePhone
     */
    public function setServicePhone(string $ServicePhone): void
    {
        $this->ServicePhone = $ServicePhone;
    }

    /**
     * @return String
     */
    public function getBusiness(): string
    {
        return $this->Business;
    }

    /**
     * @param String $Business
     */
    public function setBusiness(string $Business): void
    {
        $this->Business = $Business;
    }

    /**
     * @return String
     */
    public function getMcc(): string
    {
        return $this->Mcc;
    }

    /**
     * @param String $Mcc
     */
    public function setMcc(string $Mcc): void
    {
        $this->Mcc = $Mcc;
    }

    /**
     * @return String
     */
    public function getContactTag(): string
    {
        return $this->ContactTag;
    }

    /**
     * @param String $ContactTag
     */
    public function setContactTag(string $ContactTag): void
    {
        $this->ContactTag = $ContactTag;
    }

    /**
     * @return String
     */
    public function getContactType(): string
    {
        return $this->ContactType;
    }

    /**
     * @param String $ContactType
     */
    public function setContactType(string $ContactType): void
    {
        $this->ContactType = $ContactType;
    }

    /**
     * @return String
     */
    public function getCity(): string
    {
        return $this->City;
    }

    /**
     * @param String $City
     */
    public function setCity(string $City): void
    {
        $this->City = $City;
    }

    /**
     * @return String
     */
    public function getDistrict(): string
    {
        return $this->District;
    }

    /**
     * @param String $District
     */
    public function setDistrict(string $District): void
    {
        $this->District = $District;
    }

    /**
     * @return String
     */
    public function getAddress(): string
    {
        return $this->Address;
    }

    /**
     * @param String $Address
     */
    public function setAddress(string $Address): void
    {
        $this->Address = $Address;
    }

    /**
     * @return String
     */
    public function getProvince(): string
    {
        return $this->Province;
    }

    /**
     * @param String $Province
     */
    public function setProvince(string $Province): void
    {
        $this->Province = $Province;
    }

    /**
     * @return String
     */
    public function getCardNo(): string
    {
        return $this->CardNo;
    }

    /**
     * @param String $CardNo
     */
    public function setCardNo(string $CardNo): void
    {
        $this->CardNo = $CardNo;
    }

    /**
     * @return String
     */
    public function getCardName(): string
    {
        return $this->CardName;
    }

    /**
     * @param String $CardName
     */
    public function setCardName(string $CardName): void
    {
        $this->CardName = $CardName;
    }

    /**
     * @return String
     */
    public function getContactPhone(): string
    {
        return $this->ContactPhone;
    }

    /**
     * @param String $ContactPhone
     */
    public function setContactPhone(string $ContactPhone): void
    {
        $this->ContactPhone = $ContactPhone;
    }

    /**
     * @return String
     */
    public function getContactMobile(): string
    {
        return $this->ContactMobile;
    }

    /**
     * @param String $ContactMobile
     */
    public function setContactMobile(string $ContactMobile): void
    {
        $this->ContactMobile = $ContactMobile;
    }

    /**
     * @return String
     */
    public function getBusinessLicense(): string
    {
        return $this->BusinessLicense;
    }

    /**
     * @param String $BusinessLicense
     */
    public function setBusinessLicense(string $BusinessLicense): void
    {
        $this->BusinessLicense = $BusinessLicense;
    }

    /**
     * @return String
     */
    public function getBusinessLicenseType(): string
    {
        return $this->BusinessLicenseType;
    }

    /**
     * @param String $BusinessLicenseType
     */
    public function setBusinessLicenseType(string $BusinessLicenseType): void
    {
        $this->BusinessLicenseType = $BusinessLicenseType;
    }

    /**
     * @return String
     */
    public function getIdCardNo(): string
    {
        return $this->IdCardNo;
    }

    /**
     * @param String $IdCardNo
     */
    public function setIdCardNo(string $IdCardNo): void
    {
        $this->IdCardNo = $IdCardNo;
    }

    /**
     * @return String
     */
    public function getSubAppID(): string
    {
        return $this->SubAppID;
    }

    /**
     * @param String $SubAppID
     */
    public function setSubAppID(string $SubAppID): void
    {
        $this->SubAppID = $SubAppID;
    }

    /**
     * @return String
     */
    public function getPayPath(): string
    {
        return $this->PayPath;
    }

    /**
     * @param String $PayPath
     */
    public function setPayPath(string $PayPath): void
    {
        $this->PayPath = $PayPath;
    }

    /**
     * @return String
     */
    public function getCompanyNo(): string
    {
        return $this->CompanyNo;
    }

    /**
     * @param String $CompanyNo
     */
    public function setCompanyNo(string $CompanyNo): void
    {
        $this->CompanyNo = $CompanyNo;
    }
}