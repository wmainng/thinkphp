<?php
// +----------------------------------------------------------------------
// | 响应
// +----------------------------------------------------------------------

namespace test;

class Response
{
    /**
     * 返回json数据
     * @return void
     */
    public function json($data)
    {
        header('Content-Type:application/json; charset=utf-8');
        exit(json_encode($data));
    }

    /**
     * 返回jsonp数据
     * @param $data
     * @return void
     */
    public function jsonp($data, $callback = 'callback')
    {
        $json = json_encode($data);
        exit($callback."($json)");
    }

    /**
     * 返回xml数据
     * @return void
     */
    public function xml($xml)
    {
        header('Content-Type:text/xml; charset=utf-8');
        exit($xml);
    }
}