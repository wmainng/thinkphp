<?php

namespace test;

use GatewayWorker\Lib\Gateway;

class GatewayClient
{
    public function __construct()
    {
        Gateway::$registerAddress = '127.0.0.1:1236';
    }

    /**
     * client_id与uid绑定
     * @param $client_id
     * @param $uid
     * @return void
     */
    public static function bindUid($client_id, $uid)
    {
        Gateway::bindUid($client_id, $uid);
    }
    
    /**
     * 向任意uid的网站页面发送数据
     * @param $uid
     * @param $message
     * @return void
     */
    public static function sendToUid($uid, $message)
    {
        Gateway::sendToUid($uid, $message);
    }

    /**
     * 加入某个群组
     */
    public static function joinGroup($client_id, $group_id)
    {
        Gateway::joinGroup($client_id, $group_id);
    }
    
    /**
     * 向任意群组的网站页面发送数据
     * @return void
     */
    public static function sendToGroup($group, $message)
    {
        Gateway::sendToGroup($group, $message);
    }
}