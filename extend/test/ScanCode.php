<?php
// +----------------------------------------------------------------------
// | 扫描
// +----------------------------------------------------------------------

namespace test;

class ScanCode
{
    /**
     * 1.客户端与服务端保持长连接，获取唯一的UUID(唯一字符串就可以)生成一个二维码用的接口。
     */
    public function uuid()
    {
        return substr(md5(uniqid(mt_rand(), true)), 0, 15);//生成uuid
    }

    /**
     * 2.APP端扫描到UUID后，把用户和这个UUID绑定(可以传user_id或者token),通过websocket通知web端
     * @return void
     */
    public function app()
    {
        
    }

    /**
     * 3.前端在收到用户扫码确认的信息后，跳转。
     * @return void
     */
    public function web()
    {

    }
}