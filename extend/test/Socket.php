<?php
// +----------------------------------------------------------------------
// | Socket
// +----------------------------------------------------------------------

namespace test;

class Socket
{
    public function test()
    {
        if (extension_loaded('sockets')) {
            //Create socket IPv4
            $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP) ;
            if($socket === false) {
                $errorcode = socket_last_error() ;
                $errormsg = socket_strerror($errorcode);
                echo "<p>Error socket IPv4: ".$errormsg."</p>\n" ;
            }
            else {
                echo "<p>Socket IPv4 supported</p>\n" ;
                socket_close($socket);
            }

            //Create socket IPv6
            $socket = socket_create(AF_INET6, SOCK_STREAM, SOL_TCP) ;
            if($socket === false) {
                $errorcode = socket_last_error() ;
                $errormsg = socket_strerror($errorcode);
                echo "<p>Error socket IPv6: ".$errormsg."</p>\n" ;
            }
            else {
                echo "<p>Socket IPv6 supported</p>\n" ;
                socket_close($socket);
            }
        }
        else echo "<p>Extension PHP sockets not loaded</p>\n" ;
    }

    public function server()
    {
        //创建socket套接字
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        //设置阻塞模式
        socket_set_block($socket);
        //为套接字绑定ip和端口
        socket_bind($socket,'127.0.0.1',3046);
        //监听socket
        socket_listen($socket,4);

        while(true) {
            //接收客户端请求
            if(($msgsocket = socket_accept($socket)) !== false)
            {
                //读取请求内容
                $buf = socket_read($msgsocket, 8192);
                echo "Received msg: $buf \n";
                $str = "this is a service message";
                //向连接的客户端发送数据
                socket_write($msgsocket, $str,strlen($str));
                //操作完之后需要关闭该连接否则 feof() 函数无法正确识别打开的句柄是否读取完成
                socket_close($msgsocket);
            }
        }
    }

    public function socket()
    {
        $st="socket send message";
        $length = strlen($st);
        //创建tcp套接字
        $socket = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
        //连接tcp
        socket_connect($socket, '127.0.0.1',3046);
        //向打开的套集字写入数据（发送数据）
        $s = socket_write($socket, $st, $length);
        //从套接字中获取服务器发送来的数据
        $msg = socket_read($socket,8190);

        echo $msg;
        //关闭连接
        socket_close($socket);
    }

    public function socket_open($host, $port)
    {
        //使用 fsockopen 打开tcp连接句柄
        $fp = fsockopen("tcp://127.0.0.1",3046);
        $msg = "fsockopen send message";
        //向句柄中写入数据
        fwrite($fp,$msg);
        $ret = "";
        //循环遍历获取句柄中的数据，其中 feof() 判断文件指针是否指到文件末尾
        while (!feof($fp)){
            stream_set_timeout($fp, 2);
            $ret .= fgets($fp, 128);
        }
        //关闭句柄
        fclose($fp);
        echo $ret;
    }

    public function socket_client($address, $msg)
    {
        //使用 stream_socket_client 打开 tcp 连接
        $fp = stream_socket_client("tcp://127.0.0.1:3046");
        $msg = "fsockopen send message";
        //向句柄中写入数据
        fwrite($fp,$msg);
        $ret = "";
        //循环遍历获取句柄中的数据，其中 feof() 判断文件指针是否指到文件末尾
        while (!feof($fp)){
            stream_set_timeout($fp, 2);
            $ret .= fgets($fp, 128);
        }
        //关闭句柄
        fclose($fp);
        echo $ret;
    }
}