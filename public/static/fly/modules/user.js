layui.define(['laypage', 'fly', 'element', 'flow', 'table', 'view'], function (exports) {

    var $ = layui.jquery;
    var layer = layui.layer;
    var util = layui.util;
    var laytpl = layui.laytpl;
    var form = layui.form;
    var laypage = layui.laypage;
    var fly = layui.fly;
    var flow = layui.flow;
    var element = layui.element;
    var upload = layui.upload;
    var table = layui.table;
    var view = layui.view;
    var router = layui.router();

    //视图设置
    view.set({
        views: '/views/'
    });

    //导航选中
    $("#user_nav a").each(function () {
        if ($(this)[0].href === String(window.location)) {
            $(this).addClass("layui-this");
        }
    })

    //我发的贴
    table.render({
        elem: '#LAY_mySendCard', url: '/api/user/post', method: 'get', cols: [[{
            field: 'title',
            title: '帖子标题',
            minWidth: 300,
            templet: '<div><a href="/article/{{ d.id }}" target="_blank" class="layui-table-link">{{ d.title }}</a></div>'
        }, {
            field: 'status', title: '状态', width: 100, align: 'center', templet: function (d) {
                if (d.status === 1) {
                    return '<span style="color: #FF5722;">加精</span>';
                } else if (d.status === -1) {
                    return '<span style="color: #ccc;">审核中</span>';
                } else {
                    return '<span style="color: #5FB878;">正常</span>'
                }
            }
        }
            /*
            ,{field: 'status', title: '结贴', width: 100, align: 'center', templet: function(d){
              return d.accept >= 0 ? '<span style="color: #5FB878;">已结</span>' : '<span style="color: #ccc;">未结</span>'
            }}
            */, {
                field: 'updated_at',
                title: '发表时间',
                width: 120,
                align: 'center',
                templet: '<div>{{ d.updated_at }}</div>'
            }, {
                title: '数据',
                width: 150,
                templet: '<div><span style="font-size: 12px;">{{d.click}}阅/{{d.comment}}答</span></div>'
            }, {
                title: '操作', width: 100, templet: function (d) {
                    if ((d.accept === 0)) {
                        return '<a class="layui-btn layui-btn-xs" href="/article/' + d.id + '/edit" target="_blank">编辑</a>'
                    } else {
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled">编辑</a>';
                    }
                }
            }]], page: true, skin: 'line'
    });

    //我收藏的帖
    if ($('#LAY_myCollectioncard')[0]) {
        fly.json('/api/user/favorite', function (res) {
            table.render({
                elem: '#LAY_myCollectioncard', data: layui.sort(res.rows, 'collection_timestamp', 'desc'), cols: [[{
                    field: 'title',
                    title: '帖子标题',
                    minWidth: 300,
                    templet: '<div><a href="/article/{{ d.id }}/" target="_blank" class="layui-table-link">{{ d.title }}</a></div>'
                }, {
                    field: 'collection_timestamp',
                    title: '收藏时间',
                    width: 120,
                    align: 'center',
                    templet: '<div>{{ layui.util.timeAgo(d.collection_timestamp, 1) }}</div>'
                }]], page: true, skin: 'line'
            });
        }, {type: 'get'});
    }

    // 充值
    table.render({
        elem: '#LAY_recharge',
        url: '/user/recharge',
        method: 'get',
        cols: [[
            {
                field: 'id',
                title: 'ID',
                width: 50,
            },
            {
                field: 'out_trade_no',
                title: '订单号',
                minWidth: 150,
            },
            {
                field: 'payment', title: '充值方式', minWidth: 150, align: 'center',  templet: function (d) {
                    if (d.payment === 4) {
                        return '<span>支付宝</span>';
                    } else {
                        return '<span>微信</span>'
                    }
                }
            },
            {
                field: 'amount',
                title: '充值金额',
                width: 100,
            },
            {
                field: 'status', title: '状态', width: 100, align: 'center', templet: function (d) {
                    if (d.status === 1) {
                        return '<span style="color: #FF5722;">充值成功</span>';
                    } else {
                        return '<span style="color: #ccc;">充值失败</span>'
                    }
                }
            },
            {
                field: 'create_time',
                title: '充值时间',
                width: 180,
                align: 'center',
                templet: '<div>{{ d.create_time }}</div>'
            },
            {
                title: '操作', width: 100, templet: function (d) {
                    if ((d.accept === 0)) {
                        return '<a class="layui-btn layui-btn-xs" href="/article/' + d.id + '/edit" target="_blank">编辑</a>'
                    } else {
                        return '<a class="layui-btn layui-btn-xs layui-btn-disabled">查看</a>';
                    }
                }
            }]], page: true, skin: 'line'
    });

    // 转账
    table.render({
        elem: '#LAY_transfer',
        url: '/user/transfer',
        method: 'get',
        cols: [[
            {
                field: 'id',
                title: 'ID',
                width: 50,
            },
            {
                field: 'out_trade_no',
                title: '订单号',
                minWidth: 150,
            },
            {
                field: 'payment', title: '转账方式', minWidth: 150, align: 'center',  templet: function (d) {
                    if (d.payment === 4) {
                        return '<span>支付宝</span>';
                    } else {
                        return '<span>微信</span>'
                    }
                }
            },
            {
                field: 'amount',
                title: '充值金额',
                width: 100,
            },
            {
                field: 'status', title: '状态', width: 100, align: 'center', templet: function (d) {
                    if (d.status === 1) {
                        return '<span style="color: #FF5722;">转账成功</span>';
                    } else {
                        return '<span style="color: #ccc;">转账失败</span>'
                    }
                }
            },
            {
                field: 'create_time',
                title: '充值时间',
                width: 180,
                align: 'center',
                templet: '<div>{{ d.create_time }}</div>'
            },
            {
                title: '操作', width: 100, templet: function (d) {
                    return '<a class="layui-btn layui-btn-xs" onclick="layer.msg(\''+ d.user_note + '\')" >查看</a>'
                }
            }]], page: true, skin: 'line'
    });

    table.render({
        elem: '#LAY_issue',
        url: '/user/issue',
        method: 'get',
        cols: [[
            {
                field: 'id',
                title: 'ID',
                width: 80,
            },
            {
                field: 'project_id',
                title: '工单类型',
                minWidth: 150,
            },
            {
                field: 'title',
                title: '工单标题',
                minWidth: 150,
            },
            {
                field: 'status', title: '状态', width: 100, align: 'center', templet: function (d) {
                    if (d.status === 1) {
                        return '<span style="color: #FF5722;">已处理</span>';
                    } else {
                        return '<span style="color: #ccc;">未处理</span>'
                    }
                }
            },
            {
                field: 'create_time',
                title: '提交时间',
                width: 180,
                align: 'center',
                templet: '<div>{{ d.create_time }}</div>'
            }]], page: true, skin: 'line'
    });

    //显示当前tab
    var showThisTab = function () {
            var router = layui.router();
            element.tabChange('user', router.path[0] || _layid);
        }, liItem = $('.layui-tab[lay-filter="user"]>.layui-tab-title'),
        _layid = liItem.children('li.layui-this').attr('lay-id');

    showThisTab();

    //根据 hash 切换 tab
    $(window).on('hashchange', showThisTab);

    //点击 tab 切换 hash
    //element.on('tab(user)', function(){
    liItem.children('li').on('click', function () {
        var othis = $(this), layid = othis.attr('lay-id');
        if (layid) location.hash = '/' + layid;
    });


    var gather = {}, dom = {
        mine: $('#LAY_mine'), mineview: $('.mine-view'), minemsg: $('#LAY_minemsg'), infobtn: $('#LAY_btninfo')
    };

    //根据ip获取城市
    if ($('#L_city').val() === '') {
        $.getScript('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js', function () {
            $('#L_city').val(remote_ip_info.city || '');
        });
    }

    //上传图片
    if ($('.upload-img')[0]) {
        layui.use('upload', function (upload) {
            var avatarAdd = $('.avatar-add');

            upload.render({
                elem: '.upload-img', url: '/api/upload/', size: 50, before: function () {
                    avatarAdd.find('.loading').show();
                }, done: function (res) {
                    if (res.status === 0) {
                        $.post('/api/user/set/', {
                            avatar: res.url
                        }, function (res) {
                            location.reload();
                        });
                    } else {
                        layer.msg(res.msg, {icon: 5});
                    }
                    avatarAdd.find('.loading').hide();
                }, error: function () {
                    avatarAdd.find('.loading').hide();
                }
            });
        });
    }

    //提交成功后刷新
    fly.form['set-mine'] = function (data, required) {
        layer.msg('修改成功', {
            icon: 1, time: 1000, shade: 0.1
        }, function () {
            location.reload();
        });
    }

    //帐号绑定
    $('.acc-unbind').on('click', function () {
        var othis = $(this), type = othis.attr('type');
        layer.confirm('确定要解绑' + ({
            qq_id: 'QQ', weibo_id: '微博'
        })[type] + '吗？', {icon: 5}, function () {
            fly.json('/api/unbind', {
                type: type
            }, function (res) {
                if (res.status === 0) {
                    layer.alert('已成功解绑。', {
                        icon: 1, end: function () {
                            location.reload();
                        }
                    });
                } else {
                    layer.msg(res.msg);
                }
            });
        });
    });


    /**
     * 模板渲染
     */

    //渲染视图
    var renderView = function () {
        var router = layui.router(), path = router.path, search = router.search, url = layui.url(),
                pathstr = (url.pathname.join('/') + '/').replace(/\/\/$/, '\/'), ID = 'FLY_TAB_BODY';

            //我的消息
            if (pathstr === 'user/message/') {
                view(ID).render('user/message/index', {
                    alias: url.search.alias || ''
                });
            }

            //我的产品
            if (pathstr === 'user/product/') {
                view(ID).render('user/product/index', {
                    alias: url.search.alias || ''
                });
            }

            //订单记录
            else if (pathstr === 'user/order/') {
                if (!path[0] || path[0] === 'log') {
                    view(ID).render(function () {
                        if (path.join('/') === 'log/apply') {
                            return 'user/order/log_apply';
                        }
                        return 'user/order/log'
                    }(), {
                        platform: platform
                    });
                }
                //订单抬头信息管理
                else if (path[0] === 'info') {
                    view(ID).render(function () {
                        if (path.join('/') === 'info/set') {
                            return 'user/order/info_set';
                        }
                        return 'user/order/info'
                    }(), {
                        platform: platform
                    });
                }
            }

            //我发布的组件
            else if (pathstr === 'user/extend/') {
                view(ID).render('user/extend/index');
            }
        }, platform = $('#FLY_TAB_BODY').data('platform');


    renderView();

    //hash 切换事件
    $(window).on('hashchange', function () {
        renderView();
    });


    exports('user', {});

});
