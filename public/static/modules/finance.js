layui.define(['table', 'form'], function (exports) {
    var $ = layui.$
        , admin = layui.admin
        , table = layui.table
        , form = layui.form;

    form.render(null, 'LAY-search-form');

    //监听搜索
    form.on('submit(LAY-search-submit)', function (data) {
        var field = data.field;

        //执行重载
        table.reload('LAY-finance-recharge', {
            where: field
        });
        table.reload('LAY-finance-transfer', {
            where: field
        });
        table.reload('LAY-finance-balance', {
            where: field
        });
    });

    //充值管理
    table.render({
        elem: '#LAY-finance-recharge'
        , url: 'recharge'
        , cols: [[
            {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'user_id', title: '用户ID'}
            , {field: 'out_trade_no', title: '订单号'}
            , {field: 'amount', title: '金额'}
            , {field: 'payment', title: '方式'}
            , {field: 'status', title: '状态', templet: '#buttonTpl', minWidth: 80, align: 'center'}
            , {field: 'create_time', title: '创建时间', sort: true}
            , {field: 'update_time', title: '完成时间', sort: true}
        ]]
        , text: '对不起，加载出现异常！'
    });

    //转账管理
    table.render({
        elem: '#LAY-finance-transfer'
        , url: 'transfer'
        , cols: [[
            {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'user_id', title: '用户ID'}
            , {field: 'out_trade_no', title: '订单号'}
            , {field: 'amount', title: '金额'}
            , {field: 'payment', title: '方式'}
            , {field: 'user_note', title: '备注'}
            , {field: 'status', title: '状态', templet: '#buttonTpl', minWidth: 80, align: 'center'}
            , {field: 'create_time', title: '创建时间', sort: true}
            , {field: 'update_time', title: '完成时间', sort: true}
        ]]
        , text: '对不起，加载出现异常！'
    });

    //资金明细
    table.render({
        elem: '#LAY-finance-balance'
        , url: 'user_balance_log'
        , cols: [[
            {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'user_id', title: '用户ID'}
            , {field: 'type', title: '变动类型', templet: '#buttonTpl', minWidth: 80, align: 'center'}
            , {field: 'old_amount', title: '变动前金额'}
            , {field: 'amount', title: '变动金额'}
            , {field: 'now_amount', title: '变动后金额'}
            , {field: 'remark', title: '备注信息'}
            , {field: 'create_time', title: '变动时间', sort: true}
        ]]
        , text: '对不起，加载出现异常！'
    });

    exports('finance', {})
});
