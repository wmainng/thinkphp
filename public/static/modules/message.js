layui.define(['admin', 'table', 'util'], function(exports){
    var $ = layui.$
        ,admin = layui.admin
        ,table = layui.table
        ,element = layui.element;

    var DISABLED = 'layui-btn-disabled'

        //区分各选项卡中的表格
        ,tabs = {
            all: {
                text: '全部消息'
                ,id: 'LAY-app-message-all'
            }
            ,notice: {
                text: '通知'
                ,id: 'LAY-app-message-notice'
            }
            ,direct: {
                text: '私信'
                ,id: 'LAY-app-message-direct'
            }
        };

    //标题内容模板
    var tplTitle = function(d){
        return '<a href="message/'+ d.id +'/edit">'+ d.title;
    };

    //全部消息
    table.render({
        elem: '#LAY-app-message-all'
        ,url: 'message'
        ,page: true
        ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'title', title: '标题', width: 300}
            ,{field: 'content', title: '内容', minWidth: 300, templet: tplTitle}
            ,{field: 'create_time', title: '时间', width: 170}
        ]]
        ,skin: 'line'
    });

    //通知
    table.render({
        elem: '#LAY-app-message-notice'
        ,url: 'message?type=0'
        ,page: true
        ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'title', title: '标题', width: 300}
            ,{field: 'content', title: '内容', minWidth: 300, templet: tplTitle}
            ,{field: 'create_time', title: '时间', width: 170}
        ]]
        ,skin: 'line'
    });

    //私信
    table.render({
        elem: '#LAY-app-message-direct'
        ,url: 'message?type=1'
        ,page: true
        ,cols: [[
            {type: 'checkbox', fixed: 'left'}
            ,{field: 'title', title: '标题', width: 300}
            ,{field: 'content', title: '内容', minWidth: 300, templet: tplTitle}
            ,{field: 'create_time', title: '时间', width: 170}
        ]]
        ,skin: 'line'
    });

    //事件处理
    var events = {
        del: function(othis, type){
            var thisTabs = tabs[type]
                ,checkStatus = table.checkStatus(thisTabs.id)
                ,data = checkStatus.data; //获得选中的数据
            if(data.length === 0) return layer.msg('未选中行');

            layer.confirm('确定删除选中的数据吗？', function(){
                admin.req({
                    url: 'message'
                    , data: {ids: data.map(x => {return x.id})}
                    , type: "DELETE"
                    , done: function (res) {
                        if (res.code === 0) {
                            layer.msg(res.message, {icon: 6});
                            table.reload(thisTabs.id);
                        } else {
                            layer.msg(res.message, {icon: 5});
                        }
                    }
                });
            });
        }
        ,ready: function(othis, type){
            var thisTabs = tabs[type]
                ,checkStatus = table.checkStatus(thisTabs.id)
                ,data = checkStatus.data; //获得选中的数据
            if(data.length === 0) return layer.msg('未选中行');

            admin.req({
                url: 'message/status'
                , data: {ids: data.map(x => {return x.id})}
                , type: "PUT"
                , done: function (res) {
                    if (res.code === 0) {
                        layer.msg('标记已读成功', {
                            icon: 1
                        });
                        table.reload(thisTabs.id);
                    } else {
                        layer.msg(res.message, {icon: 5});
                    }
                }
            });
        }
        ,readyAll: function(othis, type){
            var thisTabs = tabs[type];

            admin.req({
                url: 'message/status'
                , type: "PUT"
                , done: function (res) {
                    if (res.code === 0) {
                        layer.msg(thisTabs.text + '：全部已读', {
                            icon: 1
                        });
                        table.reload(thisTabs.id);
                    } else {
                        layer.msg(res.message, {icon: 5});
                    }
                }
            });
        }
    };

    $('.LAY-app-message-btns .layui-btn').on('click', function(){
        var othis = $(this)
            ,thisEvent = othis.data('events')
            ,type = othis.data('type');
        events[thisEvent] && events[thisEvent].call(this, othis, type);
    });

    exports('message', {});
});