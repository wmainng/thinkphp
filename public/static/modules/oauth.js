layui.define(['table', 'form'], function (exports) {
    var $ = layui.$
        , admin = layui.admin
        , table = layui.table
        , form = layui.form;

    //角色管理
    table.render({
        elem: '#LAY-user-oauth-message'
        , url: 'oauth'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'third_party', title: '来源'}
            , {field: 'openid', title: '绑定账号'}
            , {field: 'create_time', title: '首次登录时间'}
            , {field: 'update_time', title: '最后登录时间'}
            , {field: 'login_times', title: '登录次数'}
            , {title: '操作', width: 200, align: 'center', fixed: 'right', toolbar: '#LAY-user-oauth-bar'}
        ]]
        , text: '对不起，加载出现异常！'
    });

    //搜索角色
    form.on('select(LAY-user-adminrole-type)', function (data) {
        //执行重载
        table.reload('LAY-user-role', {
            where: {
                role: data.value
            }
        });
    });

    //事件
    var active = {
        batchdel: function () {
            var checkStatus = table.checkStatus('LAY-user-role')
                , checkData = checkStatus.data; //得到选中的数据

            if (checkData.length === 0) {
                return layer.msg('请选择数据');
            }

            layer.confirm('确定删除吗？', function (index) {

                //执行 Ajax 后重载
                /*
                admin.req({
                  url: 'xxx'
                  //,……
                });
                */
                table.reload('LAY-user-back-role');
                layer.msg('已删除');
            });
        },
        add: function () {
            layer.open({
                type: 2
                , title: '添加新角色'
                , content: 'role/create'
                , area: ['500px', '480px']
                , btn: ['确定', '取消']
                , yes: function (index, layero) {
                    var iframeWindow = window['layui-layer-iframe' + index]
                        , submit = layero.find('iframe').contents().find("#LAY-user-role-submit");

                    //监听提交
                    iframeWindow.layui.form.on('submit(LAY-user-role-submit)', function (data) {
                        var field = data.field; //获取提交的字段

                        //提交 Ajax 成功后，静态更新表格中的数据
                        //$.ajax({});
                        table.reload('LAY-user-back-role');
                        layer.close(index); //关闭弹层
                    });

                    submit.trigger('click');
                }
            });
        }
    }
    $('.layui-btn.layuiadmin-btn-role').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

    //监听工具条
    table.on('tool(LAY-user-role)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('role/' + data.id, obj);
        } else if (obj.event === 'edit') {
            edit('编辑角色', 'role/' + data.id, '500px', '480px', 'LAY-user-role', obj);
        } else if (obj.event === 'permission') {
            layer.open({
                type: 2
                ,title: '分配权限'
                ,content: '/role/'+data.id+'/permission'
                ,area: ['500px', '420px']
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    const iframeWindow = window['layui-layer-iframe' + index]
                        , submitID = 'submit'
                        , submit = layero.find('iframe').contents().find('#' + submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+ submitID +')', function(data){
                        const field = data.field; //获取提交的字段
                        $.ajax({
                            url:data.form.action,
                            data:field,
                            type:"put",
                            dataType:"json",
                            success:function(res){
                                if(res.code===0){
                                    layer.msg(res.msg, {icon: 6});
                                    layer.close(index); //关闭弹层
                                }else{
                                    layer.msg(res.msg, {icon: 5});
                                }
                            },
                            error:function(res){
                                layer.msg(res.msg);
                            }
                        });
                    });

                    submit.trigger('click');
                }
            })
        }
    });

    exports('oauth', {})
});