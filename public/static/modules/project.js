layui.define(['table', 'form', 'element'], function (exports) {
    var $ = layui.$
        , table = layui.table
        , form = layui.form
        , element = layui.element;

    table.render({
        elem: '#LAY-app-project-list'
        , url: 'project'
        , cols: [[
            {field: 'id', width: 100, title: '项目编号', sort: true}
            , {field: 'name', width: 300, title: '项目名称'}
            , {field: 'user_id', width: 100, title: '负责人'}
            , {field: 'status', title: '项目状态', templet: '#buttonTpl', minWidth: 80, align: 'center'}
            , {title: '操作', align: 'center', fixed: 'right', toolbar: '#LAY-app-project-bar'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
        , done: function () {
            element.render('progress');
        }
    });

    //监听工具条
    table.on('tool(LAY-app-project-list)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('project/' + data.id, obj);
        } else if (obj.event === 'edit') {
            edit('编辑项目', 'project/' + data.id, '400px', '300px', 'LAY-app-project', obj);
        } else if (obj.event === 'item') {
            open('任务', 'project/' + data.id + '/issue', '1000px', '600px', true)
        }
    });

    //项目任务
    table.render({
        elem: '#LAY-app-project-issue'
        , url: 'issue'
        , cols: [[
            {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'type', width: 70, title: '类型', align: 'center', templet: '#typeTpl'}
            , {field: 'title', title: '标题'}
            , {field: 'progress', title: '进度', width: 120, align: 'center', templet: '#progressTpl'}
            , {field: 'hour', title: '计划时间', width: 90}
            , {field: 'create_time', title: '创建时间', sort: true}
            , {field: 'state', title: '状态', templet: '#buttonTpl', width: 80, align: 'center'}
            , {title: '操作', align: 'center', fixed: 'right', toolbar: '#LAY-app-project-issue-bar'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
        , done: function () {
            element.render('progress');
        }
    });

    //监听工具条
    table.on('tool(LAY-app-project-issue)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('issue/' + data.id, obj);
        } else if (obj.event === 'edit') {
            edit('编辑任务', 'issue/' + data.id, '550px', '550px', 'LAY-app-project-issue', obj);
        }
    });

    exports('project', {})
});
