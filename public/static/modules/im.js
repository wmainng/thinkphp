layui.define(['layer', 'layim'], function (exports) {
    var $ = layui.$
        , layer = layui.layer
        , setter = layui.setter
        , layim = layui.layim;

    var $body = $('body');

    // WebSocket
    const data = layui.data(setter.tableName);
    const socket = new WebSocket('wss://bengbu.link/wss');

    // // WebSocket
    // const socket = new WebSocket("wss://bengbu.link/wss");
    //
    // //连接成功时触发
    // socket.onopen = function () {
    //     console.log('连接成功');
    // };
    //
    // //触发接受的消息
    // socket.onmessage = function (e) {
    //     let res = JSON.parse(e.data);
    //     console.log(res);
    //     switch (res.type) {
    //         case 'init':
    //             socket.send(JSON.stringify({
    //                 type: 'bind',
    //                 uid: 1
    //             }));
    //             break;
    //
    //         case 'message':
    //             // 收到消息
    //             document.getElementById("notice").className = "layui-badge-dot";
    //             //socket.send(JSON.stringify({
    //             //    type: 'message',
    //             //    data: {"mine":{"id":"1","username":"wm","avatar":"","content":"hello","mine":"true"},"to":{"id":"1","username":"wm","avatar":"","type":"friend"}}
    //             //}));
    //             break;
    //
    //         case 'ping':
    //             // 回应心跳
    //             socket.send(JSON.stringify({
    //                 type: 'pang'
    //             }));
    //             break;
    //
    //         default :
    //             console.log(e.data);
    //     }
    // };
    //
    // //连接关闭时触发
    // socket.onclose = function () {
    //     console.log('连接关闭');
    // };
    //
    // //连接出错时触发
    // socket.onerror = function () {
    //     console.log('连接出错');
    // };


    //连接成功时触发
    socket.onopen = function () {
        // 建立通信
        socket.send(JSON.stringify({
            type: 'online',
            data: {id: data.user.id}
        }));
    };

    //触发接受的消息
    socket.onmessage = function (res) {
        var data = JSON.parse(res.data);
        console.log(data);

        switch(data.type){
            //用户好友
            case 'setFriendAll':
                for (const i in data.data) {
                    if (data.data[i].status === 0) {
                        layim.setFriendStatus(data.data[i].id, 'offline')
                    }
                }
                break;

            //接受消息
            case 'message':
                layim.setFriendStatus(data.data.id, 'online');
                layim.setChatStatus('<span style="color:#FF5722;">在线</span>');
                layim.getMessage(data.data);
                break;

            //上线用户
            case 'online':
                layim.setFriendStatus(data.data.id, 'online');
                layim.setChatStatus('<span style="color:#FF5722;">在线</span>');
                break;

            //下线用户
            case 'offline':
                layim.setFriendStatus(data.data.id, 'offline');
                layim.setChatStatus('<span style="color:#808080;">离线</span>');
                break;

            //添加好友/群
            case 'addList':
                //添加好友/群到主面板
                layim.addList(data.data);
                break;

            //删除好友
            case 'removeList':
                layim.removeList({
                    type: 'friend' //或者group
                    , id: data.user_id //好友或者群组ID
                });
                break;
        }

    };

    //连接关闭时触发
    socket.onclose = function () {
        //询问框
        layer.confirm('长时间未操作连接关闭,是否确定重新连接？', {
            btn: ['确定'] //按钮
        }, function () {
            window.location.reload();
        });
    };

    //连接出错时触发
    socket.onerror = function () {
        layer.alert('连接出错');
    };

    //基础配置
    layim.config({
        //初始化接口
        init: {
            url: setter.host + 'im/getList'
            , data: {}
        }
        //查看群员接口
        , members: {
            url: setter.host + 'im/getMembers'
            , data: {}
        }
        , uploadImage: {
            url: '' //（返回的数据格式见下文）
            , type: '' //默认post
        }
        , uploadFile: {
            url: '' //（返回的数据格式见下文）
            , type: '' //默认post
        }
        , isAudio: true //开启聊天工具栏音频
        , isVideo: true //开启聊天工具栏视频

        //扩展工具栏
        , tool: [{
            alias: 'code'
            , title: '代码'
            , icon: '&#xe64e;'
        }]

        //,brief: true //是否简约模式（若开启则不显示主面板）

        //,title: 'WebIM' //自定义主面板最小化时的标题
        //,right: '100px' //主面板相对浏览器右侧距离
        //,minRight: '90px' //聊天面板最小化时相对浏览器右侧距离
        , initSkin: '5.jpg' //1-5 设置初始背景
        //,skin: ['aaa.jpg'] //新增皮肤
        //,isfriend: false //是否开启好友
        //,isgroup: false //是否开启群组
        //,min: true //是否始终最小化主面板，默认false
        //,notice: true //是否开启桌面消息提醒，默认false
        //,voice: false //声音提醒，默认开启，声音文件为：default.mp3

        , msgbox: layui.cache.layimAssetsPath + 'html/msgbox.html' //消息盒子页面地址，若不开启，剔除该项即可
        , find: layui.cache.layimAssetsPath + 'html/find.html' //发现页面地址，若不开启，剔除该项即可
        , chatLog: layui.cache.layimAssetsPath + 'html/chatlog.html' //聊天记录页面地址，若不开启，剔除该项即可

    });

    //监听自定义工具栏点击，以添加代码为例
    layim.on('tool(code)', function (insert) {
        layer.prompt({
            title: '插入代码 - 工具栏扩展示例'
            , formType: 2
            , shade: 0
        }, function (text, index) {
            layer.close(index);
            insert('[pre class=layui-code]' + text + '[/pre]'); //将内容插入到编辑器
        });
    });

    //监听layim建立就绪
    layim.on('ready', function (res) {
        //检查有无未读消息
        //1 异步提交的urL,2 form表单以键值对形式传输,3 访问后成功的回调函数
        // $.get("/api/im/getMessages", {read: 0}, function (res) {
        //     if (res.code !== 0) {
        //         return layer.msg(res.msg);
        //     }
        //     if (res.data.length !== 0) {
        //         //发送未读消息
        //         layui.each(res.data, function (index, item) {
        //             layim.getMessage(item);
        //         });
        //
        //         //消息设置已读
        //         $.post("readMsg", '', function (data) {});
        //     }
        // });

        // 消息盒子有新消息
        if (res.mine.count !== 0) {
            layim.msgbox(res.mine.count);
        }

    });

    //监听在线状态的切换事件
    layim.on('online', function (status) {
        const type = status === 'online' ? 'online' : 'offline';

        // 发送消息
        socket.send(JSON.stringify({
            type: type,
            data: {id: data.user.id}
        }));

        //此时，你就可以通过Ajax将这个状态值记录到数据库中了。
        //$.post("setOnline", {status: status}, function (data) {
        //    layer.msg(data.data);
        //});
    });

    //监听签名修改
    layim.on('sign', function (value) {
        //$.post("setSign", {sign: value}, function (data) {
            //console.log('签名'+data);
        //});
    });

    //监听更换背景皮肤
    layim.on('setSkin', function (filename, src) {
        //console.log(filename); //获得文件名，如：1.jpg
        //console.log(src); //获得背景路径，如：http://res.layui.com/layui/src/css/modules/layim/skin/1.jpg
    });

    //监听发送消息
    layim.on('sendMessage', function (data) {
        // 发送消息
        socket.send(JSON.stringify({
            "type": 'message',
            "data": data
        }));
    });

    //监听查看群员
    layim.on('members', function (data) {
        //console.log(data);
    });

    //监听聊天窗口的切换
    layim.on('chatChange', function (res) {
        //更新当前会话状态
        var type = res.data.type;
        console.log(res.data)
        if (type === 'friend') {
            //模拟标注好友状态
            if (res.data.status) {
                layim.setChatStatus('<span style="color:#FF5722;">在线</span>');
            } else {
                layim.setChatStatus('<span style="color:#808080;">离线</span>');
            }
        } else if (type === 'group') {
            //模拟系统消息
            layim.getMessage({
                system: true
                , id: res.data.id
                , type: "group"
                , content: '模拟群员' + (Math.random() * 100 | 0) + '加入群聊'
            });
        }
    });

    //自定义事件
    $body.on('click', '.layui-layim-user', function (e) {
        // 只能点击一次
        const that = $(this);
        if(that.hasClass("on")) {
        	return;
        }
        that.addClass("on");
        //注意:不能使用yes或success,获取不到form
        layer.open({
            type: 2
            , title: '我的资料'
            , maxmin: false
            , area: ['400px', '500px']
            , content: layui.cache.layimAssetsPath + 'html/set.html'
            , btn: ['确定', '取消']
            , cancel: function () {
                that.removeClass("on");
            }
        });
    });

    let i = 0;
    $body.on('mousedown', '.layui-show img', function (e) {
        const str = $(this).parent("li").attr('class'),
            group = str.substr(6, 1) === 'f' ? 0 : 1,
            uid = group ? str.substr(12) : str.substr(11),
            username = $(this).next().text(),
            sign = $(this).next().next().text();
        // 右键
        if (i === 0 && e.which === 3) {
            i = 1;
            window.data = {id: uid, username: username, sign: sign};
            layer.open({
                type: 2
                , title: username
                , shade: false
                , maxmin: false
                , area: ['294px', '198px']
                , skin: 'layui-box layui-layer-border'
                , resize: true
                , content: layui.cache.layimAssetsPath + 'html/info.html'
                , cancel: function () {
                    i = 0;
                }
            });
        }
    });

    exports('im', {});
});