layui.define(['table', 'form'], function (exports) {
    var $ = layui.$
        , admin = layui.admin
        , view = layui.view
        , table = layui.table
        , form = layui.form;

    //区块类型
    table.render({
        elem: '#LAY-set-wechat-menu'
        , url: "wechat_menu"
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'name', title: '类型名称', minWidth: 100}
            , {field: 'sort', title: '排序', minWidth: 100}
            , {field: 'create_time', title: '创建时间', minWidth: 100, sort: true}
            , {field: 'update_time', title: '更新时间', minWidth: 100, sort: true}
            , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#LAY-set-wechat-menu-bar'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-set-wechat-menu)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('wechat_menu/' + data.id, obj);
        } else if (obj.event === 'edit') {
            edit('编辑自定义菜单', 'wechat_menu/' + data.id, '600px', '400px', 'LAY-set-wechat-menu', obj);
        }
    });

    //区块管理
    table.render({
        elem: '#LAY-app-content-block'
        , url: "block"
        , cols: [[
            {checkbox: true, fixed: true}
            , {field: 'id', title: 'ID', sort: true, width: 60}
            , {field: 'block_type_id', title: '类型', toolbar: '#position'}
            , {field: 'name', title: '名称'}
            , {field: 'title', title: '标题'}
            , {field: 'image', title: '图片', toolbar: '#thumb'}
            , {field: 'url', title: '链接', width: 200}
            , {field: 'sort', title: '排序', width: 60}
            , {field: 'created_at', title: '创建时间'}
            , {field: 'updated_at', title: '更新时间'}
            , {fixed: 'right', title: '操作', width: 150, align: 'center', toolbar: '#options'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-app-content-block)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('确定删除此条类型？', function (index) {
                $.post("block/destroy", {_method: 'delete', ids: data.id}, function (res) {
                    if (res.code === 0) {
                        obj.del(); //删除对应行（tr）的DOM结构
                        layer.close(index);
                        layer.msg(data.msg, {icon: 6});
                    } else {
                        layer.msg(data.msg, {icon: 5});
                    }
                });
            });
        } else if (obj.event === 'edit') {
            layer.open({
                type: 2
                , title: '编辑类型'
                , content: 'block/' + data.id + '/edit'
                , area: ['450px', '300px']
                , btn: ['确定', '取消']
                , yes: function (index, layero) {
                    //点击确认触发 iframe 内容中的按钮提交
                    var submit = layero.find('iframe').contents().find("#layuiadmin-app-form-submit");
                    submit.click();
                }
            });
        }
    });

    exports('wechat', {})
});