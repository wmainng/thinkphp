layui.define(['table', 'form'], function (exports) {
    var $ = layui.$
        , admin = layui.admin
        , table = layui.table
        , form = layui.form;

    //权限管理
    table.render({
        elem: '#LAY-user-permission'
        , url: 'permission'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'name', title: '名称'}
            , {field: 'title', title: '标题'}
            , {field: 'route', title: '路由'}
            , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#LAY-user-permission-bar'}
        ]]
        , page: true
        , limit: 10
        , height: 'full-320'
        , text: '对不起，加载出现异常！'
    });

    //监听搜索
    form.on('submit(search)', function (data) {
        var field = data.field;

        //执行重载
        table.reload('datatable', {
            where: field
        });
    });

    //事件
    var active = {
        batchdel: function () {
            var checkStatus = table.checkStatus('LAY-user-permission')
                , checkData = checkStatus.data; //得到选中的数据

            if (checkData.length === 0) {
                return layer.msg('请选择数据');
            }

            layer.confirm('确定删除吗？', function (index) {
                admin.req({
                    url: 'permission'
                    , data: {ids: checkData.map(x => {return x.id})}
                    , type: "DELETE"
                    , done: function (res) {
                        if (res.code === 0) {
                            layer.msg(res.message, {icon: 6});
                            table.reload('LAY-user-permission');
                            layer.close(index);
                        } else {
                            layer.msg(res.message, {icon: 5});
                        }
                    }
                });
            });
        }
        , add: function () {
            layer.open({
                type: 2
                ,title: '添加权限'
                ,content: 'permission/create'
                ,maxmin: true
                ,area: ['450px', '500px']
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-user-permission-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);
                    //监听提交
                    iframeWindow.layui.form.on('submit('+ submitID +')', function (submit) {
                        admin.req({
                            url: 'permission'
                            , data: submit.field
                            , type: "POST"
                            , done: function (res) {
                                if (res.code === 0) {
                                    layer.msg(res.message, {icon: 6});
                                    table.reload('LAY-user-permission'); //数据刷新
                                    layer.close(index); //关闭弹层
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                            }
                        });
                    });
                    submit.trigger('click');
                }
            });
        }
    }
    $('.layui-btn.layuiadmin-btn-permission').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

    //监听工具条
    table.on('tool(LAY-user-permission)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('真的删除行么', function (index) {
                admin.req({
                    url: 'permission/' + data.id
                    , type: "delete"
                    , done: function (res) {
                        obj.del();
                        layer.close(index);
                    }
                });
            });
        } else if (obj.event === 'edit') {
            layer.open({
                type: 2
                ,title: '编辑权限'
                ,content: 'permission/' + data.id + '/edit'
                ,area: ['500px', '480px']
                ,btn: ['确定', '取消']
                ,resize: false
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-user-permission-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+ submitID +')', function(submit){
                        admin.req({
                            url: 'permission/' + data.id
                            , data: submit.field
                            , type: "PUT"
                            , done: function (res) {
                                if (!res.code) {
                                    layer.msg(res.message, {icon: 6});
                                    obj.update(submit.field);
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                                layer.close(index);
                            }
                        });
                    });

                    submit.trigger('click');
                }
            });
        }
    });

    exports('permission', {})
});