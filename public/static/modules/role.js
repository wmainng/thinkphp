layui.define(['table', 'form'], function (exports) {
    var $ = layui.$
        , admin = layui.admin
        , table = layui.table
        , form = layui.form;

    const tableID = 'LAY-user-role-manage';

    //角色管理
    table.render({
        elem: '#LAY-user-role-manage'
        , url: 'role'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'name', width:250, title: '角色名称'}
            , {field: 'remark', title: '角色描述'}
            , {field: 'status', width:80, title: '状态', toolbar: '#buttonTpl'}
            , {title: '操作', width: 250, align: 'center', fixed: 'right', toolbar: '#LAY-user-role-bar'}
        ]]
        , text: '对不起，加载出现异常！'
    });

    //事件
    var active = {
        batchdel: function () {
            var checkStatus = table.checkStatus(tableID)
                , checkData = checkStatus.data; //得到选中的数据

            if (checkData.length === 0) {
                return layer.msg('请选择数据');
            }

            layer.confirm('确定删除吗？', function (index) {
                admin.req({
                    url: 'role'
                    , data: {ids: checkData.map(x => {return x.id})}
                    , type: "DELETE"
                    , done: function (res) {
                        if (res.code === 0) {
                            layer.msg(res.message, {icon: 6});
                            table.reload(tableID);
                            layer.close(index);
                        } else {
                            layer.msg(res.message, {icon: 5});
                        }
                    }
                });
            });
        },
        add: function () {
            layer.open({
                type: 2
                , title: '添加角色'
                , content: 'role/create'
                , area: ['450px', '380px']
                , btn: ['确定', '取消']
                , yes: function (index, layero) {
                    var iframeWindow = window['layui-layer-iframe' + index]
                        , submit = layero.find('iframe').contents().find("#LAY-user-role-submit");

                    //监听提交
                    iframeWindow.layui.form.on('submit(LAY-user-role-submit)', function (data) {
                        admin.req({
                            url: 'role'
                            , data: data.field
                            , type: "POST"
                            , done: function (res) {
                                if (res.code === 0) {
                                    layer.msg(res.message, {icon: 6});
                                    table.reload('LAY-user-role-manage'); //数据刷新
                                    layer.close(index); //关闭弹层
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                            }
                        });
                    });

                    submit.trigger('click');
                }
            });
        }
    }
    $('.layui-btn.layuiadmin-btn-role').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

    //监听工具条
    table.on('tool(LAY-user-role-manage)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('真的删除行么', function (index) {
                admin.req({
                    url: 'role/' + data.id
                    , type: "delete"
                    , done: function (res) {
                        obj.del();
                        layer.close(index);
                    }
                });
            });
        } else if (obj.event === 'edit') {
            layer.open({
                type: 2
                ,title: '编辑角色'
                ,content: 'role/' + data.id + '/edit'
                ,area: ['450px', '380px']
                ,btn: ['确定', '取消']
                ,resize: false
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-user-role-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+ submitID +')', function(submit){
                        admin.req({
                            url: 'role/' + data.id
                            , data: submit.field
                            , type: "PUT"
                            , done: function (res) {
                                if (!res.code) {
                                    layer.msg(res.message, {icon: 6});
                                    obj.update(submit.field);
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                                layer.close(index);
                            }
                        });
                    });

                    submit.trigger('click');
                }
            });
        } else if (obj.event === 'permission') {
            layer.open({
                type: 2
                ,title: '分配权限'
                ,content: 'role/'+data.id+'/permission'
                ,area: ['500px', '420px']
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    const iframeWindow = window['layui-layer-iframe' + index]
                        , submitID = 'submit'
                        , submit = layero.find('iframe').contents().find('#' + submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+ submitID +')', function(submit){
                        $.ajax({
                            url: 'role/'+data.id+'/permission',
                            data: submit.field,
                            type: "put",
                            dataType:"json",
                            success:function(res){
                                if(res.code===0){
                                    layer.msg(res.message, {icon: 6});
                                    layer.close(index); //关闭弹层
                                }else{
                                    layer.msg(res.message, {icon: 5});
                                }
                            },
                            error:function(res){
                                layer.msg(res.message);
                            }
                        });
                    });

                    submit.trigger('click');
                }
            })
        }
    });

    exports('role', {})
});