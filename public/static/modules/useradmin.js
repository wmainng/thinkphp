layui.define(['table', 'form'], function (exports) {
    var $ = layui.$
        , admin = layui.admin
        , table = layui.table
        , form = layui.form;

    //管理员管理
    table.render({
        elem: '#LAY-user-admin-manage'
        , url: 'admin'
        , cols: [[
            {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'username', title: '用户名'}
            , {field: 'email', title: '邮箱'}
            , {field: 'mobile', title: '手机'}
            , {field: 'last_ip', title: '最后登录IP', sort: true}
            , {field: 'last_time', title: '最后登录时间', sort: true}
            , {field: 'status', title: '状态', templet: '#buttonTpl', minWidth: 80, align: 'center'}
            , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#LAY-user-admin-bar'}
        ]]
        , text: '对不起，加载出现异常！'
    });

    //监听搜索
    form.on('submit(LAY-user-admin-search)', function(data){
        var field = data.field;

        //执行重载
        table.reload('LAY-user-admin-manage', {
            where: field
        });
    });

    //事件
    var active = {
        add: function(){
            layer.open({
                type: 2
                ,title: '添加管理员'
                ,content: 'admin/create'
                ,area: ['450px', '450px']
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-user-admin-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+ submitID +')', function(data){
                        admin.req({
                            url: 'admin'
                            , data: data.field
                            , type: "POST"
                            , done: function (res) {
                                if (res.code === 0) {
                                    layer.msg(res.message, {icon: 6});
                                    table.reload('LAY-user-front-submit'); //数据刷新
                                    layer.close(index); //关闭弹层
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                            }
                        });
                    });

                    submit.trigger('click');
                }
            });
        }
    }
    $('.layui-btn.layuiadmin-btn-admin').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

    //监听工具条
    table.on('tool(LAY-user-admin-manage)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.prompt({
                formType: 1
                , title: '敏感操作，请验证口令'
            }, function (value, index) {
                layer.close(index);
                layer.confirm('确定删除此管理员？', function (index) {
                    admin.req({
                        url: 'admin/' + data.id
                        , type: "delete"
                        , done: function (res) {
                            obj.del();
                            layer.close(index);
                        }
                    });
                });
            });
        } else if (obj.event === 'edit') {
            layer.open({
                type: 2
                ,title: '编辑管理员'
                ,content: 'admin/' + data.id + '/edit'
                ,area: ['450px', '450px']
                ,btn: ['确定', '取消']
                ,resize: false
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-user-admin-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+ submitID +')', function(submit){
                        admin.req({
                            url: 'admin/' + data.id
                            , data: submit.field
                            , type: "PUT"
                            , done: function (res) {
                                if (!res.code) {
                                    layer.msg(res.message, {icon: 6});
                                    obj.update(submit.field);
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                                layer.close(index);
                            }
                        });
                    });

                    submit.trigger('click');
                }
            });
        }
    });

    exports('useradmin', {})
});
