layui.define(['table', 'form'], function (exports) {
    var $ = layui.$
        , table = layui.table
        , form = layui.form;

    //导航管理
    table.render({
        elem: '#LAY-set-nav-list'
        , url: 'nav'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 100, title: 'ID', sort: true}
            , {field: 'name', title: '名称', minWidth: 100}
            , {field: 'sort', width: 80, title: '排序'}
            , {field: 'status', title: '状态', templet: '#statusTpl', width: 80, align: 'center'}
            , {field: 'create_time', title: '创建时间', sort: true}
            , {title: '操作', width: 250, align: 'center', fixed: 'right', toolbar: '#LAY-set-nav-bar'}
        ]]
        , page: true
        , limit: 30
        , height: 'full-320'
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-set-nav-list)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('nav/' + data.id, obj);
        } else if (obj.event === 'edit') {
            edit('编辑导航', 'nav/' + data.id, '400px', '300px', 'LAY-set-nav-list', obj);
        } else if (obj.event === 'item') {
            open('菜单', 'nav/' + data.id + '/menu', '900px', '500px', true)
        }
    });

    //导航菜单管理
    table.render({
        elem: '#LAY-set-nav-menu'
        , url: 'menu'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 100, title: 'ID', sort: true}
            , {field: 'name', title: '菜单名', minWidth: 100}
            , {field: 'icon', title: '图标', width: 100, templet: '#imgTpl'}
            , {field: 'url', title: '地址'}
            , {field: 'sort', width: 80, title: '排序'}
            , {field: 'status', title: '状态', templet: '#statusTpl', width: 80, align: 'center'}
            , {field: 'create_time', title: '创建时间', sort: true}
            , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#LAY-set-nav-menu-bar'}
        ]]
        , page: true
        , limit: 30
        , height: 'full-320'
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-set-nav-menu)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('menu/' + data.id, obj);
        } else if (obj.event === 'edit') {
            edit('编辑菜单', 'menu/' + data.id, '600px', '400px', 'LAY-set-nav-menu', obj);
        }
    });

    exports('nav', {})
});