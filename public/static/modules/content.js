layui.define(['table', 'form'], function (exports) {
    var $ = layui.$
        , table = layui.table
        , form = layui.form
        , admin = layui.admin;

    form.render(null, 'app-content-search');

    //监听搜索
    form.on('submit(LAY-app-content-search)', function (data) {
        var field = data.field;

        //执行重载
        table.reload('LAY-app-content-list', {
            where: field
        });
    });

    const active = {
        add: function () {
            layer.open({
                type: 2
                , title: '添加分类'
                , content: 'article_category/create'
                , area: ['700px', '600px']
                , btn: ['确定', '取消']
                , yes: function (index, layero) {
                    var iframeWindow = window['layui-layer-iframe' + index]
                        , submitID = 'LAY-app-content-category-submit'
                        , submit = layero.find('iframe').contents().find('#' + submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit(' + submitID + ')', function (formData) {
                        $.ajax({
                            url: 'article_category',
                            data: formData.field,
                            type: "post",
                            dataType: "json",
                            success: function (res) {
                                if (res.code === 0) {
                                    layer.msg(res.message, {icon: 6});
                                    table.reload('LAY-app-content-category-list');
                                    layer.close(index); //关闭弹层
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                            }
                        });
                    });

                    submit.trigger('click');
                }
            });
        },
        addPage: function () {
            layer.open({
                type: 2
                , title: '添加页面'
                , content: 'page/create'
                , maxmin: true
                , area: ['1000px', '600px']
                , btn: ['确定', '取消']
                , yes: function (index, layero) {
                    var iframeWindow = window['layui-layer-iframe' + index]
                        , submitID = 'LAY-app-content-page-submit'
                        , submit = layero.find('iframe').contents().find('#' + submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit(' + submitID + ')', function (formData) {
                        $.ajax({
                            url: 'page',
                            data: formData.field,
                            type: "post",
                            dataType: "json",
                            success: function (res) {
                                if (res.code === 0) {
                                    layer.msg(res.message, {icon: 6});
                                    table.reload('LAY-app-content-page');
                                    layer.close(index); //关闭弹层
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                            }
                        });
                    });

                    submit.trigger('click');
                }
            });
        }
    };
    $('.layui-btn.layuiadmin-btn-list').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

    //文章管理
    table.render({
        elem: '#LAY-app-content-list'
        , url: 'article'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 100, title: '文章ID', sort: true}
            , {field: 'name', title: '文章分类', templet: '<div>{{ d.category.name }}</div>'}
            , {field: 'title', title: '文章标题'}
            , {field: 'author', title: '作者', templet: '<div>{{ d.user.nickname }}</div>'}
            , {field: 'create_time', title: '创建时间'}
            , {field: 'update_time', title: '更新时间'}
            , {field: 'status', title: '发布状态', templet: '#buttonTpl', minWidth: 80, align: 'center'}
            , {title: '操作', minWidth: 150, align: 'center', fixed: 'right', toolbar: '#LAY-app-content-list-bar'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-app-content-list)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('真的删除行么', function (index) {
                admin.req({
                    url: 'article/' + data.id
                    , type: "delete"
                    , done: function (res) {
                        obj.del();
                        layer.close(index);
                    }
                });
            });
        } else if (obj.event === 'edit') {
            layer.open({
                type: 2
                ,title: '编辑文章'
                ,content: 'article/' + data.id + '/edit'
                ,area: ['1000px', '600px']
                ,maxmin: true
                ,btn: ['确定', '取消']
                ,resize: false
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-app-content-list-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+ submitID +')', function(submit){
                        admin.req({
                            url: 'article/' + data.id
                            , data: submit.field
                            , type: "PUT"
                            , done: function (res) {
                                if (!res.code) {
                                    layer.msg(res.message, {icon: 6});
                                    obj.update(submit.field);
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                                layer.close(index);
                            }
                        });
                    });

                    submit.trigger('click');
                }
            });
        }
    });

    //分类管理
    table.render({
        elem: '#LAY-app-content-category-list'
        , url: 'article_category'
        , cols: [[
            {type: 'numbers', fixed: 'left'}
            , {field: 'id', width: 100, title: 'ID', sort: true}
            , {field: 'name', title: '分类名称', minWidth: 100}
            , {field: 'description', title: '描述'}
            , {field: 'sort', title: '排序'}
            , {field: 'status', title: '状态', templet: '#buttonTpl', minWidth: 80, align: 'center'}
            , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#LAY-app-content-category-bar'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-app-content-category-list)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('真的删除行么', function (index) {
                admin.req({
                    url: 'article_category/' + data.id
                    , type: "delete"
                    , done: function (res) {
                        obj.del();
                        layer.close(index);
                    }
                });
            });
        } else if (obj.event === 'edit') {
            layer.open({
                type: 2
                ,title: '编辑分类'
                ,content: 'article_category/' + data.id + '/edit'
                ,area: ['700px', '600px']
                ,btn: ['确定', '取消']
                ,resize: false
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-app-content-category-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+ submitID +')', function(submit){
                        admin.req({
                            url: 'article_category/' + data.id
                            , data: submit.field
                            , type: "PUT"
                            , done: function (res) {
                                if (!res.code) {
                                    layer.msg(res.message, {icon: 6});
                                    obj.update(submit.field);
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                                layer.close(index);
                            }
                        });
                    });

                    submit.trigger('click');
                }
            });
        }
    });

    //页面管理
    table.render({
        elem: '#LAY-app-content-page'
        , url: 'page'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'title', title: '标题'}
            , {field: 'author', title: '作者', width: 120, templet: '<div>{{ d.user.nickname }}</div>'}
            , {field: 'create_time', title: '发布时间', width: 170}
            , {field: 'status', title: '状态', templet: '#buttonTpl', width: 80, align: 'center'}
            , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#LAY-app-content-page-bar'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });
    //监听工具条
    table.on('tool(LAY-app-content-page)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('真的删除行么', function (index) {
                admin.req({
                    url: 'page/' + data.id
                    , type: "delete"
                    , done: function (res) {
                        obj.del();
                        layer.close(index);
                    }
                });
            });
        } else if (obj.event === 'edit') {
            layer.open({
                type: 2
                ,title: '编辑页面'
                ,content: 'page/' + data.id + '/edit'
                ,area: ['1000px', '600px']
                ,btn: ['确定', '取消']
                ,resize: false
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-app-content-page-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+ submitID +')', function(submit){
                        admin.req({
                            url: 'page/' + data.id
                            , data: submit.field
                            , type: "PUT"
                            , done: function (res) {
                                if (!res.code) {
                                    layer.msg(res.message, {icon: 6});
                                    obj.update(submit.field);
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                                layer.close(index);
                            }
                        });
                    });

                    submit.trigger('click');
                }
            });
        }
    });

    //标签管理
    table.render({
        elem: '#LAY-app-content-tags'
        , url: './json/content/tags.js' //模拟接口
        , cols: [[
            {type: 'numbers', fixed: 'left'}
            , {field: 'id', width: 100, title: 'ID', sort: true}
            , {field: 'tags', title: '分类名', minWidth: 100}
            , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#layuiadmin-app-cont-tagsbar'}
        ]]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-app-content-tags)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {

        } else if (obj.event === 'edit') {

        }
    });

    //评论管理
    table.render({
        elem: '#LAY-app-content-comm'
        , url: 'article_comment' //模拟接口
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 100, title: 'ID', sort: true}
            , {field: 'nickname', title: '回帖人', templet: '<div>{{ d.user.nickname }}</div>'}
            , {field: 'avatar', title: '头像', width: 100, templet: '#imgTpl'}
            , {field: 'commentable_id', title: '回帖ID', sort: true}
            , {field: 'content', title: '回帖内容', width: 200}
            , {field: 'create_time', title: '回帖时间', sort: true}
            , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#table-content-com'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-app-content-comm)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('真的删除行么', function (index) {
                admin.req({
                    url: 'article_comment/' + data.id
                    , type: "delete"
                    , done: function (res) {
                        obj.del();
                        layer.close(index);
                    }
                });
            });
        } else if (obj.event === 'edit') {
            layer.open({
                type: 2
                ,title: '编辑回帖'
                ,content: 'article_comment/' + data.id
                ,area: ['550px', '400px']
                ,btn: ['确定', '取消']
                ,resize: false
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-user-role-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+ submitID +')', function(submit){
                        admin.req({
                            url: 'article_comment/' + data.id
                            , data: submit.field
                            , type: "PUT"
                            , done: function (res) {
                                if (!res.code) {
                                    layer.msg(res.message, {icon: 6});
                                    obj.update(submit.field);
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                                layer.close(index);
                            }
                        });
                    });

                    submit.trigger('click');
                }
            });
        }
    });

    exports('content', {})
});