layui.define(['table', 'form'], function (exports) {
    var $ = layui.$
        , table = layui.table
        , form = layui.form;

    //幻灯片管理
    table.render({
        elem: '#LAY-set-slide-list'
        , url: 'slide'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 100, title: 'ID', sort: true}
            , {field: 'name', title: '名称', minWidth: 100}
            , {field: 'sort', width: 80, title: '排序'}
            , {field: 'status', title: '状态', templet: '#statusTpl', width: 80, align: 'center'}
            , {field: 'create_time', title: '创建时间', sort: true}
            , {title: '操作', width: 250, align: 'center', fixed: 'right', toolbar: '#LAY-set-slide-bar'}
        ]]
        , page: true
        , limit: 30
        , height: 'full-320'
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-set-slide-list)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('slide/' + data.id, obj);
        } else if (obj.event === 'edit') {
            edit('编辑幻灯片', 'slide/' + data.id, '400px', '300px', 'LAY-set-slide', obj);
        } else if (obj.event === 'item') {
            open('子栏目', 'slide/' + data.id + '/item', '900px', '500px', true)
        }
    });

    //幻灯片子项目管理
    table.render({
        elem: '#LAY-set-slide-item'
        , url: 'item'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 100, title: 'ID', sort: true}
            , {field: 'title', title: '名称', minWidth: 100}
            , {field: 'image', title: '缩略图', width: 100, templet: '#imgTpl'}
            , {field: 'url', title: '地址'}
            , {field: 'sort', width: 80, title: '排序'}
            , {field: 'status', title: '状态', templet: '#statusTpl', width: 80, align: 'center'}
            , {field: 'create_time', title: '创建时间', sort: true}
            , {title: '操作', minWidth: 180, align: 'center', fixed: 'right', toolbar: '#LAY-set-slide-item-bar'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-set-slide-item)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('item/' + data.id, obj);
        } else if (obj.event === 'edit') {
            edit('编辑子项目', 'item/' + data.id, '600px', '400px', 'LAY-set-slide-item', obj);
        }
    });

    exports('slide', {})
});