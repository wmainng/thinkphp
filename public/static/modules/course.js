layui.define(['table', 'form'], function (exports) {
    var $ = layui.$
        , admin = layui.admin
        , view = layui.view
        , table = layui.table
        , form = layui.form;

    //分类管理
    table.render({
        elem: '#LAY-app-course-category'
        , url: 'course_category'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 100, title: 'ID', sort: true}
            , {field: 'name', title: '分类名', minWidth: 100}
            , {field: 'icon', title: '图标'}
            , {field: 'sort', title: '排序'}
            , {field: 'status', title: '状态', templet: '#buttonTpl', minWidth: 80, align: 'center'}
            , {field: 'create_time', title: '创建时间'}
            , {field: 'update_time', title: '更新时间'}
            , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#table-course-category'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-app-course-category)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('course_category/' + data.id, obj);
        } else if (obj.event === 'edit') {
            edit('编辑分类', 'course_category/' + data.id, '800px', '500px', 'LAY-app-course-category', obj);
        }
    });

    //课程管理
    table.render({
        elem: '#LAY-app-course-list'
        , url: layui.setter.host + '/course'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'name', title: '课程分类', templet: '<div>{{ d.category.name }}</div>'}
            , {field: 'title', title: '课程名称'}
            , {field: 'price', title: '价格', width: 100}
            , {field: 'status', title: '发布状态', templet: '#buttonTpl', width: 100, align: 'center'}
            , {field: 'update_time', title: '发布时间'}
            , {title: '操作', minWidth: 280, align: 'center', fixed: 'right', toolbar: '#table-course-list'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-app-course-list)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('course/' + data.id, obj);
        } else if (obj.event === 'edit') {
            edit('编辑课程', 'course/' + data.id, '1000px', '600px', 'LAY-app-course', obj);
        } else if (obj.event === 'lesson') {
            open('章节', 'course/' + data.id + '/lesson', '1000px', '600px', true)
        } else if (obj.event === 'document') {
            open('课件', 'course/' + data.id + '/document', '1000px', '600px', true)
        }
    });

    //章节管理
    table.render({
        elem: '#LAY-app-course-lesson'
        , url: 'lesson'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'title', title: '章节名称'}
            , {field: 'type', title: '章节类型', templet: '#typeTpl', width: 90, align: 'center'}
            , {field: 'is_free', title: '试听章节', templet: '#freeTpl', width: 90, align: 'center'}
            , {field: 'status', title: '章节状态', templet: '#buttonTpl', width: 90, align: 'center'}
            , {field: 'update_time', title: '发布时间'}
            , {title: '操作', minWidth: 180, align: 'center', fixed: 'right', toolbar: '#table-course-lesson'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-app-course-lesson)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('lesson/' + data.id, obj);
        } else if (obj.event === 'edit') {
            edit('编辑章节', 'lesson/' + data.id, '1000px', '550px', 'LAY-app-course-lesson', obj);
        }
    });

    //课件管理
    table.render({
        elem: '#LAY-app-course-document'
        , url: 'document'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'title', title: '课件名称'}
            , {field: 'filepath', title: '课件路径', templet: '#typeTpl'}
            , {field: 'sort', title: '排序', width: 60, align: 'center'}
            , {field: 'update_time', title: '发布时间', width: 160}
            , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#LAY-app-course-document-bar'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-app-course-document)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('document/' + data.id, obj);
        } else if (obj.event === 'edit') {
            edit('编辑课件', 'document/' + data.id, '700px', '400px', 'LAY-app-course-document', obj);
        }
    });

    exports('course', {})
});