layui.define(['table', 'form', 'upload'], function (exports) {
    var $ = layui.$
        , admin = layui.admin
        , view = layui.view
        , table = layui.table
        , form = layui.form;

    //列表管理
    table.render({
        elem: '#LAY-app-attachment-list'
        , url: 'storage'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 60, title: 'ID', sort: true}
            , {field: 'user_id', width: 80, title: '用户ID'}
            , {field: 'file_name', title: '文件名'}
            , {field: 'file_path', title: '文件预览', width: 100, templet: '#imgTpl'}
            , {field: 'file_ext', width: 90, title: '文件格式'}
            , {field: 'file_size', width: 90, title: '文件大小'}
            , {field: 'create_time', width: 160, title: '上传时间'}
            , {title: '操作', width: 100, align: 'center', fixed: 'right', toolbar: '#table-file-list'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-app-attachment-list)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            del('storage/' + data.id, obj);
        }
    });

    exports('storage', {})
});