layui.define(['form', 'upload'], function (exports) {
    var $ = layui.$
        , layer = layui.layer
        , laytpl = layui.laytpl
        , setter = layui.setter
        , table = layui.table
        , admin = layui.admin
        , form = layui.form
        , upload = layui.upload;

    var $body = $('body');

    form.render();

    //自定义验证
    form.verify({
        nickname: function (value, item) { //value：表单的值、item：表单的DOM对象
            if (!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)) {
                return '用户名不能有特殊字符';
            }
            if (/(^\_)|(\__)|(\_+$)/.test(value)) {
                return '用户名首尾不能出现下划线\'_\'';
            }
            if (/^\d+\d+\d$/.test(value)) {
                return '用户名不能全为数字';
            }
        }

        //我们既支持上述函数式的方式，也支持下述数组的形式
        //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
        , pass: [
            /^[\S]{6,12}$/
            , '密码必须6到12位，且不能出现空格'
        ]

        //确认密码
        , repass: function (value) {
            if (value !== $('#LAY_password').val()) {
                return '两次密码输入不一致';
            }
        }
    });

    layui.data.done = function(d){
        form.render();
    };

    form.on('radio(type)', function (data) {
        $('.type1').hide();
        $('.type2').hide();
        $('.type3').hide();
        $('.type4').hide();
        $('.type' + data.value).show();
    });

    //网站设置
    form.on('submit(set_system)', function (obj) {
        admin.req({
            url: '/admin/config/' + obj.field.id
            , type: 'put'
            , data: obj.field
            , success: function (res) {
                if (!res.code) {
                    layer.msg(res.message, {icon: 6});
                } else {
                    layer.msg(res.message, {icon: 5});
                }
            }
        });
        return false;
    });

    //上传头像
    var avatarSrc = $('#LAY_avatarSrc');
    upload.render({
        url: '/api/upload/'
        , elem: '#LAY_avatarUpload'
        , done: function (res) {
            if (res.status === 0) {
                avatarSrc.val(res.url);
            } else {
                layer.msg(res.msg, {icon: 5});
            }
        }
    });

    //查看头像
    admin.events.avartatPreview = function (othis) {
        var src = avatarSrc.val();
        layer.photos({
            photos: {
                "title": "查看头像" //相册标题
                , "data": [{
                    "src": src //原图地址
                }]
            }
            , shade: 0.01
            , closeBtn: 1
            , anim: 5
        });
    };

    //设置我的资料
    form.on('submit(setmyinfo)', function (obj) {
        //layer.msg(JSON.stringify(obj.field));
        admin.req({
            url: '/admin/user/info'
            , type: 'post'
            , data: obj.field
            , success: function (res) {
                if (!res.code) {
                    layer.msg(res.message, {icon: 6});
                } else {
                    layer.msg(res.message, {icon: 5});
                }
            }
        });
        return false;
    });

    //设置密码
    form.on('submit(setmypass)', function (obj) {
        //layer.msg(JSON.stringify(obj.field));
        admin.req({
            url: '/admin/user/password'
            ,type: 'post'
            ,data: obj.field
            ,success: function(res){
                if (!res.code) {
                    layer.msg(res.message, {icon: 6});
                } else {
                    layer.msg(res.message, {icon: 5});
                }
            }
        });
        return false;
    });

    //支付管理
    table.render({
        elem: '#LAY-set-payment-manage'
        , url: 'payment'
        , cols: [[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 80, title: 'ID', sort: true}
            , {field: 'name', title: '名称', width: 150}
            , {field: 'code', title: '标识', width: 100}
            , {field: 'desc', title: '描述'}
            , {field: 'status', title: '状态', templet: '#buttonTpl', width: 80, align: 'center'}
            , {title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#LAY-set-payment-bar'}
        ]]
        , page: true
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
        , text: '对不起，加载出现异常！'
    });

    //监听工具条
    table.on('tool(LAY-set-payment-manage)', function (obj) {
        var data = obj.data;
        if (obj.event === 'edit') {
            layer.open({
                type: 2
                ,title: '编辑支付方式'
                ,content: 'payment/' + data.id + '/edit'
                ,area: ['600px', '500px']
                ,btn: ['确定', '取消']
                ,resize: false
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'LAY-set-payment-submit'
                        ,submit = layero.find('iframe').contents().find('#'+ submitID);

                    //监听提交
                    iframeWindow.layui.form.on('submit('+ submitID +')', function(submit){
                        admin.req({
                            url: 'payment/' + data.id
                            , data: submit.field
                            , type: "PUT"
                            , done: function (res) {
                                if (!res.code) {
                                    layer.msg(res.message, {icon: 6});
                                    obj.update(submit.field);
                                } else {
                                    layer.msg(res.message, {icon: 5});
                                }
                                layer.close(index);
                            }
                        });
                    });

                    submit.trigger('click');
                }
            });
        }
    });

    //对外暴露的接口
    exports('set', {});
});
