<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 角色
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\Permission;
use app\common\model\Role;
use app\common\model\RolePermission;
use think\facade\View;
use think\Request;
use think\response\Json;

class RoleController extends AdminBaseController
{
    /**
     * 显示资源列表
     *
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $request = $this->request->param();
            $where = [];
            if (!empty($request['id'])) {
                $where[] = ['id', '=', $request['id']];
            }
            if (!empty($request['username'])) {
                $where[] = ['username', '=', $request['username']];
            }
            if (!empty($request['email'])) {
                $where[] = ['email', '=', $request['email']];
            }
            $list = Role::where($where)->order('id desc')->paginate(10);
            return $this->result($list->items(), '', ['count' => $list->total()]);
        }
        View::assign('role', Role::select());
        return View::fetch('user/role/index');
    }

    /**
     * 显示创建资源表单页.
     *
     * @return string
     */
    public function create()
    {
        return View::fetch('user/role/create');
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = $request->param();
        return Role::assignSave($data);
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return array
     */
    public function read($id)
    {
        return Role::find($id);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return string
     */
    public function edit($id)
    {
        View::assign('data', Role::find($id));
        return View::fetch('user/role/edit');
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function update(Request $request)
    {
        $data = $request->param();
        return Role::assignUpdate($data);
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return Json
     */
    public function delete($id)
    {
        $role = Role::find($id);
        $role->delete();
        $res = $role->permissions()->detach();
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }

    /**
     * 批量删除
     *
     */
    public function batchDelete()
    {
        $ids = $this->request->param('ids');
        Role::destroy($ids);
        // 删除中间表数据
        $res = RolePermission::where('role_id', 'in', $ids)->delete();
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }

    /**
     * 角色分配权限
     */
    public function permission($id)
    {
        // 查询当前权限
        $role = Role::find($id);
        $data = array_column($role->permissions->toArray(),'id');
        // 查询全部权限
        $permissions = json_encode(Permission::treeData($data));
        View::assign(compact('role','permissions'));
        return View::fetch('user/role/permission');
    }

    /**
     * 角色存储权限
     */
    public function assignPermission($id)
    {
        $role = Role::find($id);
        $permissions = $this->request->param('permission_ids');
        if (empty($permissions)){
            $role->permissions()->detach();
            return $this->success('清除角色权限成功');
        }
        $role->syncPermissions($permissions);
        return $this->success('更新角色权限成功');
    }
}
