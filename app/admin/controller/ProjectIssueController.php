<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 项目任务
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\ProjectIssue;
use think\facade\View;

class ProjectIssueController extends AdminBaseController
{
    /**
     * @OA\Get(path="/project/{project_id}/issue",tags={"项目任务"},summary="列表",
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="The User")
     * )
     */
    public function index($project_id)
    {
        if ($this->request->isAjax()) {
            $param = $this->request->param();
            $where[] = ['project_id', '=', $project_id];
            if (!empty($param['username'])) {
                $where[] = ['username', '=', $param['username']];
            }
            if (!empty($param['title'])) {
                $where[] = ['title', 'like', '%' . $param['title'] . '%'];
            }
            if (!empty($param['type'])) {
                $where[] = ['type', '=', $param['type']];
            }
            $list = ProjectIssue::where($where)->order('id desc')->paginate(10);
            return $this->result($list->items(), '', ['count' => $list->total()]);
        }
        return View::fetch('app/project/issue');
    }

    public function create()
    {
        View::assign('issue');
        return View::fetch('app/project/issueform');
    }

    /**
     * @OA\Post(path="/project/{project_id}/issue",tags={"项目任务"},summary="新增",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="文章名称", property="title", type="string", default="dd"),
     *           @OA\Property(description="文章内容", property="content", type="string"),
     *           required={"title", "content"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save($project_id)
    {
        $data = $this->request->param();
        $data['start_time'] = !empty($data['start_time']) ? $data['start_time'] :  date('Y-m-d H:i:s');
        $data['end_time'] = !empty($data['end_time']) ? $data['end_time'] : date('Y-m-d 23:59:59');
        $data['user_id'] = $this->user_id;
        $data['project_id'] = $project_id;
        ProjectIssue::create($data);
        return $this->success('添加成功');
    }

    /**
     * @OA\Get(path="/project/{project_id}/issue/{id}",tags={"项目任务"},summary="详情",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($project_id, $id)
    {
        $data = ProjectIssue::find($id);
        $this->success('获取成功', $data);
    }

    public function edit($id)
    {
        $issue = ProjectIssue::find($id);
        View::assign('issue', $issue);
        return View::fetch('app/project/issueform');
    }

    /**
     * @OA\Put(path="/project/{project_id}/issue/{id}",tags={"项目任务"},summary="编辑",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="文章名称", property="title", type="string"),
     *           @OA\Property(description="文章内容", property="content", type="string"),
     *           required={"title", "content"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($project_id, $id)
    {
        $data = $this->request->param();
        $article = ProjectIssue::update($data, ['id' => $id]);
        if (!$article) {
            return $this->error('保存失败');
        }
        return $this->success('保存成功');
    }

    /**
     * @OA\Delete(path="/project/{project_id}/issue/{id}",tags={"项目任务"},summary="删除",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="The User")
     * )
     */
    public function delete($project_id, $id)
    {
        $result = ProjectIssue::destroy($id);
        if ($result) {
            $this->success('删除成功');
        }
        $this->error('删除失败');
    }
}
