<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\common\model\Permission;
use think\facade\View;

class IndexController extends AdminBaseController
{
    /**
     * 1.安装swagger-ui前端
     * git clone https://github.com/swagger-api/swagger-ui.git
     * 复制swagger-ui目录下dist文件到tp项目的public目录下,编辑dist目录下index.html文件，修改url地址为文档路由地址
     * 2.安装swagger-php后端
     * 进入你的项目目录执行如下命令：composer require zircote/swagger-php
     * 提示安装完成后会在你tp项目的vendor中生成一个zircote的组件文件夹，说明已经安装插件成功了。
     * 3.生成swagger.json文件 控制器方法生成或命令行生成
     * 4.编写swagger注释
     *
     * @OA\OpenApi(
     *    security={{"bearerAuth": {}}}
     * )
     *
     * @OA\Components(
     *     @OA\SecurityScheme(
     *         securityScheme="bearerAuth",
     *         type="http",
     *         scheme="bearer",
     *     )
     * )
     *
     * @OA\Info(
     *   title="忆昔网开发文档",
     *   version="1.0.0",
     *   description="WebService API 是基于HTTPS/HTTP协议的数据接口，开发者可以使用任何客户端、服务器和开发语言，按照WebService API规范，按需构建HTTPS请求，并获取结果数据。"
     * )
     *
     * @OA\Server(
     *      url="{schema}://bengbu.link/v1",
     *      description="",
     *      @OA\ServerVariable(
     *          serverVariable="schema",
     *          enum={"https", "http"},
     *          default="https"
     *      )
     * )
     */
    public function index()
    {
        return View::fetch('index');
    }

    /**
     * 控制台
     * @return string
     */
    public function console()
    {
        $server_info = array(
            //网站域名url
            //document_root 网站目录
            //mysql_version MySQL数据库版本
            //server_soft WEB运行环境
            //server_port 服务器端口
            'server_os' => PHP_OS,//服务器操作系统
            'server_ip' => $_SERVER['SERVER_ADDR'] . ':' . $_SERVER['SERVER_PORT'],//服务器IP
            'web_server' => 'nginx',//WEB 服务器
            'php_sapi_name' => php_sapi_name(),//PHP运行方式
            'php_version' => PHP_VERSION,//PHP版本
            'mysql_version' => '5.7.37', //MYSQL 版本
            //ThinkPHP版本号
            //'max_upload_size' => ini_get('upload_max_filesize'),//最大上传限制
            //'max_execution_time' => ini_get('max_execution_time') . "秒",//执行时间限制
            //'剩余空间' => round((@disk_free_space(".") / (1024 * 1024)), 2) . 'M',
            //'服务器时间' => date("Y年n月j日 H:i:s"),
            //'北京时间' => gmdate("Y年n月j日 H:i:s", time() + 8 * 3600),
        );
        View::assign([
            'server_info' => $server_info
        ]);
        return View::fetch('home/console');
    }

    public function homepage1()
    {
        return View::fetch('home/homepage1');
    }

    public function homepage2()
    {
        return View::fetch('home/homepage2');
    }

    /**
     * 菜单
     * @return void
     */
    public function nav()
    {
        return $this->success('', Permission::getMenu());
    }

    /**
     * 清楚缓存
     */
    public function clear()
    {

    }
}
