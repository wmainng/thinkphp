<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 导航菜单
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\NavMenu;
use think\facade\View;

class NavMenuController extends AdminBaseController
{
    /**
     * @OA\Get(path="/nav/:slide_id/menu",tags={"导航菜单"},summary="列表",
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index($nav_id)
    {
        if ($this->request->isAjax()) {
            $param = $this->request->param();
            $field = $param['field'] ?? 'id';
            $order = $param['order'] ?? 'desc';
            $page  = $param['page'] ?? 1;

            $where[] = ['nav_id', '=', $nav_id];
            if (isset($param['status'])) {
                $where[] = ['status', '=', $param['status']];
            }
            $list = NavMenu::where($where)->order($field, $order)->page((int)$page,10)->select();
            $this->result($list);
        }
        return View::fetch('set/nav/menu');
    }

    public function create()
    {
        View::assign('menu');
        return View::fetch('set/nav/menuform');
    }

    /**
     * @OA\Post(path="/nav/:slide_id/menu",tags={"导航菜单"},summary="新增",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="名称", property="name", type="string"),
     *           @OA\Property(description="地址", property="url", type="string"),
     *           @OA\Property(description="图标", property="icon", type="integer"),
     *           @OA\Property(description="排序", property="sort", type="integer"),
     *           @OA\Property(description="状态", property="status", type="integer"),
     *           required={"name","url","icon"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {
        $param = $this->request->param();

        $this->validate($param, 'app\common\validate\NavMenu');

        NavMenu::create($param);
        $this->success('保存成功');
    }

    /**
     * @OA\Get(path="/nav/:slide_id/menu/{id}",tags={"导航菜单"},summary="详情",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($id)
    {
        $data = NavMenu::find($id);
        $this->success('获取成功', $data);
    }

    public function edit($id)
    {
        $menu = NavMenu::find($id);
        View::assign('menu', $menu);
        return View::fetch('set/nav/menuform');
    }

    /**
     * @OA\Post(path="/nav/:slide_id/menu/{id}",tags={"导航菜单"},summary="编辑",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="名称", property="name", type="string"),
     *           @OA\Property(description="地址", property="url", type="string"),
     *           @OA\Property(description="图标", property="icon", type="integer"),
     *           @OA\Property(description="排序", property="sort", type="integer"),
     *           @OA\Property(description="状态", property="status", type="integer"),
     *           required={"name","url","icon"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($id)
    {
        $param = $this->request->param();
        $param['id'] = $id;

        $this->validate($param, 'app\common\validate\NavMenu');

        $menu = NavMenu::find($id);

        $res = $menu->save($param);
        if (!$res) {
            $this->error("保存失败");
        }
        $this->success('保存成功', $menu);
    }

    /**
     * @OA\Delete(path="/nav/:slide_id/menu/{id}",tags={"导航菜单"},summary="删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($id)
    {
        $res = NavMenu::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }

    /**
     * @OA\Get(path="/menus",tags={"导航菜单"},summary="列表",
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function menus()
    {
        $param = $this->request->param();
        $field = $param['field'] ?? 'id';
        $order = $param['order'] ?? 'desc';
        $where[] = ['status', '=', 1];
        $list = NavMenu::where($where)->order($field, $order)->select();
        $this->result(0,'success', $list);
    }
}