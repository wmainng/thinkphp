<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 用户认证
// +---------------------------------------------------------------------

namespace app\admin\controller;


use app\BaseController;
use app\common\model\Oauth;
use think\facade\View;

class OauthController extends BaseController
{
    /**
     * @OA\Get(path="/user",tags={"用户"},summary="列表",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $request = $this->request->param();
            $where   = [];
            if (!empty($request['id'])) {
                $where[] = ['id', '=', $request['id']];
            }
            if (!empty($request['username'])) {
                $where[] = ['username', '=', $request['username']];
            }
            if (!empty($request['email'])) {
                $where[] = ['email', '=', $request['email']];
            }
            $list = Oauth::where($where)->order('id desc')->paginate(10);
            if ($list->isEmpty()) {
                $data = [
                    'code' => 1,
                    'msg'  => '数据为空',
                ];
                return json($data);
            }
            $data = [
                'code'  => 0,
                'msg'   => '正在请求中...',
                'count' => $list->total(),
                'data'  => $list->items()
            ];
            return json($data);
        }
        return View::fetch('admin/user/oauth/index');
    }

    /**
     * 微信小程序用户登录
     */
    public function wxapplogin()
    {
        $post = $this->request->param();

        $rule = [
            'code'           => 'require',
            'encrypted_data' => 'require',
            'iv'             => 'require',
            'raw_data'       => 'require',
            'signature'      => 'require',
        ];
        $message = [
            'code.require'           => '缺少参数code!',
            'encrypted_data.require' => '缺少参数encrypted_data!',
            'iv.require'             => '缺少参数iv!',
            'raw_data.require'       => '缺少参数raw_data!',
            'signature.require'      => '缺少参数signature!',
        ];

        $this->validate($post, $rule, $message);
    }
}