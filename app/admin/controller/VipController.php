<?php
declare (strict_types=1);
// +----------------------------------------------------------------------
// | 用户级别
// +----------------------------------------------------------------------

namespace app\admin\controller;


use app\BaseController;
use app\common\model\UserRank;

class VipController extends BaseController
{
    /**
     * @OA\Get(path="/user_rank",tags={"用户级别"},summary="列表",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Parameter(name="keyword", in="query", description="关键词", @OA\Schema(type="string")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        $param = $this->request->param();
        $field = $param['field'] ?? 'id';
        $order = $param['order'] ?? 'asc';
        $where = [];
        if (isset($param['keyword'])) {
            $where[] = ['name', 'like', '%' . $param['keyword'] . '%'];
        }
        if (isset($param['status'])) {
            $where[] = ['status', '=', $param['status']];
        }

        $list  = UserRank::where($where)->order($field, $order)->select();
        $this->success('获取成功', $list);
    }

    /**
     * @OA\Post(path="/user_rank",tags={"用户级别"},summary="新增",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="名称", property="name", type="text"),
     *           @OA\Property(description="最低", property="min_points", type="integer"),
     *           @OA\Property(description="最大", property="max_points", type="integer"),
     *           @OA\Property(description="折扣", property="discount", type="number"),
     *           @OA\Property(description="排序", property="sort", type="number"),
     *           @OA\Property(description="状态", property="status", type="number"),
     *           required={"name","min_points","max_points","discount","sort","status"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {
        $param = $this->request->param();
        $this->validate($param, 'app\common\validate\UserRank');
        $article = UserRank::create($param);
        $this->success('保存成功', $article);
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     */
    public function read($id)
    {
        $data = UserRank::find($id);
        $this->success('获取成功', $data);
    }

    /**
     * 保存更新的资源
     *
     * @param  int  $id
     */
    public function update($id)
    {
        $param = $this->request->param();
        $param['id'] = $id;
        $this->validate($param, 'app\common\validate\UserRank');
        $article = UserRank::update($param);
        if (!$article) {
            $this->error("保存失败");
        }
        $this->success('保存成功', $article);
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     */
    public function delete($id)
    {
        $res = UserRank::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }
}
