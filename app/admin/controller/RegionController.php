<?php
declare (strict_types=1);
// +----------------------------------------------------------------------
// | 地区管理
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\Region;
use app\model\Config;
use helper\Arr;
use helper\facade\IMap;

class RegionController extends AdminBaseController
{
    public function index()
    {

    }

    /**
     * @OA\Get(path="/region/list",tags={"行政区划"},summary="获取省市区列表",
     *   @OA\Parameter(name="keywords", in="query", description="搜索关键词：行政区名称、adcode", @OA\Schema(type="string")),
     *   @OA\Parameter(name="subdistrict", in="query", description="下级行政区级数:0,1,2,3,4,5", @OA\Schema(type="int", default="3")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function list()
    {
        $param = $this->request->param();
        $param['keywords'] = $param['keywords'] ?? 0;
        $param['subdistrict'] = $param['subdistrict'] ?? 3;
        $this->success('获取成功', Region::getList($param));
    }

    /**
     * @OA\Get(path="/region/getchildren/{code}",tags={"行政区划"},summary="获取下级行政区划",
     *   @OA\Parameter(name="code", in="path", description="code", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function getChildren($code)
    {
        $list = Region::getParents($code);
        $list = Arr::subTree($list, 0, 'children', 'adcode', 'parent_adcode');
        $this->success('获取成功', $list);
    }

    /**
     * @OA\Get(path="/region/suggestion",tags={"行政区划"},summary="输入提示",
     *   @OA\Parameter(name="keyword", in="query", description="搜索关键词", @OA\Schema(type="string")),
     *   @OA\Parameter(name="region", in="query", description="城市:行政区名称、adcode", @OA\Schema(type="string")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function suggestion()
    {
        $param = $this->request->param();
        if (empty($param['keyword'])) {
            $this->error('请输入关键词');
        }
        $param['region'] = $param['region'] ?? '';
        $param['region'] = is_numeric($param['region']) ? substr($param['region'], 0, 6) : $param['region'];

        $config = Config::getConf('map');
        $map    = IMap::factory('amap', $config['key']);
        $list   = $map->getSuggestion($param['region'], $param['keyword']);
        if ($list) {
            foreach ($list as $k=>$v) {
                if (!$v['location']) {
                    unset($list[$k]);
                }
            }
            $list = array_values($list);
            $location = array_column($list, 'location');
            $location = implode('|', array_filter($location));
            $component = $map->getAddress($location, 'true');

            foreach ($list as $k=>$v) {
                if (isset($component[$k]['addressComponent'])) {
                    $list[$k]['province'] = $component[$k]['addressComponent']['province'];
                    $list[$k]['city']     = $component[$k]['addressComponent']['city'];
                    $list[$k]['district'] = $component[$k]['addressComponent']['district'];
                    $list[$k]['street']   = $component[$k]['addressComponent']['township'];
                    $list[$k]['adcode']   = $component[$k]['addressComponent']['towncode'] ?
                        substr($component[$k]['addressComponent']['towncode'], 0, 9) : '';
                } else {
                    $list[$k]['street'] = '';
                    $list[$k]['adcode'] = '';
                }
            }
        }
        $this->success('获取成功', $list);
    }
}
