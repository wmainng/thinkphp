<?php

namespace app\admin\controller;

use app\common\model\UserDevice;
use think\facade\View;

class LoginController extends AdminBaseController
{
    public function index()
    {
        return View::fetch('user/login');
    }

    /**
     * @OA\Post(path="/login",tags={"用户认证"},summary="登录",
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="手机号码", property="mobile", type="string", default=""),
     *           @OA\Property(description="验证码", property="code", type="string", default=""),
     *           @OA\Property(description="设备标识", property="device_id", type="string"),
     *           @OA\Property(description="设备类型", property="device_type", type="string"),
     *           @OA\Property(description="设备平台:ios,android", property="platform_type", type="string"),
     *           required={"mobile", "code"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {
        $param = $this->request->param();

        $this->validate($param, 'app\common\validate\Login');

        $param['remember'] = isset($param['remember']);

        if ($this->auth()->attempt($param, $param['remember'])) {
            $user = $this->auth()->user();
            // 分发UserLogin事件
            event('UserLogin', $user);
            $this->success('登录成功!', $user);
        }
        $this->error('登入失败');
    }

    /**
     * @OA\Post(path="/logout",tags={"用户认证"},summary="退出",
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function logout()
    {
        $this->auth()->logout();
        return $this->success('登入成功');
    }
}