<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 幻灯片子项目
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\SlideItem;
use think\facade\View;

class SlideItemController extends AdminBaseController
{
    /**
     * @OA\Get(path="/slide/:slide_id/item",tags={"幻灯片子项目"},summary="列表",
     *   @OA\Parameter(name="field", in="query", description="字段", @OA\Schema(type="string")),
     *   @OA\Parameter(name="order", in="query", description=排序, @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $param = $this->request->param();
            $field = $param['field'] ?? 'id';
            $order = $param['order'] ?? 'desc';
            $limit = $param['limit'] ?? 10;

            $where[] = ['slide_id', '=', $param['slide_id']];
            if (!empty($request['name'])) {
                $where[] = ['name', '=', $request['name']];
            }
            $list = SlideItem::where($where)->order($field, $order)->paginate($limit);
            return $this->result($list->items(), '', ['count' => $list->total()]);
        }
        return View::fetch('set/slide/item');
    }

    public function create()
    {
        View::assign('item');
        return View::fetch('set/slide/itemform');
    }

    /**
     * @OA\Post(path="/slide/:slide_id/item",tags={"幻灯片子项目"},summary="新增",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="名称", property="name", type="string"),
     *           @OA\Property(description="地址", property="url", type="string"),
     *           @OA\Property(description="图标", property="icon", type="integer"),
     *           @OA\Property(description="排序", property="sort", type="integer"),
     *           @OA\Property(description="状态", property="status", type="integer"),
     *           required={"name","url","icon"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save($slide_id)
    {
        $param = $this->request->param();
        $param['slide_id'] = $slide_id;
        $this->validate($param, 'app\common\validate\SlideItem');

        SlideItem::create($param);
        $this->success('保存成功');
    }

    /**
     * @OA\Get(path="/slide/:slide_id/item/{id}",tags={"幻灯片子项目"},summary="详情",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($slide_id, $id)
    {
        $data = SlideItem::find($id);
        $this->success('获取成功', $data);
    }

    public function edit($id)
    {
        $item = SlideItem::find($id);
        View::assign('item', $item);
        return View::fetch('set/slide/itemform');
    }

    /**
     * @OA\Post(path="/slide/:slide_id/item/{id}",tags={"幻灯片子项目"},summary="编辑",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="名称", property="name", type="string"),
     *           @OA\Property(description="地址", property="url", type="string"),
     *           @OA\Property(description="图标", property="icon", type="integer"),
     *           @OA\Property(description="排序", property="sort", type="integer"),
     *           @OA\Property(description="状态", property="status", type="integer"),
     *           required={"name","url","icon"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($slide_id, $id)
    {
        $param = $this->request->param();
        $param['id'] = $id;

        $this->validate($param, 'app\common\validate\SlideItem');

        $menu = SlideItem::find($id);

        $res = $menu->save($param);
        if (!$res) {
            $this->error("保存失败");
        }
        $this->success('保存成功', $menu);
    }

    /**
     * @OA\Delete(path="/slide/:slide_id/item/{id}",tags={"幻灯片子项目"},summary="删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($slide_id, $id)
    {
        $res = SlideItem::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }
}