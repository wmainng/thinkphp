<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 文章管理
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\Article;
use app\common\model\ArticleCategory;
use think\facade\View;

class ArticleController extends AdminBaseController
{
    /**
     * @OA\Get(path="/article",tags={"文章"},summary="列表",
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $request = $this->request->param();
            $field = $param['field'] ?? 'id';
            $order = $param['order'] ?? 'desc';
            $where[] = ['type', '=', 0];
            if (!empty($request['id'])) {
                $where[] = ['id', '=', $request['id']];
            }
            if (!empty($request['user_id'])) {
                $where[] = ['user_id', '=', $request['user_id']];
            }
            if (!empty($request['category_id'])) {
                $where[] = ['category_id', '=', $request['category_id']];
            }

            $list = Article::with([
                'category' => function($query) {
                    $query->field('id,name');
                },
                'user' => function($query) {
                    $query->field('id,nickname');
                }])
                ->where($where)
                ->order($field, $order)
                ->paginate(10);
            return $this->result($list->items(), '', ['count' => $list->total()]);
        }
        return View::fetch('app/content/list');
    }

    public function create()
    {
        View::assign([
            'categories' => ArticleCategory::categories(),
            'article'    => null
        ]);
        return View::fetch('app/content/listform');
    }

    /**
     * @OA\Post(path="/article",tags={"文章"},summary="新增",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="文章分类", property="category_id", type="integer"),
     *           @OA\Property(description="文章名称", property="title", type="string"),
     *           @OA\Property(description="文章内容", property="content", type="string"),
     *           required={"title", "content"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {
        $data = $this->request->param();
        $data['user_id'] = $this->user_id;
        $data['status'] = isset($data['status']);
        $article = Article::create($data);
        return $this->success('添加成功', $article);
    }

    /**
     * @OA\Get(path="/article/{id}",tags={"文章"},summary="详情",
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="The User")
     * )
     */
    public function read($id)
    {
        $article = Article::with(['user' => function($query) {
            $query->field('id,nickname');
        }])->find($id);
        return $this->success('success', $article);
    }

    public function edit($id)
    {
        View::assign([
            'categories' => ArticleCategory::categories(),
            'article'    => Article::find($id)
        ]);
        return View::fetch('app/content/listform');
    }

    /**
     * @OA\Put(path="/article/{id}",tags={"文章"},summary="编辑",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="文章分类", property="category_id", type="integer"),
     *           @OA\Property(description="文章名称", property="title", type="string"),
     *           @OA\Property(description="文章内容", property="content", type="string"),
     *           required={"title", "content"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($id)
    {
        $param = $this->request->param();
        $param['user_id'] = $this->user_id;
        $article = Article::find($id);
        $result = $article->save($param);
        if (!$result) {
            return $this->error('保存失败');
        }
        return $this->success('保存成功', $article);
    }

    /**
     * @OA\Delete(path="/article/{id}",tags={"文章"},summary="删除",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="The User")
     * )
     */
    public function delete($id)
    {
        $result = Article::destroy($id);
        if ($result) {
            $this->success('删除成功');
        }
        $this->error('删除失败');
    }

    /**
     * @OA\Get(path="/article/search",tags={"文章"},summary="搜索",
     *   @OA\Parameter(name="category_id", in="query", description="分类ID", @OA\Schema(type="int")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="The User")
     * )
     */
    public function search()
    {
        $where = [];
        $request = $this->request->param();
        if (!empty($request['category_id'])) {
            $where[] = ['category_id', '=', $request['category_id']];
        }
        $list = Article::with([
            'category'	=> function($query) {
                $query->field('id,name');
            }
        ])->field('id,title,category_id,thumbnail,create_time,update_time')
            ->where($where)
            ->order('id desc')
            ->paginate(10);
        return $this->result($list->items(), '', ['count' => $list->total()]);
    }
}