<?php

namespace app\admin\controller;

use app\common\model\Payment;
use think\facade\View;

class PaymentController extends AdminBaseController
{
    public function index()
    {
        // 充值 提现
        // pc,wap,app
        if ($this->request->isAjax()) {
            $list  = Payment::paginate(10);
            return $this->result($list->items(), '', ['count' => $list->total()]);
        }
        return View::fetch('set/payment/list');
    }

    public function create()
    {

    }

    public function edit($id)
    {
        $data = Payment::find($id);
        View::assign('data', $data);
        return View::fetch('set/payment/edit');
    }

    public function update($id)
    {
        $payment = Payment::find($id);
        $data = $this->request->param();
        $payment->save($data);
        return $this->success('Success', $payment);
    }
}
