<?php
declare (strict_types=1);
// +----------------------------------------------------------------------
// | 系统设置
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\common\model\Config;
use think\facade\View;

class ConfigController extends AdminBaseController
{
    /**
     * 显示编辑资源表单页.
     *
     * @param  string  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $key = trim($id);
        $value = Config::config($key);
        View::assign('key', $key);
        View::assign('value', $value);
        return View::fetch('set/system/' . $key);
    }

    /**
     *
     * @return \think\Response
     */
    public function update($id)
    {
        $param = $this->request->param();
        unset($param['id']);
        $result = Config::config($id, $param);
        if (!$result) {
            return $this->error('保存失败');
        }
        return $this->success('保存成功');
    }
}