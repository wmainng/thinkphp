<?php

namespace app\admin\controller;

use app\BaseController;
use app\common\model\Config;
use app\common\model\WechatMenu;
use helper\tencent\Wechat;
use think\facade\View;

class WechatMenuController extends BaseController
{
    private Wechat $wechat;

    public function initialize()
    {
        parent::initialize();

        $sdk = new Wechat(Config::config('wechat'));
        $sdk->setAccessToken(runtime_path());
        $this->wechat = $sdk;
    }

    public function index()
    {
        if ($this->request->isAjax()) {
            $list = WechatMenu::select();
            return $this->result(0,'success', $list, ['count' => count($list)]);
        }
        return View::fetch('admin/set/wechat/menu');
    }

    public function create(): string
    {
        View::assign('menu');
        return View::fetch('admin/set/wechat/menuform');
    }

    public function save()
    {
        $count = WechatMenu::where('parent_id', 0)->count();
        if ($count == 3) {
            $this->error('一级菜单最多3个');
        }

        $param = $this->request->param();
        WechatMenu::create($param);
        $this->success('保存成功');
    }

    public function edit($id): string
    {
        $menu = WechatMenu::find($id);
        View::assign('menu', $menu);
        return View::fetch('admin/set/wechat/menuform');
    }

    public function update($id): string
    {
        $param = $this->request->param();
        $article = WechatMenu::find($id);
        $result = $article->save($param);
        if (!$result) {
            return $this->error('保存失败');
        }
        return $this->success('保存成功', $article);
    }

    public function delete($id)
    {
        $res = WechatMenu::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');

        //$data = $this->wechat->deleteMenu();
    }

    public function publish()
    {
        $ids   = $this->request->param('ids');
        $menus = WechatMenu::field('name,type,url,appid,pagepath')
            ->where('id', 'in', $ids)
            ->order('sort', 'desc')
            ->select()->toArray();

        $wechat = Config::config('miniprogram');
        $button = [];
        foreach ($menus as $v) {
            if ($v['type'] == 'click') {
                $button[] = [
                    'type' => $v['type'],
                    'name' => $v['name'],
                    'key'  => $v['key']
                ];
            } else if ($v['type'] == 'view') {
                $button[] = [
                    'type' => $v['type'],
                    'name' => $v['name'],
                    'url'  => $v['url']
                ];
            } else {
                $button[] = [
                    'type'      => $v['type'],
                    'name'      => $v['name'],
                    'appid'     => $wechat['appId'],
                    'pagepath'  => $v['pagepath']
                ];
            }
        }
        $res = $this->wechat->createMenu(json_encode(['button' => $button], JSON_UNESCAPED_UNICODE));
        if (empty($res['errcode'])) {
            $this->success('发布成功');
        }
        $this->error('发布失败');
    }
}