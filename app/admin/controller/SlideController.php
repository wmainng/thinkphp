<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 幻灯片
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\Slide;
use app\common\model\SlideItem;
use think\facade\View;

class SlideController extends AdminBaseController
{
    /**
     * @OA\Get(path="/slide",tags={"幻灯片"},summary="列表",
     *   @OA\Parameter(name="field", in="query", description="字段", @OA\Schema(type="string")),
     *   @OA\Parameter(name="order", in="query", description=排序, @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $field = $param['field'] ?? 'id';
            $order = $param['order'] ?? 'desc';
            $limit = $param['limit'] ?? 10;
            $where = [];
            if (!empty($request['id'])) {
                $where[] = ['id', '=', $request['id']];
            }
            if (!empty($request['name'])) {
                $where[] = ['name', '=', $request['name']];
            }
            $list = Slide::where($where)->order($field, $order)->paginate($limit);
            return $this->result($list->items(), '', ['count' => $list->total()]);
        }
        return View::fetch('set/slide/list');
    }

    public function create()
    {
        View::assign('slide');
        return View::fetch('set/slide/listform');
    }

    /**
     * @OA\Post(path="/slide",tags={"幻灯片"},summary="新增",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="名称", property="name", type="string"),
     *           @OA\Property(description="描述", property="description", type="string"),
     *           @OA\Property(description="排序", property="sort", type="integer"),
     *           @OA\Property(description="状态", property="status", type="integer"),
     *           required={"name"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {
        $param = $this->request->param();

        $this->validate($param, 'app\common\validate\Slide');

        Slide::create($param);
        $this->success('保存成功');
    }

    /**
     * @OA\Get(path="/slide/{id}",tags={"幻灯片"},summary="详情",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($id)
    {
        $data = Slide::find($id);
        $this->success('获取成功', $data);
    }

    public function edit($id)
    {
        $slide = Slide::find($id);
        View::assign('slide', $slide);
        return View::fetch('set/slide/listform');
    }

    /**
     * @OA\Post(path="/slide/{id}",tags={"幻灯片"},summary="编辑",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="名称", property="name", type="string"),
     *           @OA\Property(description="描述", property="description", type="string"),
     *           @OA\Property(description="排序", property="sort", type="integer"),
     *           @OA\Property(description="状态", property="status", type="integer"),
     *           required={"name"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($id)
    {
        $param = $this->request->param();

        $this->validate($param, 'app\common\validate\Slide');

        $data = Slide::find($id);

        $res = $data->save($param);
        if (!$res) {
            $this->error("保存失败");
        }
        $this->success('保存成功', $data);
    }

    /**
     * @OA\Delete(path="/slide/{id}",tags={"幻灯片"},summary="删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($id)
    {
        $item = SlideItem::where('slide_id', $id)->find();
        if ($item) {
            $this->error("请先删除此栏目下的内容");
        }
        $res = Slide::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }

    /**
     * @OA\Get(path="/slides",tags={"幻灯片"},summary="列表",
     *   @OA\Parameter(name="field", in="query", description="字段", @OA\Schema(type="string")),
     *   @OA\Parameter(name="order", in="query", description=排序, @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function list()
    {
        $list = SlideItem::where('slide_id', 1)->order('id','desc')->select();
        $data = [
            'code'    => 0,
            'message' => 'Success',
            'data'    => $list->toArray()
        ];
        return json($data);
    }
}