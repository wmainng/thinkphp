<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 权限
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\Permission;
use app\common\model\RolePermission;
use think\facade\View;
use think\Request;

class PermissionController extends AdminBaseController
{
    /**
     * 显示资源列表
     *
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $request = $this->request->param();
            $where = [];
            if (!empty($request['title'])) {
                $where[] = ['title', '=', $request['title']];
            }
            if (!empty($request['name'])) {
                $where[] = ['name', '=', $request['name']];
            }
            if (!empty($request['route'])) {
                $where[] = ['route', '=', $request['route']];
            }
            $list = Permission::where($where)->order('id desc')->paginate(10);
            return $this->result($list->items(), '', ['count' => $list->total()]);
        }
        return View::fetch('user/permission/index');
    }

    /**
     * 显示创建资源表单页.
     *
     * @return string
     */
    public function create()
    {
        View::assign('menu', Permission::getMenu(false));
        return View::fetch('user/permission/create');
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = $request->param();
        $data['status'] = isset($data['status']) ? 1 : 0;
        return Permission::assignSave($data);
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return array
     */
    public function read($id)
    {
        return Permission::find($id);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return string
     */
    public function edit($id)
    {
        View::assign([
            'menu' => Permission::getMenu(false),
            'info' => Permission::find($id),
        ]);
        return View::fetch('user/permission/edit');
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function update(Request $request)
    {
        $data = $request->param();
        $data['status'] = isset($data['status']) ? 1 : 0;
        return Permission::assignUpdate($data);
    }

    /**
     * 删除指定资源
     *
     * @param  string  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        $permission = Permission::find($id);
        $permission->delete();
        $res = $permission->roles()->detach();
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }

    /**
     * 批量删除资源
     *
     */
    public function batchDelete()
    {
        $ids = $this->request->param('ids');
        Permission::destroy($ids);
        // 删除中间表数据
        $res = RolePermission::where('permission_id', 'in', $ids)->delete();
        if (!$res) {
            return $this->error("删除失败");
        }
        return $this->success('删除成功');
    }
}
