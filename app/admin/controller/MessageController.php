<?php
// +----------------------------------------------------------------------
// | 用户消息
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\Message;
use think\facade\View;

class MessageController extends AdminBaseController
{
    /**
     * @OA\Get(path="/message",tags={"消息"},summary="列表",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Parameter(name="status", in="query", description="状态", @OA\Schema(type="int", default="0")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $param = $this->request->param();
            $field = $param['field'] ?? 'id';
            $order = $param['order'] ?? 'DESC';
            $limit = $param['limit'] ?? 10;

            $where = [];
            if (isset($param['type'])) {
                $where[] = ['type', '=' , $param['type']];
            }
            if (isset($param['status'])) {
                $where[] = ['status', '=' , $param['status']];
            }
            $list  = Message::where($where)->order($field, $order)->paginate($limit);
            return $this->result($list->items(), '', ['count' => $list->total()]);
        }

        $message = Message::field('id,type')->where('status', 0)->select()->toArray();
        $res = [];
        if ($message) {
            $arr = array_column($message, 'type', 'id');
            $res = array_count_values($arr);
        }
        View::assign([
            'notice' => $res[0] ?? 0,
            'direct' => $res[1] ?? 0
        ]);
        return View::fetch('app/message/index');
    }

    public function create()
    {

    }

    /**
     * @OA\Post(path="/message",tags={"消息"},summary="新增",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="文章分类", property="category_id", type="integer"),
     *           @OA\Property(description="文章名称", property="title", type="string"),
     *           @OA\Property(description="文章内容", property="content", type="string"),
     *           required={"title", "content"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {

    }

    /**
     * @OA\Get(path="/message/{id}",tags={"消息"},summary="详情",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($id)
    {
        $data = Message::find($id);
        $this->success('获取成功', $data);
    }

    public function edit($id)
    {
        $message = Message::find($id);
        if ($message->status == 0) {
            $message->status = 1;
            $message->save();
        }
        View::assign('data', $message);
        return View::fetch('app/message/edit');
    }

    /**
     * @OA\Put(path="/message/{id}",tags={"消息"},summary="已读",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($id)
    {
        Message::update([
            'id'      => $id,
            'user_id' => $this->user_id,
            'status'  => 1
        ]);
        $this->success('已读');
    }

    /**
     * @OA\Delete(path="/message/{id}",tags={"消息"},summary="删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($id)
    {
        $res = Message::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }

    /**
     * @OA\Delete(path="/message",tags={"消息"},summary="批量删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="ids", in="path", description="ids", @OA\Schema(type="array")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function batchDelete()
    {
        $ids = $this->request->param('ids');
        $res = Message::destroy($ids);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }

    /**
     * @OA\Put(path="/message/status",tags={"消息"},summary="批量已读",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="ids", in="path", description="ids", @OA\Schema(type="array")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function batchStatus()
    {
        $ids = $this->request->param('ids');
        Message::where('id', 'in', $ids)->update(['status'  => 1]);
        $this->success('已读');
    }

    /**
     * @OA\Delete(path="/message",tags={"消息"},summary="清空",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function clear()
    {
        Message::where('user_id', $this->user_id)->delete();
        $this->success('操作成功');
    }

    /**
     * @OA\Get(path="/message/new",tags={"消息"},summary="最新",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function new()
    {
        if(empty($this->user_id)){
            $data['count']  = 0;
        }else{
            $data['count'] = Message::where('user_id',$this->user_id)
                ->whereTime('create_time', 'today')
                ->where('status',0)
                ->count();
        }
        $this->success('请求成功', $data);
    }
}
