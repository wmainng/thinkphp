<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 导航
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\Nav;
use app\common\model\NavMenu;
use think\facade\View;

class NavController extends AdminBaseController
{
    /**
     * @OA\Get(path="/nav",tags={"导航"},summary="列表",
     *   @OA\Parameter(name="field", in="query", description="字段", @OA\Schema(type="string")),
     *   @OA\Parameter(name="order", in="query", description=排序, @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $param = $this->request->param();
            $field = $param['field'] ?? 'id';
            $order = $param['order'] ?? 'desc';
            $limit = $param['limit'] ?? 10;
            $where = [];
            if (!empty($request['id'])) {
                $where[] = ['id', '=', $request['id']];
            }
            if (!empty($request['name'])) {
                $where[] = ['name', '=', $request['name']];
            }
            $list = Nav::where($where)->order($field, $order)->paginate($limit);
            return $this->result($list->items(), '', ['count' => $list->total()]);
        }
        return View::fetch('set/nav/list');
    }

    public function create()
    {
        View::assign('nav');
        return View::fetch('set/nav/listform');
    }

    /**
     * @OA\Post(path="/nav",tags={"导航"},summary="新增",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="名称", property="name", type="string"),
     *           @OA\Property(description="描述", property="description", type="string"),
     *           @OA\Property(description="排序", property="sort", type="integer"),
     *           @OA\Property(description="状态", property="status", type="integer"),
     *           required={"name"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {
        $param = $this->request->param();

        $this->validate($param, 'app\common\validate\Nav');

        Nav::create($param);
        $this->success('保存成功');
    }

    /**
     * @OA\Get(path="/nav/{id}",tags={"导航"},summary="详情",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($id)
    {
        $data = Nav::find($id);
        $this->success('获取成功', $data);
    }

    public function edit($id)
    {
        $nav = Nav::find($id);
        View::assign('nav', $nav);
        return View::fetch('set/nav/listform');
    }

    /**
     * @OA\Post(path="/nav/{id}",tags={"导航"},summary="编辑",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="名称", property="name", type="string"),
     *           @OA\Property(description="描述", property="description", type="string"),
     *           @OA\Property(description="排序", property="sort", type="integer"),
     *           @OA\Property(description="状态", property="status", type="integer"),
     *           required={"name"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($id)
    {
        $param = $this->request->param();

        $this->validate($param, 'app\common\validate\Nav');

        $data = Nav::find($id);

        $res = $data->save($param);
        if (!$res) {
            $this->error("保存失败");
        }
        $this->success('保存成功', $data);
    }

    /**
     * @OA\Delete(path="/nav/{id}",tags={"导航"},summary="删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($id)
    {
        $item = NavMenu::where('nav_id', $id)->find();
        if ($item) {
            $this->error("请先删除此栏目下的内容");
        }
        $res = Nav::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }

    /**
     * @OA\Get(path="/navs",tags={"导航菜单"},summary="列表",
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function list()
    {
        $param = $this->request->param();
        $field = $param['field'] ?? 'id';
        $order = $param['order'] ?? 'desc';
        $where[] = ['status', '=', 1];
        $list = NavMenu::where($where)->order($field, $order)->select();
        $this->result(0,'success', $list);
    }
}