<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 项目
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\Project;
use app\common\model\ProjectIssue;
use think\facade\View;

class ProjectController extends AdminBaseController
{
    /**
     * @OA\Get(path="/project",tags={"项目"},summary="列表",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $param = $this->request->param();
            $field = $param['field'] ?? 'id';
            $order = $param['order'] ?? 'asc';
            $where[] = ['user_id', '=', $this->user_id];
            if (isset($param['keyword'])) {
                $where[] = ['name', 'like', '%' . $param['keyword'] . '%'];
            }
            if (isset($param['status'])) {
                $where[] = ['status', '=', $param['status']];
            }
            $list  = Project::where($where)->order($field, $order)->paginate(10);
            return $this->result($list->items(), '', ['count' => $list->total()]);
        }
        return View::fetch('app/project/list');
    }

    public function create()
    {
        View::assign('project');
        return View::fetch('app/project/listform');
    }

    /**
     * @OA\Post(path="/project",tags={"项目"},summary="新增",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="银行名称", property="card_name", type="string"),
     *           @OA\Property(description="银行卡号", property="card_num", type="number"),
     *           @OA\Property(description="银行备注", property="card_remark", type="string"),
     *           required={"card_name","card_num"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {
        $param = $this->request->param();
        $param['user_id'] = $this->user_id;
        $this->validate($param, 'app\common\validate\Project');
        $article = Project::create($param);
        $this->success('保存成功', $article);
    }

    /**
     * @OA\Get(path="/project/{id}",tags={"项目"},summary="详情",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($id)
    {
        $data = Project::find($id);
        $this->success('获取成功', $data);
    }

    public function edit($id)
    {
        $project = Project::find($id);
        View::assign('project', $project);
        return View::fetch('app/project/listform');
    }

    /**
     * @OA\Put(path="/project/{id}",tags={"项目"},summary="编辑",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="银行名称", property="card_name", type="string"),
     *           @OA\Property(description="银行卡号", property="card_num", type="number"),
     *           @OA\Property(description="银行备注", property="card_remark", type="string"),
     *           required={"card_name","card_num"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($id)
    {
        $param = $this->request->param();
        $param['id'] = $id;
        $param['user_id'] = $this->user_id;
        $this->validate($param, 'app\common\validate\Project');
        $article = Project::update($param);
        if (!$article) {
            $this->error("保存失败");
        }
        $this->success('保存成功', $article);
    }

    /**
     * @OA\Delete(path="/project/{id}",tags={"项目"},summary="删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($id)
    {
        $issue = ProjectIssue::where('project_id', $id)->find();
        if ($issue) {
            $this->error("请先删除此栏目下的内容");
        }
        $res = Project::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }
}
