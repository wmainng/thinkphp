<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 用户
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\User;
use think\facade\View;

class UserController extends AdminBaseController
{
    /**
     * @OA\Get(path="/user",tags={"用户"},summary="列表",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $request = $this->request->param();
            $where[] = ['user_type', '=', 0];
            if (isset($request['id'])) {
                $where[] = ['id', '=', $request['id']];
            }
            if (isset($request['username'])) {
                $where[] = ['username', '=', $request['username']];
            }
            if (isset($request['mobile'])) {
                $where[] = ['mobile', '=', $request['mobile']];
            }
            if (isset($request['sex'])) {
                $where[] = ['sex', '=', $request['sex']];
            }
            $list = User::where($where)->order('id desc')->paginate(10);
            return $this->result($list->items(), '', ['count' => $list->total()]);
        }
        return View::fetch('user/user/index');
    }

    /**
     * 显示创建资源表单页.
     *
     * @return string
     */
    public function create()
    {
        $data = [
            'username' => '',
            'password' => '',
            'mobile' => '',
            'email' => '',
            'sex' => 0
        ];
        View::assign('data',$data);
        return View::fetch('user/user/form');
    }

    public function save()
    {
        $param = $this->request->param();
        if (!empty($param['password'])) {
            $param['password'] = $this->auth()->credentials($param);
        } else {
            unset($param['password']);
        }
        $article = User::create($param);
        return $this->success('添加成功', $article);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return string
     */
    public function edit($id)
    {
        $data = User::find($id);
        View::assign('data', $data);
        return View::fetch('user/user/form');
    }

    /**
     * 保存更新的资源
     *
     */
    public function update($id)
    {
        $param = $this->request->param();
        $user = User::find($id);
        if (!empty($param['password'])) {
            $param['password'] = $this->auth()->credentials($param);
        } else {
            unset($param['password']);
        }
        $data = $user->allowField(['username', 'password', 'email', 'mobile', 'sex'])->save($param);
        $this->success('获取成功', $data);
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     */
    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
        $this->success('删除成功');
    }

    /**
     * 批量删除
     *
     */
    public function batchDelete()
    {
        $ids = $this->request->param('ids');
        $res = User::destroy($ids);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }

    /**
     * 个人信息
     * @return string
     */
    public function info()
    {
        $user = User::find($this->user_id);
        View::assign(['user' => $user]);
        return View::fetch('user/admin/info');
    }

    public function updateInfo()
    {
        $param = $this->request->param();
        $user = User::find($this->user_id);
        $user->allowField(['nickname','sex', 'email', 'mobile', 'sign'])->save($param);
        $this->success('资料修改成功');
    }

    /**
     * 密码
     * @return string
     */
    public function password()
    {
        return View::fetch('user/admin/password');
    }

    /**
     *
     * @return void
     */
    public function updatePassword()
    {
        $param = $this->request->param();
        if ($param['password'] != $param['password_confirm']) {
            $this->error("两次密码不一致");
        }
        $this->auth()->password($param);
        $this->success('密码修改成功');
    }
}
