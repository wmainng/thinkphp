<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 搜索历史
// +----------------------------------------------------------------------

namespace app\admin\controller;


use app\BaseController;
use app\common\model\Search;

class SearchController extends BaseController
{
    /**
     * @OA\Get(path="/search",tags={"搜索"},summary="列表",
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        $where = [];
        $request = $this->request;
        if (!empty($request['id'])) {
            $where[] = ['id', '=', $request['id']];
        }
        $sort = isset($request['sort']) ? $request['sort'] : 'id';
        $list = Search::where($where)->order($sort . ' desc')->paginate(10);
        if ($list->isEmpty()) {
            $data = [
                'code'    => 1,
                'message' => '数据为空',
            ];
            return json($data);
        }
        $data = [
            'code'    => 0,
            'message' => '正在请求中...',
            'count'   => $list->total(),
            'data'    => $list->items()
        ];
        return json($data);
    }

    public function hot()
    {
        $list = Search::field('id,keywords')->order('num','desc')->limit(10)->select();
        $this->success('Success', $list);
    }

    public function history()
    {
        $list = Search::field('id,keywords')->order('num','desc')->limit(10)->select();
        $this->success('Success', $list);
    }
}