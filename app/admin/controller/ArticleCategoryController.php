<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 分类管理
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\ArticleCategory;
use think\facade\View;

class ArticleCategoryController extends AdminBaseController
{
    /**
     * @OA\Get(path="/article_category",tags={"文章分类"},summary="列表",
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $request = $this->request->param();
            $where = [];
            if (!empty($request['id'])) {
                $where[] = ['id', '=', $request['id']];
            }
            if (!empty($request['name'])) {
                $where[] = ['name', '=', $request['name']];
            }
            $list = ArticleCategory::where($where)->order('id desc')->paginate(10);
            return $this->result($list->items(), '', ['count' => $list->total()]);
        }
        return View::fetch('app/content/category');
    }

    public function create()
    {
        View::assign([
            'categories' => ArticleCategory::categories(),
            'category'    => null
        ]);
        return View::fetch('app/content/categoryform');
    }
    
    /**
     * @OA\Post(path="/article_category",tags={"文章分类"},summary="新增",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="上级分类", property="parent_id", type="integer"),
     *           @OA\Property(description="分类名称", property="title", type="string"),
     *           @OA\Property(description="分类排序", property="sort", type="integer"),
     *           required={"parent_id", "title"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {
        ArticleCategory::create($this->request->param());
        return $this->success('保存成功');
    }

    /**
     * @OA\Get(path="/article_category/{id}",tags={"文章分类"},summary="详情",
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($id)
    {

    }

    public function edit($id)
    {
        View::assign([
            'categories' => ArticleCategory::categories(),
            'category'   => ArticleCategory::find($id)
        ]);
        return View::fetch('app/content/categoryform');
    }

    /**
     * @OA\Put(path="/article_category/{id}",tags={"文章分类"},summary="编辑",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="上级分类", property="parent_id", type="integer"),
     *           @OA\Property(description="分类名称", property="title", type="string"),
     *           @OA\Property(description="分类排序", property="sort", type="integer"),
     *           required={"parent_id", "title"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($id)
    {
        $data = $this->request->param();
        ArticleCategory::update($data, ['id' => $id]);
        return $this->success('保存成功');
    }

    /**
     * @OA\Delete(path="/article_category/{id}",tags={"文章分类"},summary="删除",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($id)
    {
        $result = ArticleCategory::destroy($id);
        if ($result) {
            $this->success('删除成功');
        }
        $this->error('删除失败');
    }
}