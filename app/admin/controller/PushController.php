<?php
declare (strict_types=1);
// +----------------------------------------------------------------------
// | 推送管理
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\model\Push;

class PushController extends AdminBaseController
{
    /**
     * @OA\Get(path="/push",tags={"消息推送"},summary="列表",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Parameter(name="role_id", in="query", description="角色ID", @OA\Schema(type="int")),
     *   @OA\Parameter(name="status", in="query", description="状态", @OA\Schema(type="int")),
     *   @OA\Parameter(name="street", in="query", description="镇编码", @OA\Schema(type="int")),
     *   @OA\Parameter(name="village", in="query", description="村编码", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        $param = $this->request->param();
        $field = $param['field'] ?? 'create_time';
        $order = $param['order'] ?? 'desc';
        $limit = $param['limit'] ?? 10;

        $where = [];
        if (isset($param['keyword'])) {
            $where[] = ['title', 'like', '%' . $param['keyword'] . '%'];
        }
        if (isset($param['status'])) {
            $where[] = ['status', '=', $param['status']];
        }

        $list  = Push::with(['user'	=> function($query) {
            $query->field('id,name,mobile');
        }])
            ->where($where)->order($field, $order)->paginate($limit);
        $count = $list->total();
        $list  = $list->items();
        $this->success('获取成功', compact('list', 'count'));
    }

    /**
     * @OA\Post(path="/push",tags={"消息推送"},summary="新增",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="用户id", property="user_id", type="integer"),
     *           @OA\Property(description="标题", property="title", type="string"),
     *           @OA\Property(description="内容", property="content", type="string"),
     *           required={"user_id","title","content"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {
        $param = $this->request->param();
        if ($param['user_id']) {
            foreach ($param['user_id'] as $user_id) {
                Push::message($user_id, $param['title'], $param['content']);
            }
        }
        $this->success('推送成功');
    }

    public function push()
    {
        // 建立socket连接到内部推送端口
        $client = stream_socket_client('tcp://127.0.0.1:5678', $errno, $errmsg, 1);
        // 推送的数据，包含uid字段，表示是给这个uid推送
        $data = array('uid'=>'uid1', 'percent'=>'88%');
        // 发送数据，注意5678端口是Text协议的端口，Text协议需要在数据末尾加上换行符
        fwrite($client, json_encode($data)."\n");
        // 读取推送结果
        echo fread($client, 8192);
    }
}