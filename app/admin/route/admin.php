<?php

use think\facade\Route;


// login
Route::get('login', 'Login/index');
Route::post('login', 'Login/save');
Route::get('logout', 'Login/logout');

Route::get('/', 'Index/index');
Route::get('index/console', 'Index/console');
Route::get('index/homepage1', 'Index/homepage1');
Route::get('index/homepage2', 'Index/homepage2');

// storage
Route::resource('storage', 'Storage');

// admin
Route::resource('admin', 'Admin');
Route::get('role/:id/permission', 'Role/permission');
Route::put('role/:id/permission', 'Role/assignPermission');
Route::resource('role', 'Role');
Route::delete('role$', 'Role/batchDelete');
Route::resource('permission', 'Permission');
Route::delete('permission$', 'Permission/batchDelete');

// user
Route::get('user/info', 'User/info');
Route::post('user/info', 'User/updateInfo');
Route::get('user/password', 'User/password');
Route::post('user/password', 'User/updatePassword');
Route::resource('user', 'User');
Route::resource('oauth', 'user.Oauth');

// content
Route::resource('article_category', 'ArticleCategory');
Route::resource('article', 'Article');
Route::resource('page', 'Page');
Route::resource('article_comment', 'Comment');

Route::get('search', 'Search/index');

// course
Route::resource('course_category', 'course_category');
Route::resource('course', 'course');
Route::delete('course', 'course/destroy');
Route::resource('course.lesson', 'CourseLesson');
Route::resource('course.document', 'CourseDocument');

// project
Route::resource('project', 'Project');
Route::resource('project.issue','ProjectIssue');

// finance
Route::resource('UserBalanceLog', 'UserBalanceLog');
Route::get('recharge', 'UserBalanceLog/recharge_list');
Route::get('transfer', 'UserBalanceLog/transfer_list');

// set
// system
Route::get('config/:id', 'Config/edit');
Route::put('config/:id', 'Config/update');

// message
Route::delete('message', 'Message/batchDelete');
Route::put('message/status', 'Message/batchStatus');
Route::resource('message', 'Message');

// nav
Route::resource('nav', 'Nav');
Route::resource('nav.menu','NavMenu');

// slide
Route::resource('slide', 'Slide');
Route::resource('slide.item','SlideItem');

// payment
Route::resource('payment', 'payment');

// wechat
Route::resource('wechat_replay', 'Replay');
Route::resource('wechat_menu', 'Menu');
Route::post('wechat_menu/publish', 'Menu/publish');
