<?php
// 事件定义文件
return [
    'bind'      => [
        'UserLogin' => 'app\common\event\UserLogin',
        // 更多事件绑定
    ],

    'listen'    => [
        'AppInit'  => [],
        'HttpRun'  => [],
        'HttpEnd'  => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'UserLogin' => ['app\common\listener\UserLogin'],
        // 更多事件监听
    ],

    'subscribe' => [
        'app\common\subscribe\User',
        // 更多事件订阅
    ],
];
