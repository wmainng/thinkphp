<?php
namespace app;

// 应用请求对象类
class Request extends \think\Request
{
    /**
     * 用户信息
     * @var object
     */
    protected object $user;
}
