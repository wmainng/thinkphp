<?php
// 应用公共文件

//函数的命名使用小写字母和下划线（小写字母开头）的方式
function auth()
{
    return new \helper\Auth;
}

/* 检查是否在微信中打开 */
function check_user_agent(){
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    if (strpos($user_agent, 'MicroMessenger') === false) {
        return false;
    }else{
        return true;
    }
}
