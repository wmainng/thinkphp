<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

// doc
Route::any('index', 'Index/index');

Route::any('demo', 'Index/demo');

// article
Route::get('article_categories', 'Article/category');
Route::get('article/search', 'Article/search');
Route::get('article/:id', 'Article/read');
Route::get('article', 'Article/index');

// wechat
Route::any('wechat', 'Wechat/index');

// public
Route::get('/config/website', 'Public/website');
Route::get('menu', 'Public/menu');
Route::get('slide', 'Public/slide');


Route::group('deposit', function () {
    Route::post('direct', 'Deposit/direct');

})->middleware('api');

// payment
Route::group('payment', function () {
    Route::post('create', 'Payment/create');
    Route::get('query', 'Payment/query');
    Route::post('transfer', 'Payment/transfer');
})->middleware('api');

//    Route::post('login', 'Wechat/login');
//
//    // auth
//    Route::group('auth', function () {
//        Route::get('captcha','Verification/index');
//        Route::post('captcha', 'Verification/captcha');
//        Route::post('register', 'register');
//        Route::post('login', 'auth.login/save');
//        Route::post('logout', 'auth.logout/index');
//    });

//    Route::get('attachment/cos_sts', 'attachment/cos_sts');

//    // comment
//    Route::get('comment/top', 'comment/top');
//
//    // search
//    Route::get('search/hot', 'search/hot');
//    Route::get('search/history', 'search/history');
//
//    // user_sign
//    Route::get('user_sign/status', 'user.SignIn/status');
//    Route::get('user_sign/top', 'user.SignIn/top');

//Route::group('api', function () {
//
//    // attachment
//    Route::resource('attachment', 'Attachment')->except(['create', 'edit']);
//
//    // search
//    Route::resource('search', 'search');
//
//    // article
//    Route::resource('article_category', 'article.article_category');
//    Route::resource('article', 'article.article');
//    Route::resource('article_tag', 'article.article_tag');
//
//    // comment
//    Route::post('comment/zan', 'comment/zan');
//    Route::post('comment/accept', 'comment/accept');
//    Route::resource('comment', 'comment');
//
//    // user
//    Route::get('user/profile', 'user.user/profile');
//    Route::get('user/phone', 'wechat.index/phone');
//    Route::resource('user', 'user.user');
//    Route::get('user/post', 'user.user/post');
//    Route::get('user/favorite', 'user.user/favorite');
//
//    // role
//    Route::resource('role', 'role');
//
//    // permission
//    Route::resource('permission', 'permission');
//
//    // project
//    Route::resource('project', 'Project')->except(['create', 'edit']);
//    Route::resource('project.issue','ProjectIssue')->except(['create', 'edit']);
//
//
//    Route::resource('recharge', 'Recharge')->except(['create', 'edit']);
//    Route::resource('transfer', 'Transfer')->except(['create', 'edit']);
//
//    // message
//    Route::get('message/new', 'message/new');
//    Route::resource('message', 'message');
//})->prefix('api.')->middleware('api');
//
//// im
//Route::group('api', function () {
//    Route::get('im/getMessages', 'im/getMessages');
//    Route::get('im/sendMessage', 'im/sendMessage');
//    Route::post('im/readMessage', 'im/readMessage');
//    Route::get('im/getList', 'im/getList');
//    Route::get('im/getMembers', 'im/getMembers');
//    Route::get('im/getFriends', 'im/getFriends');
//    Route::post('im/applyFriend', 'im/applyFriend');
//    Route::post('im/refuseFriend', 'im/refuseFriend');
//    Route::post('im/agreeFriend', 'im/agreeFriend');
//    Route::get('im/getGroups', 'im/getGroups');
//})->prefix('im.');
