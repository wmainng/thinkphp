<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 错误控制器
// +----------------------------------------------------------------------

namespace app\v1\controller;

class ErrorController
{
    public function __call($method, $args)
    {
        // 抛出 HTTP 异常
        //throw new \think\exception\HttpException(404, '页面异常');
        abort(404, '页面异常');
    }
}