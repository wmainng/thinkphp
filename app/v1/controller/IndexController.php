<?php

namespace app\v1\controller;

use helper\payment\ecpss\Ecpss;
use helper\payment\sumpay\QuickPay;
use helper\util\StringBuilder;

class IndexController extends ApiBaseController
{
    /**
     * 1.安装swagger-ui前端
     * git clone https://github.com/swagger-api/swagger-ui.git
     * 复制swagger-ui目录下dist文件到tp项目的public目录下,编辑dist目录下index.html文件，修改url地址为文档路由地址
     * 2.安装swagger-php后端
     * 进入你的项目目录执行如下命令：composer require zircote/swagger-php
     * 提示安装完成后会在你tp项目的vendor中生成一个zircote的组件文件夹，说明已经安装插件成功了。
     * 3.生成swagger.json文件 控制器方法生成或命令行生成
     * 4.编写swagger注释
     *
     * @OA\OpenApi(
     *    security={{"bearerAuth": {}}}
     * )
     *
     * @OA\Components(
     *     @OA\SecurityScheme(
     *         securityScheme="bearerAuth",
     *         type="http",
     *         scheme="bearer",
     *     )
     * )
     *
     * @OA\Info(
     *   title="开发文档",
     *   version="1.0.0",
     *   description="WebService API 是基于HTTPS/HTTP协议的数据接口，开发者可以使用任何客户端、服务器和开发语言，按照WebService API规范，按需构建HTTPS请求，并获取结果数据。"
     * )
     *
     * @OA\Server(
     *      url="{schema}://api.huoxun.cc/v1",
     *      description="",
     *      @OA\ServerVariable(
     *          serverVariable="schema",
     *          enum={"https", "http"},
     *          default="https"
     *      )
     * )
     */
    public function index()
    {
//        return json([
//            'code' => 0,
//            'msg'  => 'Success',
//            'data' => [
//                'method' => $this->request->method(),
//                'header' => $this->request->header(),
//                'query'  => $this->request->query(),
//                'body'   => $this->request->param()
//            ]
//        ]);
        $config['merNo'] = "53495";
        $config['channelNo'] = "150468523";
        $config['privateKey'] = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAKtY4l3kxWkUMP+CNw3oqMYFL9f5
1IvjVI0Ed4HYr/u58ROH7wJfKhyD2NAMk/R2DdKHQLmdAJLxpz7Dc5BKNgjpkYcalqjeg8S4tOpo
CdKO0DmalFQwULYivhvNKrsKrFoQ3LloIk7J5+RS6dN3Hh7E6Ty9aUHGLdZsmCpQeLAFAgMBAAEC
gYAc9KlUWqutpTbeht/M3uteg1PheJudhC4uonmbnMWokXSW61rNmM1iAjnqdqWuIyEU2bxrYYfO
TT4JEjh+Qrw8n3I5BW6AOoYkC6Rj6KJoJLRXvlXwqpVKaDrM5fzE3d+Q+TylaSSqP0M2Wb7E7553
dF+mGSmy2Af71PW5zc+LwQJBAPmouz5fDExBmv+PwonSR73lxBaxMJEwWgHQU0atxg7pRrGQyUYF
gAe2sdu6QmDMq/n8luWvXqBXPCb0VQR1s5ECQQCvsvk4V5CHG8Fuy/zraI0r1uqZyNtxrt1mRPh4
lJtrrOlsP/j31U21YDL5nyacJTo70PfWHN241NNv4avuetM1AkBcAHEYGAC5LUgIO5sOHP6xHTX4
ws1KA853cqTtm+Yr4o1ZrChv9GDs9sduWKJTd7k8g3e8JrcxUVRqaonV/eXxAkAC25xmKDZ3nCH3
VWYVxiuVrPc/7R50qO18/l40R+aHR9a8JgY8scGD077AhLAyFgDufCWr2+hXW07dIQSU6naJAkB2
7CvsQQOT8j7vdzuH6moXcvLAAdpzWxxZ8HzZ0jkXkzMN6TMx2zVnjUcvhjKGi91uUrD+CPa6+y1U
wOPLuu/C";

        $mer = new Ecpss($config);
        //echo $mer->applyMerchant();
        //echo $mer->merchantInQuery();
        echo $mer->subMerchantConf();
    }

    public function demo()
    {
        $config = [
            'appId' => "101024003",
            'publicKey' => "cert/dev_pub.cer",
            'privateKey' => "cert/yixuntiankong.pfx",
            'password' => "sumpay",
        ];
        $quickPay = new QuickPay($config);
        //$result = $quickPay->availableBank('100105097167');

        $result = $quickPay->checkCardNo('');
        halt($result);
    }
}