<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 文章管理
// +----------------------------------------------------------------------

namespace app\v1\controller;

use app\common\model\Article;
use app\common\model\ArticleCategory;

class ArticleController extends ApiBaseController
{
    /**
     * @OA\Get(path="/article_categories",tags={"公共"},summary="文章分类",
     *   @OA\Parameter(name="parent_id", in="query", description="上级分类ID", @OA\Schema(type="int", default="0")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function category()
    {
        $where[] = ['status', '=', 1];
        $request = $this->request->param();
        if (isset($request['parent_id'])) {
            $where[] = ['parent_id', '=', $request['parent_id']];
        }
        $data = ArticleCategory::field('id,parent_id,name,thumbnail,description')->where($where)->select();
        return $this->result($data);
    }

    /**
     * @OA\Get(path="/article",tags={"公共"},summary="文章列表",
     *   @OA\Parameter(name="category_id", in="query", description="分类ID", @OA\Schema(type="int")),
     *   @OA\Parameter(name="type", in="query", description="文章类型: 0文章,1单页面", @OA\Schema(type="int", default="0")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        $where = [];
        $request = $this->request->param();
        if (!empty($request['id'])) {
            $where[] = ['id', '=', $request['id']];
        }
        if (!empty($request['category_id'])) {
            $where[] = ['category_id', '=', $request['category_id']];
        }
        if (!empty($request['type'])) {
            $where[] = ['type', '=', $request['type']];
        }
        $limit = $request['limit'] ? intval($request['limit']) : 10;
        $list = Article::with([
            'category'	=> function($query) {
                $query->field('id,name');
            },
            'user' => function ($query) {
                $query->field('id,nickname,avatar');
            }
        ])->field('id,title,category_id,thumbnail,create_time,update_time')
            ->where($where)->order('id desc')
            ->paginate($limit);
        return $this->result($list);
    }

    /**
     * @OA\Get(path="/article/{id}",tags={"公共"},summary="文章详情",
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($id)
    {
        $article = Article::with([
            'user' => function($query) {
                $query->field('id,nickname');
            },
            'category'	=> function($query) {
                $query->field('id,name');
            }
        ])->find($id);
        return $this->result($article);
    }

    /**
     * @OA\Get(path="/article/search",tags={"公共"},summary="文章搜索",
     *   @OA\Parameter(name="category_id", in="query", description="分类ID", @OA\Schema(type="int")),
     *   @OA\Parameter(name="type", in="query", description="文章类型：0.文章；1.页面", @OA\Schema(type="int")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function search()
    {
        $where = [];
        $request = $this->request->param();
        if (!empty($request['category_id'])) {
            $where[] = ['category_id', '=', $request['category_id']];
        }
        if (!empty($request['type'])) {
            $where[] = ['type', '=', $request['type']];
        }
        $limit = $request['limit'] ? intval($request['limit']) : 10;
        $list = Article::with([
            'category'	=> function($query) {
                $query->field('id,name');
            }
        ])->field('id,title,category_id,thumbnail,create_time,update_time')
            ->where($where)
            ->order('id desc')
            ->limit($limit)
            ->select();
        return $this->result($list);
    }
}