<?php

namespace app\v1\controller;

use app\common\model\Config;
use app\common\model\Oauth;
use helper\tencent\Wechat;

class WechatController  extends ApiBaseController
{
    /**
     * 微信公众号
     * @return Wechat
     */
    private function wechat()
    {
        $config = Config::config('wechat');
        $ocr    = Config::config('ocr');
        $config['baidu'] = $ocr['baidu'];
        $config['ali']   = $ocr['ali'];
        $sdk = new Wechat($config);
        $sdk->setAccessToken(runtime_path());
        return $sdk;
    }

    /**
     * 微信公众号
     * @return void
     */
    public function index()
    {
        $sdk = $this->wechat();
        $sdk->valid();
        $sdk->responseMsg();
        die;
    }

    /**
     * 登陆
     * @return \think\Response
     */
    public function login()
    {
        $code = $this->request->param('code');
        $data = $this->wechat()->getToken($code, 1);

        // 返回用户信息
        $user = Oauth::miniProgram([
            'openid'       => $data['openid'],
            'union_id'     => $data['unionid'],
            'access_token' => $data['session_key'],
        ]);

        // 获取token
        $token = $this->auth()->getToken([
            'iat' => time(),
            'exp' => time() + 180 * 24 * 60 * 60,
            'sub' => $user->id,
            'aud' => 0
        ]);

        return $this->success('Success', ['user' => $user, 'token' => $token]);
    }

    /**
     * @return void
     */
    public function phone()
    {
        $code = $this->request->param('code');
        $response = $this->wechat()->getPhoneNumber($code);
        trace($response);
    }

    /**
     * 微信分享
     * @return void
     */
    public function share()
    {
        $signPackage = $this->wechat()->getSignPackage();

        echo $_GET['callback'].'('.json_encode(array(
                'c_url_back' => 'window.location.href = "'.$url_back.'";',//后退跳转
                'c_url' => 'window.location.href = "'.$url.'";',////分享跳转
                'config' => array(
                    'debug' =>false,//true
                    'appId' => $signPackage['appId'],
                    'timestamp' => $signPackage['timestamp'],
                    'nonceStr' => $signPackage['nonceStr'],
                    'signature' => $signPackage['signature'],
                    'jsApiList' => array(
                        'checkJsApi',
                        'onMenuShareTimeline',
                        'hideOptionMenu',
                        'onMenuShareAppMessage',
                        'hideMenuItems',
                        'showMenuItems'
                    )
                ),
                'share_app_info' => array(
                    'desc' => $app_desc,
                    'img_url' => $app_img,
                    'title' => $app_title,
                    'link' => $app_link,
                ),
                'share_timeline_info' => array(
                    'title' => $timeline_title,
                    'img_url' => $timeline_img,
                    'link' => $timeline_link,
                )
            )).')';
    }
}
