<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 充值管理
// +----------------------------------------------------------------------

namespace app\v1\controller;

use helper\FuLu;

class DepositController extends ApiBaseController
{
    private FuLu $fuLu;

    /**
     * 初始化
     */
    protected function initialize()
    {
        parent::initialize();

        $this->fuLu = new FuLu([
            'appKey'    => '3GEUXYMRz1YoX7irMIr73N8HuBLigGcpYex60y7wjwvIoSEfGoV9wy0L9gJWZmkv',
            'appSecret' => 'f06fac9154b54855be1d5f616478fbc4'
        ]);
    }

    /**
     * @OA\Post(path="/deposit/direct",tags={"充值"},summary="直充下单",
     *   @OA\Parameter(name="Authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="商品编号", property="product_id", type="string"),
     *           @OA\Property(description="充值账号", property="charge_account", type="string"),
     *           @OA\Property(description="购买数量", property="buy_num", type="number"),
     *           @OA\Property(description="外部订单号", property="customer_order_no", type="string"),
     *           required={"product_id","buy_num","customer_order_no"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function direct()
    {
        $param = $this->request->param();
        $res = $this->fuLu->direct($param);
        echo json_encode($res);die;
    }

    /**
     * @OA\Post(path="/deposit/card",tags={"充值"},summary="卡密下单",
     *   @OA\Parameter(name="Authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="商品编号", property="product_id", type="string"),
     *           @OA\Property(description="购买数量", property="buy_num", type="number"),
     *           @OA\Property(description="外部订单号", property="customer_order_no", type="string"),
     *           required={"product_id","buy_num","customer_order_no"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function card()
    {
        $param = $this->request->param();
        $res = $this->fuLu->card($param);
        echo json_encode($res);die;
    }

    /**
     * @OA\Get(path="/deposit/order",tags={"充值"},summary="订单查询",
     *   @OA\Parameter(name="Authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="customer_order_no", in="query", description="外部订单号", @OA\Schema(type="string")),
     *   @OA\Response(response="200", description="The User")
     * )
     */
    public function order()
    {
        $param = $this->request->param();
        $res = $this->fuLu->order($param);
        echo json_encode($res);die;
    }
}