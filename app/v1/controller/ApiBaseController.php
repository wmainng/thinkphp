<?php

namespace app\v1\controller;

use app\BaseController;

class ApiBaseController extends BaseController
{
    //protected string $domain;

    protected function initialize()
    {
        parent::initialize();
        //$this->domain = $this->request->domain();
    }
}