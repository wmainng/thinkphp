<?php
declare (strict_types=1);
// +----------------------------------------------------------------------
// | 优惠券记录
// +----------------------------------------------------------------------

namespace app\mall\controller;

use app\BaseController;
use app\mall\model\CouponLog;

class CouponLogController extends BaseController
{
    /**
     * @OA\Get(path="/coupon_log",tags={"优惠券记录"},summary="列表",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        $list = CouponLog::where('user_id', $this->user_id)->order('id', 'desc')->select();
        $this->success('Success', $list);
    }

    /**
     * @OA\Delete(path="/coupon_log/{id}",tags={"优惠券记录"},summary="删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($id)
    {
        $where     = [
            ['user_id', '=', $this->user_id],
            ['id', '=', $id]
        ];
        $couponLog = CouponLog::where($where)->find();
        $res       = $couponLog->delete();
        if (!$res) {
            $this->error('删除失败');
        }
        $this->success('删除成功');
    }
}