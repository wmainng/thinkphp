<?php
declare (strict_types=1);
// +----------------------------------------------------------------------
// | 优惠券
// +----------------------------------------------------------------------

namespace app\mall\controller;

use app\BaseController;
use app\mall\model\Coupon;

class CouponController extends BaseController
{
    /**
     * @OA\Get(path="/coupon",tags={"优惠券"},summary="列表",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Parameter(name="type", in="query", description="类型", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        $param = $this->request->param();
        $field = $param['field'] ?? 'id';
        $order = $param['order'] ?? 'desc';
        $limit = $param['limit'] ?? 10;

        $where = [];
        if (isset($param['keyword'])) {
            $where[] = ['name', 'like', '%' . $param['keyword'] . '%'];
        }
        if (isset($param['type'])) {
            $where[] = ['type', '=', $param['type']];
        }
        if (isset($param['status'])) {
            $where[] = ['status', '=', $param['status']];
        }

        $list  = Coupon::where($where)
            ->order($field, $order)
            ->paginate($limit);
        $count = $list->total();
        $list  = $list->items();
        $this->success('获取成功', compact('list', 'count'));
    }

    /**
     * @OA\Post(path="/coupon",tags={"优惠券"},summary="新增",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="名称", property="title", type="string"),
     *           @OA\Property(description="描述", property="desc", type="string"),
     *           @OA\Property(description="有效期", property="end_date", type="string"),
     *           @OA\Property(description="面额", property="price", type="number"),
     *           @OA\Property(description="可发放总数", property="total", type="number"),
     *           @OA\Property(description="每人限领", property="limit", type="integer"),
     *           @OA\Property(description="最低消费金额", property="price_limit", type="number"),
     *           @OA\Property(description="状态", property="status", type="integer"),
     *           required={"title","end_date","price","total","limit","price_limit"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {
        $param = $this->request->param();
        $this->validate($param, 'app\mall\validate\Coupon');
        $param['start_date'] = $param['start_date'] ?? date('Y-m-d');
        if (strtotime($param['end_date']) < strtotime($param['start_date'])) {
            $this->error("有效期错误");
        }
        $data = Coupon::create($param);
        if (!$data) {
            $this->error("保存失败");
        }
        $this->success('保存成功', $data);
    }

    /**
     * @OA\Get(path="/coupon/{id}",tags={"优惠券"},summary="详情",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($id)
    {
        $data = Coupon::find($id);
        $this->success('获取成功', $data);
    }

    /**
     * @OA\Put(path="/coupon/{id}",tags={"优惠券"},summary="编辑",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="名称", property="title", type="string"),
     *           @OA\Property(description="描述", property="desc", type="string"),
     *           @OA\Property(description="有效期", property="end_date", type="string"),
     *           @OA\Property(description="面额", property="price", type="number"),
     *           @OA\Property(description="可发放总数", property="total", type="number"),
     *           @OA\Property(description="每人限领", property="limit", type="integer"),
     *           @OA\Property(description="最低消费金额", property="price_limit", type="number"),
     *           @OA\Property(description="状态", property="status", type="integer"),
     *           required={"title","end_date","price","total","limit","price_limit"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($id)
    {
        $param            = $this->request->param();
        $param['id']      = $id;
        $param['user_id'] = $this->user_id;
        $this->validate($param, 'app\mall\validate\Coupon');
        if (strtotime($param['end_date']) < strtotime($param['start_date'])) {
            $this->error("有效期错误");
        }
        $data = Coupon::update($param);
        if (!$data) {
            $this->error("保存失败");
        }
        $this->success('保存成功', $data);
    }

    /**
     * @OA\Delete(path="/coupon/{id}",tags={"优惠券"},summary="删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($id)
    {
        $res = Coupon::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }
}