<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 收货地址
// +----------------------------------------------------------------------

namespace app\mall\controller;

use app\BaseController;
use app\common\model\UserAddress;

class AddressController extends BaseController
{
    /**
     * @OA\Get(path="/address",tags={"收货地址"},summary="列表",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        $param = $this->request->param();
        $field = $param['field'] ?? 'id';
        $order = $param['order'] ?? 'desc';
        $page  = $param['page'] ?? 1;
        $where = [];
        $where[] = ['user_id', '=', $this->user_id];
        $list = UserAddress::where($where)->order($field, $order)->page((int)$page,10)->select();
        $this->success('获取成功', $list);
    }

    /**
     * @OA\Post(path="/address",tags={"收货地址"},summary="新增",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="姓名", property="consignee", type="string"),
     *           @OA\Property(description="手机", property="mobile", type="string"),
     *           @OA\Property(description="省", property="province", type="integer"),
     *           @OA\Property(description="市", property="city", type="integer"),
     *           @OA\Property(description="区/县", property="district", type="integer"),
     *           @OA\Property(description="街道/乡镇", property="street", type="integer"),
     *           @OA\Property(description="社区/村", property="village", type="integer"),
     *           @OA\Property(description="详细地址", property="address", type="string"),
     *           @OA\Property(description="是否默认", property="is_default", type="integer"),
     *           required={"consignee", "mobile","province","city","district"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {
        $param = $this->request->param();
        $param['user_id'] = $this->user_id;
        $param['is_default']  = $param['is_default'] ?? 0;
        $this->validate($param, 'app\common\validate\Address');

        // 默认
        if ($param['is_default']) {
            UserAddress::syncDefault($param['user_id']);
        }
        UserAddress::create($param);
        $this->success('保存成功');
    }

    /**
     * @OA\Get(path="/address/{id}",tags={"收货地址"},summary="详情",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($id)
    {
        $data = UserAddress::find($id);
        $this->success('获取成功', $data);
    }

    /**
     * @OA\Put(path="/address/{id}",tags={"收货地址"},summary="编辑",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="multipart/form-data",
     *         @OA\Schema(
     *           @OA\Property(description="姓名", property="consignee", type="string"),
     *           @OA\Property(description="手机", property="mobile", type="string"),
     *           @OA\Property(description="省", property="province", type="integer"),
     *           @OA\Property(description="市", property="city", type="integer"),
     *           @OA\Property(description="区/县", property="district", type="integer"),
     *           @OA\Property(description="街道/乡镇", property="street", type="integer"),
     *           @OA\Property(description="社区/村", property="village", type="integer"),
     *           @OA\Property(description="详细地址", property="address", type="string"),
     *           @OA\Property(description="是否默认", property="is_default", type="integer"),
     *           required={"consignee", "mobile","province","city","district"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($id)
    {
        $param = $this->request->param();
        $param['id'] = $id;
        $param['user_id'] = $this->user_id;
        $param['is_default']  = $param['is_default'] ?? 0;

        $this->validate($param, 'app\common\validate\Address');

        // 默认
        if ($param['is_default']) {
            UserAddress::syncDefault($param['user_id']);
        }

        $res = UserAddress::update($param);
        if (!$res) {
            $this->error("保存失败");
        }
        $this->success('保存成功');
    }

    /**
     * @OA\Delete(path="/address/{id}",tags={"收货地址"},summary="删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($id)
    {
        $res = UserAddress::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }
}
