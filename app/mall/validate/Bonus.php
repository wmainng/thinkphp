<?php

namespace app\mall\validate;

use think\Validate;

class Bonus extends Validate
{
    protected $rule = [
        'name' => 'require',
        'type' => 'require',
        'total_price' => 'require',
        'price_type' => 'require',
        'start_time' => 'require',
        'end_time' => 'require',
    ];

    protected $message = [
        'name.require' => '名称必须',
        'type.require' => '类型必须',
        'total_price.require' => '总金额必须',
        'price_type.number' => '是否固定金额必须',
        'start_time.number' => '开始时间必须',
        'end_time.number' => '结束时间必须',
    ];
}