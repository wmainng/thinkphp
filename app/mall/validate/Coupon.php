<?php

namespace app\mall\validate;

use think\Validate;

class Coupon extends Validate
{
    protected $rule = [
        'title'       => 'require',
        'end_date'    => 'require|date',
        'price'       => 'require',
        'total'       => 'require',
        'limit'       => 'require',
        'price_limit' => 'require',
    ];

    protected $message = [
        'title.require'       => '名称必须',
        'end_date.require'    => '有效期必须',
        'price.require'       => '面额必须',
        'total.require'       => '可发放必须',
        'limit.require'       => '每人限领必须',
        'price_limit.require' => '消费金额必须',
    ];
}