<?php

namespace app\mall\validate;

use think\Validate;

class Draw extends Validate
{
    protected $rule = [
        'type'       => 'require',
        'name'       => 'require',
        'start_time' => 'require|date',
        'end_time'   => 'require|date',
        'join_type'  => 'require',
        'join_count' => 'require',
    ];

    protected $message = [
        'type.require'       => '活动类型必须',
        'name.require'       => '活动名称必须',
        'start_time.require' => '开始时间必须',
        'end_time.require'   => '结束时间必须',
        'join_type.require'  => '参与类型必须',
        'join_count.require' => '参与次数必须',
    ];
}