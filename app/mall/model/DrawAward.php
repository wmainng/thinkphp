<?php

namespace app\mall\model;

use think\model;

class DrawAward extends Model
{
    public function bonus()
    {
        return $this->hasOne(Bonus::class, 'id', 'bonus_id');
    }

    public function coupon()
    {
        return $this->hasOne(Coupon::class, 'id', 'coupon_id');
    }
}