<?php

namespace app\mall\model;

use think\model;

class Draw extends Model
{
    public function award()
    {
        return $this->hasMany(DrawAward::class, 'draw_id', 'id');
    }
}