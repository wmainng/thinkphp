<?php

namespace app\mall\model;

use think\model;
use think\model\concern\SoftDelete;

class CouponLog extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
}