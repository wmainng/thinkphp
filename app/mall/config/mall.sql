/*
 Navicat Premium Data Transfer

 Source Server         : 47.114.139.144
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : 47.114.139.144:3306
 Source Schema         : thinkphp

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 07/07/2022 09:19:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bonus
-- ----------------------------
DROP TABLE IF EXISTS `bonus`;
CREATE TABLE `bonus`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '红包名称',
  `type` tinyint(0) NOT NULL DEFAULT 1 COMMENT '红包类型 1活动红包 2关注红包 3奖品红包',
  `total_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '总面额',
  `price_type` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否固定金额 1固定金额',
  `fixed_price` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT '固定金额',
  `price_start` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT '最小金额',
  `price_end` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT '最大金额',
  `blessing` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '祝福语',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `status` tinyint(0) NOT NULL DEFAULT 0 COMMENT '红包状态 1正在进行 2过期 3失效',
  UNIQUE INDEX `bonus_id_uindex`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '红包' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bonus_log
-- ----------------------------
DROP TABLE IF EXISTS `bonus_log`;
CREATE TABLE `bonus_log`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `user_id` int(0) NOT NULL DEFAULT 0,
  `bonus_id` int(0) NOT NULL DEFAULT 0,
  `receive_time` timestamp(0) NULL DEFAULT NULL COMMENT '领取时间',
  `receive_price` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT '领取金额',
  `used_time` timestamp(0) NULL DEFAULT NULL COMMENT '使用时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户红包' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `desc` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `start_date` date NULL DEFAULT NULL COMMENT '有效期开始时间',
  `end_date` date NULL DEFAULT NULL COMMENT '有效期结束时间',
  `price` int(0) NOT NULL DEFAULT 0 COMMENT '面额',
  `price_limit` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '限额',
  `status` tinyint(0) NOT NULL DEFAULT 1 COMMENT '状态 1:有效 2:失效',
  `total` int(0) NOT NULL DEFAULT 0 COMMENT '总数',
  `points` int(0) NOT NULL DEFAULT 0 COMMENT '兑换所需积分',
  `limit` int(0) NOT NULL DEFAULT 0 COMMENT '张数限制',
  `receive_count` int(0) NOT NULL DEFAULT 0 COMMENT '领取数量',
  `use_count` int(0) NOT NULL DEFAULT 0 COMMENT '使用数量',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '优惠券' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for coupon_log
-- ----------------------------
DROP TABLE IF EXISTS `coupon_log`;
CREATE TABLE `coupon_log`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `user_id` int(0) NOT NULL DEFAULT 0,
  `coupon_id` int(0) NOT NULL DEFAULT 0,
  `order_id` int(0) NOT NULL DEFAULT 0,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `desc` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `start_date` date NULL DEFAULT NULL COMMENT '有效期开始时间',
  `end_date` date NULL DEFAULT NULL COMMENT '有效期结束时间',
  `price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '面额',
  `price_limit` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '限额',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '优惠券记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for draw
-- ----------------------------
DROP TABLE IF EXISTS `draw`;
CREATE TABLE `draw`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动名称',
  `content` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动详情',
  `start_time` timestamp(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` timestamp(0) NULL DEFAULT NULL COMMENT '结束时间',
  `thumb` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动图片',
  `type` tinyint(0) NOT NULL DEFAULT 0 COMMENT '活动类型 1刮刮卡 2大转盘 3砸金蛋 4生肖翻翻看',
  `join_type` tinyint(0) NOT NULL DEFAULT 0 COMMENT '参与类型  0共几次 1每天几次 2无限制',
  `join_count` tinyint(0) NOT NULL DEFAULT 0 COMMENT '参与次数',
  `join_total` tinyint(0) NOT NULL DEFAULT 0 COMMENT '总参与次数',
  `join_win` tinyint(0) NOT NULL DEFAULT 0 COMMENT '总中奖次数',
  `failed` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '未中奖说明',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `points` int(0) NOT NULL DEFAULT 0 COMMENT '0不消耗积分 大于0消耗积分',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动列表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for draw_award
-- ----------------------------
DROP TABLE IF EXISTS `draw_award`;
CREATE TABLE `draw_award`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `draw_id` int(0) NOT NULL DEFAULT 0 COMMENT '活动id',
  `level` tinyint(0) NOT NULL DEFAULT 0 COMMENT '奖品等级',
  `type` tinyint(0) NOT NULL DEFAULT 0 COMMENT '奖品类型 1积分 2红包 3优惠券',
  `number` int(0) NOT NULL DEFAULT 0 COMMENT '奖品数量',
  `count` int(0) NOT NULL DEFAULT 0 COMMENT '中奖数量',
  `probability` int(0) NOT NULL DEFAULT 0 COMMENT '中奖概率',
  `bonus_id` int(0) NOT NULL DEFAULT 0 COMMENT '红包id',
  `coupon_id` int(0) NOT NULL DEFAULT 0 COMMENT '优惠券id',
  `points` int(0) NOT NULL DEFAULT 0 COMMENT '奖品积分',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '奖品列表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for draw_log
-- ----------------------------
DROP TABLE IF EXISTS `draw_log`;
CREATE TABLE `draw_log`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `user_id` int(0) NOT NULL DEFAULT 0,
  `user_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `draw_id` int(0) NOT NULL DEFAULT 0 COMMENT '活动id',
  `draw_award_id` int(0) NOT NULL DEFAULT 0 COMMENT '中奖奖品id',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '领取备注',
  `status` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否中奖 0未中奖 1中奖',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '参与时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '领取时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '中奖记录' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
