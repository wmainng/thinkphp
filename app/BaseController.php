<?php
declare (strict_types = 1);

namespace app;

use helper\Auth;
use think\App;
use think\exception\HttpResponseException;
use think\exception\ValidateException;
use think\Response;
use think\Validate;

/**
 * 控制器基础类
 */
abstract class BaseController
{
    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [];

    /**
     * 用户
     * @var
     */
    protected $user;

    /**
     * 用户ID
     * @var int
     */
    protected $user_id;

    /**
     * 构造方法
     * @access public
     * @param  App  $app  应用对象
     */
    public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $this->app->request;

        // 控制器初始化
        $this->initialize();
    }

    /**
     * 初始化
     */
    protected function initialize()
    {
        $this->user    = $this->request->user;
        $this->user_id = $this->user->id ?? 0;
    }

    /**
     * 验证数据
     * @access protected
     * @param  array        $data     数据
     * @param  string|array $validate 验证器名或者验证规则数组
     * @param  array        $message  提示信息
     * @param  bool         $batch    是否批量验证
     * @return bool
     * @throws ValidateException
     */
    protected function validate(array $data, $validate, array $message = [], bool $batch = false)
    {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                [$validate, $scene] = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v     = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        $v->message($message);

        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        return $v->failException(true)->check($data);
    }

    /**
     * 操作成功跳转的快捷方法
     * @access protected
     * @param  mixed $msg 提示信息
     * @param  mixed $data 返回的数据
     * @param  string $url 跳转的URL地址
     * @param  integer $wait 跳转等待时间
     * @param  array $header 发送的Header信息
     * @return Response
     */
    protected function success($msg = '', $data = '', string $url = '', int $wait = 3, array $header = [])
    {
        if (!$url && isset($_SERVER["HTTP_REFERER"])) {
            $url = $_SERVER["HTTP_REFERER"];
        } else {
            $url = (strpos($url, '://') || 0 === strpos($url, '/')) ? $url : (string)$this->app->route->buildUrl($url);
        }

        $result = [
            'code' => 0,
            'message' => $msg,
            'data' => $data,
            'url' => $url,
            'wait' => $wait,
        ];

        $type = $this->getResponseType();
        // 把跳转模板的渲染下沉，这样在 response_send 行为里通过getData()获得的数据是一致性的格式
        if ('html' == strtolower($type)) {
            $type = 'view';
            $response = Response::create($this->app->config->get('jump.dispatch_success_tmpl'), $type)->assign($result)->header($header);
        } else {
            $response = Response::create($result, $type)->header($header);
        }

        throw new HttpResponseException($response);
    }

    /**
     * 操作错误跳转的快捷方法
     * @access protected
     * @param  mixed $msg 提示信息
     * @param  mixed $data 返回的数据
     * @param  string $url 跳转的URL地址
     * @param  integer $wait 跳转等待时间
     * @param  array $header 发送的Header信息
     * @return Response
     */
    protected function error($msg = '', $data = '', string $url = '', int $wait = 3, array $header = [])
    {
        if (!$url) {
            $url = $this->request->isAjax() ? '' : 'javascript:history.back(-1);';
        } else {
            $url = (strpos($url, '://') || 0 === strpos($url, '/')) ? $url : (string)$this->app->route->buildUrl($url);
        }

        $result = [
            'code' => 1,
            'message' => $msg,
            'data' => $data,
            'url' => $url,
            'wait' => $wait,
        ];

        $type = $this->getResponseType();

        if ('html' == strtolower($type)) {
            $type = 'view';
            $response = Response::create($this->app->config->get('jump.dispatch_error_tmpl'), $type)->assign($result)->header($header);
        } else {
            $response = Response::create($result, $type)->header($header);
        }

        throw new HttpResponseException($response);
    }

    /**
     * URL重定向
     * @access protected
     * @param  string $url 跳转的URL表达式
     * @param  integer $code http code
     * @param  array $with 隐式传参
     * @return void
     */
    protected function redirect(string $url, $params = [], $code = 302, $with = [])
    {
        $url = (strpos($url, '://') || 0 === strpos($url, '/')) ? $url : (string)$this->app->route->buildUrl($url,$params);

        $response = Response::create($url, 'redirect');

        $response->code($code)->with($with);

        throw new HttpResponseException($response);
    }

    /**
     * 获取当前的 response 输出类型
     * @access protected
     * @return string
     */
    private function getResponseType()
    {
        return request()->isAjax()
            ? 'json'
            : 'html';
    }

    /**
     * 返回封装后的API数据到客户端
     * @param mixed $data 要返回的数据
     * @param string $msg 提示信息
     * @param array $param 提示信息
     * @param int $httpCode
     * @param array $header 发送的Header信息
     * @return Response
     */
    protected function result($data = [], string $msg = 'Success', array $param = [], int $httpCode = 200, array $header = []): Response
    {
        $code = 0;
        $msg  = $msg ?? 'Success';
        if ($data === false) {
            $code = 1;
            $msg  = $msg != 'Success' ? $msg : 'Error';
        }
        $result = [
            'code'    => $code,
            'message' => $msg,
            'data'    => $data,
        ];
        $result   = $param ? array_merge($result, $param) : $result;
        $response = Response::create($result, 'json', $httpCode)->header($header);
        throw new HttpResponseException($response);
    }

    /**
     * 用户认证
     * @return Auth
     */
    protected function auth(): Auth
    {
        return new Auth();
    }
}
