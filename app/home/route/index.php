<?php

use think\facade\Route;


Route::get('/', 'Index/index');

Route::get('doc', 'Doc/index');

// forum
//Route::post('forum/reply', 'Comment/save');
Route::get('forum$', 'Index/forum');
Route::get('forum/:id', 'Index/read');

// payment
Route::rule('alipay','Notify/alipay','GET|POST');
Route::rule('wxpay','Notify/wxpay','GET|POST');

// auth
Route::get('verify', 'Auth/verification');
Route::post('code', 'Auth/captcha');
Route::rule('login','Auth/login','GET|POST');
Route::rule('register','Auth/register','GET|POST');
Route::rule('forget','Auth/forgetPassword','GET|POST');
Route::rule('reset','Auth/resetPassword','GET|POST');
Route::rule('logout','Auth/logout','GET|POST');
Route::get('oauth', 'Auth/oauth');

// cashier
Route::get('cashier', 'Cashier/index');

// user
Route::group('user', function () {
    Route::get('/','User/index');
    Route::get('issue', 'User/issue');
    Route::post('issue', 'User/doIssue');
    Route::get('set', 'User/set');
    Route::get('article', 'User/article');
    Route::get('message', 'Message/index');

    // 充值提现
    Route::get('recharge', 'User/recharge');
    Route::post('recharge', 'User/doRecharge');
    Route::get('tips', 'User/tips');
    Route::get('transfer', 'User/transfer');
    Route::post('transfer', 'User/doTransfer');
})->middleware('user');
