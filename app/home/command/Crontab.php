<?php
declare (strict_types = 1);

namespace app\home\command;

use app\common\model\Region;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class Crontab extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('crontab')
            ->addArgument('name', Argument::OPTIONAL, "your name")
            ->addOption('city', null, Option::VALUE_REQUIRED, 'city name')
            ->setDescription('the crontab command');
    }

    protected function execute(Input $input, Output $output)
    {
        $name = $input->getArgument('name');
        $name = $name ? trim($name) : '';

        if ($input->hasOption('option')) {
            $option = $input->getOption('option');
        } else {
            $option = '';
        }

        $response = '';
        if (method_exists($this, $name)) {
            $response = $this->$name($option);
        }

        // 指令输出
        $output->writeln("Task," . $name . '!' . $option . ':' . $response);
    }

    /**
     * 获取
     * @return string
     */
    private function district($option)
    {
        $list = Region::where('parent_adcode', $option)->select()->toArray();
        $district = new Region();
        $region = new \helper\util\Region();
        foreach ($list as $item) {
            $len = strlen((string)$item['adcode']);
            switch ($len) {
                case 6:
                    // 获取镇
                    $street = $region->street((string)$item['adcode']);
                    $district->replace()->saveAll($street);
                    foreach ($street as $vo) {
                        // 获取村
                        $village = $region->village((string)$vo['adcode']);
                        $district->replace()->saveAll($village);
                    }
                    break;
                case 9:
                    // 获取村
                    $village = $region->village((string)$item['adcode']);
                    $district->replace()->saveAll($village);
            }
        }
        return 'success';
    }

    private function music()
    {
        $filename = public_path() . 'music/top.txt';
        $subject  = file_get_contents($filename);

        // 标题
        $pattern = '/<b title="([^"]*)"/i';
        preg_match_all($pattern,$subject,$matches);
        $titles = $matches[1];

        // id
        $pattern = '/<tr id="([^"]*)"/i';
        preg_match_all($pattern,$subject,$matches);
        $ids = $matches[1];

        $client = new \GuzzleHttp\Client();
        foreach ($ids as $k=>$v) {
            $id   = substr($v, 0, 10);
            $url  = 'http://music.163.com/song/media/outer/url?id=' . $id .'.mp3';

            $titles[$k] = str_replace('&nbsp;', ' ', $titles[$k]);
            $file = public_path('music/163') . $titles[$k] . '.mp3';
            try {
                $client->get($url, ['sink' => $file]);
            } catch (\GuzzleHttp\Exception\GuzzleException $e) {
                $url  = 'https://music.163.com/song/media/outer/url?id=' . $id .'.mp3';
                try {
                    $client->get($url, ['sink' => $file]);
                } catch (\GuzzleHttp\Exception\GuzzleException $e) {
                    echo $e->getMessage() . "\r\n";
                    continue;
                }
                continue;
            }
        }
    }
}
