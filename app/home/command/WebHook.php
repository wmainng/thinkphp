<?php

namespace app\home\command;

class WebHook
{
    public function handle()
    {
        // 本地仓库路径 项目目录
        $local = '/var/www/html/test';

        // 密码 gitee项目管理webhook中设置
        $password = '5644564646464646';

        // 验证仓库目录
        if (!is_dir($local)) {
            header('HTTP/1.1 500 Internal Server Error');
            die('Local directory is missing');
        }

        // 验证请求体内容
        $payload = file_get_contents('php://input');
        if (!$payload) {
            header('HTTP/1.1 400 Bad Request');
            die('HTTP HEADER or POST is missing.');
        }

        // 验证密码
        $data = json_decode($payload, true);
        if (empty($data) || $data['password'] != $password) {
            header('HTTP/1.1 403 Permission Denied');
            die('Permission denied.');
        }

        // 执行linux命令 并输出执行结果,包括错误信息，在gitee webhook中可以查看和测试
        echo shell_exec("cd {$local} && git pull 2>&1");// 可以获取全部数据 2>&1：表示shell脚本执行过程中的错误信息会被输出
        //exec($cmd,$output,$status);//只能获取最后一行数据 $output：数组格式，用于存储2>&1输出的错误信息,$status：shell脚本的执行状态，0表示成功，其他都表示失败

        // 如果你的shell脚本返回的结果是一个列表，建议使用shell_exec，相应的，如果它返回的只是某种状态，建议用exec。
    }
}