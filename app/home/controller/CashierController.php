<?php

namespace app\home\controller;

use app\common\model\UserBalanceLog;
use helper\util\Encrypt;
use think\facade\View;

class CashierController extends HomeBaseController
{
    public function index()
    {
        $param = $this->request->param();
        if (isset($param['out_trade_no'])) {
            $data = Encrypt::decrypt($param['auth'], 'DES-EDE3', 'payment');
            if (!$data) {
                $this->error('错误');
            }
            $data = json_decode($data, true);
            if ($data['expire_time'] < time()) {
                $this->error('授权码已过期[auth expired]');
            }
            View::assign('data', $data);
        }
        return View::fetch('cashier/index');
    }

    public function save()
    {
        $param = $this->request->param();

        $log = UserBalanceLog::find($param['id']);

        $log->payment = $param['payment'];
        $log->save();

        // 调用支付
    }
}