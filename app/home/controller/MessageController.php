<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 用户消息
// +----------------------------------------------------------------------

namespace app\home\controller;

use app\common\model\Message;
use think\facade\View;

class MessageController extends HomeBaseController
{
    public function index()
    {
        $list = Message::where('user_id', $this->user_id)->order('id', 'desc')->select();
        View::assign('list', $list);
        return View::fetch('user/message');
    }

    /**
     * @OA\Get(path="/message/{id}",tags={"消息"},summary="详情",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($id)
    {
        $data = Message::find($id);
        $this->success('获取成功', $data);
    }

    public function edit($id)
    {
        $message = Message::find($id);
        if ($message->status == 0) {
            $message->status = 1;
            $message->save();
        }
        View::assign('data', $message);
        return View::fetch('admin/app/message/edit');
    }

    /**
     * @OA\Put(path="/message/{id}",tags={"消息"},summary="已读",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($id)
    {
        Message::update([
            'id'      => $id,
            'user_id' => $this->user_id,
            'status'  => 1
        ]);
        $this->success('已读');
    }

    /**
     * @OA\Delete(path="/message/{id}",tags={"消息"},summary="删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($id)
    {
        $res = Message::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }

    /**
     * @OA\Delete(path="/message",tags={"消息"},summary="批量删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="ids", in="path", description="ids", @OA\Schema(type="array")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function batchDelete()
    {
        $ids = $this->request->param('ids');
        $res = Message::destroy($ids);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }

    /**
     * @OA\Put(path="/message/status",tags={"消息"},summary="批量已读",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="ids", in="path", description="ids", @OA\Schema(type="array")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function batchStatus()
    {
        $ids = $this->request->param('ids');
        Message::where('id', 'in', $ids)->update(['status'  => 1]);
        $this->success('已读');
    }

    /**
     * @OA\Delete(path="/message",tags={"消息"},summary="清空",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function clear()
    {
        Message::where('user_id', $this->user_id)->delete();
        $this->success('操作成功');
    }

    /**
     * @OA\Get(path="/message/new",tags={"消息"},summary="最新",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function new()
    {
        if(empty($this->user_id)){
            $data['count']  = 0;
        }else{
            $data['count'] = Message::where('user_id',$this->user_id)
                ->whereTime('create_time', 'today')
                ->where('status',0)
                ->count();
        }
        $this->success('请求成功', $data);
    }
}
