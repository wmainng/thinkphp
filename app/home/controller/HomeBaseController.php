<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 前台控制器基类
// +----------------------------------------------------------------------

namespace app\home\controller;

use app\BaseController;
use app\common\model\Config;
use app\common\model\Payment;
use think\facade\View;

class HomeBaseController extends BaseController
{
    protected array $website;

    public function initialize()
    {
        parent::initialize();
        $this->website = $this->config('website');
        $this->website['domain'] = $this->request->domain();
        View::assign([
            'site_info' => $this->website,
            'user' => (array) $this->auth()->user()
        ]);
    }

    /**
     * 获取配置信息
     * @param string $key
     * @return array|mixed
     */
    protected function config(string $key)
    {
        return Config::config($key);
    }

    /**
     * 获取支付信息
     * @param array $where
     * @return array
     */
    protected function payment(array $where = [])
    {
        return Payment::field('id,name')->where($where)->where('status', 1)->select();
    }
}