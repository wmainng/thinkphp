<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 错误控制器
// +----------------------------------------------------------------------

namespace app\home\controller;

use think\facade\View;

class ErrorController
{
    public function __call($method, $args)
    {
        return View::fetch('template/404');
    }
}