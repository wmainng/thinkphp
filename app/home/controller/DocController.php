<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | API文档
// +----------------------------------------------------------------------

namespace app\home\controller;

use think\facade\Request;
use think\facade\View;

class DocController extends HomeBaseController
{
    public function index()
    {
        //PUT、POST请求方式的注解格式不一样
        //Paramter参数的in参数
        //1、header:参数在header头中传递
        //2、query：参数在地址后传递如 http://dev.think.com?id=1
        //3、path:参数在rest地址中如 http://dev.think.com/user/1
        //4、cookie:参数在cookie中传递

        //Schema配置
        //1、type：指定字段类型
        //2、default：指定字段默认值

        //header('Content-Type: application/x-yaml');
        //echo $openapi->toYaml();

        //header('Content-Type: application/json');
        //echo $openapi->toJson();

        $version   = 'v1';
        $directory = app()->getRootPath() . 'app/' . $version . '/controller';
        $openapi = \OpenApi\Generator::scan([$directory]);

        $path    = app()->getRootPath() . 'public/' . $version;
        $file    = 'swagger.json';
        if(!is_dir($path)){
            mkdir($path,0777,true);
        }

        $openapi->saveAs($path. '/'. $file);

        View::assign('url', Request::domain() . '/' . $version . '/' . $file);
        return View::fetch('template/doc');
    }
}