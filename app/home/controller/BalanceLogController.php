<?php
declare (strict_types=1);
// +----------------------------------------------------------------------
// | 余额变动记录
// +----------------------------------------------------------------------

namespace app\home\controller;


use app\BaseController;
use app\common\model\UserBalanceLog;

class BalanceLogController extends BaseController
{
    /**
     * @OA\Get(path="/balance_log",tags={"余额变动记录"},summary="明细",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Parameter(name="type", in="query", description="类型,如:0,1,2,3", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        $param = $this->request->param();
        $field = $param['field'] ?? 'id';
        $order = $param['order'] ?? 'DESC';
        $limit = $param['limit'] ?? 10;

        $where['user_id'] = $this->user_id;
        if (isset($param['create_time'])) {
            $where['create_time'] = $param['create_time'];
        }
        $list = UserBalanceLog::withSearch(['user_id', 'create_time'], $where)
            ->order($field, $order)
            ->paginate($limit);
        $count = $list->total();
        $list  = $list->items();
        $this->success('获取成功', compact('list', 'count'));
    }
}