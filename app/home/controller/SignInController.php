<?php
declare (strict_types = 1);

namespace app\home\controller;


use app\BaseController;
use app\common\model\UserSign;

class SignInController extends BaseController
{
    /**
     * 获取签到状态
     * @param int $user_id
     */
    public function status($user_id = 0)
    {
        $sign = UserSign::where('user_id', '=', $user_id)->whereDay('created_at')->find();
        $sign['signed'] = $sign ? 1 : 0;
        if (!$sign['signed']) {
            $sign = [
                'experience' => 1,
                'days'       => 0,
            ];
        }
        $data = [
            'code'    => 0,
            'message' => '正在请求中...',
            'data'    => $sign
        ];
        return json($data);
    }

    /**
     * 获取签到排行
     */
    public function top()
    {
        // 最新签到
        $data[0] = UserSign::with(['user'=> function($query) {
            $query->field('id,username,avatar');
        }
        ])->order('created_at desc')->limit(20)->select();
        // 今日最快
        $data[1] = UserSign::with(['user'=> function($query) {
            $query->field('id,username,avatar');
        }
        ])->order('created_at asc')->limit(20)->select();
        // 总签到榜
        $data[2] = UserSign::with(['user'=> function($query) {
            $query->field('id,username,avatar');
        }
        ])->order('days desc')->limit(20)->select();
        return $this->success('', $data);
    }

    public function in()
    {
        if (!auth()->check()) {
            $this->error('请登录');
        }
    }
}