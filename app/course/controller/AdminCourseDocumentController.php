<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 课件
// +----------------------------------------------------------------------

namespace app\course\controller;

use app\BaseController;
use app\course\model\Course;
use app\course\model\CourseDocument;
use think\facade\View;

class AdminCourseDocumentController extends BaseController
{
    /**
     * @OA\Get(path="/course/{course_id}/document",tags={"课程课件"},summary="列表",
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="The User")
     * )
     */
    public function index($course_id)
    {
        if ($this->request->isAjax()) {
            $list = CourseDocument::where('course_id', $course_id)->order('id desc')->paginate(10);
            $data = [
                'code'    => 0,
                'message' => 'success',
                'count'   => $list->total(),
                'data'    => $list->items()
            ];
            return json($data);
        }
        return View::fetch('admin/app/course/document');
    }

    public function create()
    {
        View::assign('document');
        return View::fetch('admin/app/course/documentform');
    }

    /**
     * @OA\Post(path="/course/{course_id}/document",tags={"课程课件"},summary="新增",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="course_id", in="path", description="course_id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="课件名称", property="title", type="string"),
     *           @OA\Property(description="课件路径", property="filepath", type="string"),
     *           @OA\Property(description="排序", property="sort", type="integer"),
     *           required={"title", "filepath"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save($course_id)
    {
        $course = Course::find($course_id);
        if (!$course) {
            $this->error('无数据');
        }
        $param  = $this->request->param();
        $param['course_id'] = $course_id;
        $param['user_id'] = $this->user_id;
        $document = CourseDocument::create($param);
        $this->success('操作成功', $document);
    }

    public function edit($id)
    {
        $document = CourseDocument::find($id);
        View::assign('document', $document);
        return View::fetch('admin/app/course/documentform');
    }

    /**
     * @OA\Put(path="/course/{course_id}/document/{id}",tags={"课程课件"},summary="编辑",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="course_id", in="path", description="course_id", @OA\Schema(type="int")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="课件名称", property="title", type="string"),
     *           @OA\Property(description="课件路径", property="filepath", type="string"),
     *           @OA\Property(description="排序", property="sort", type="integer"),
     *           required={"title", "filepath"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($course_id, $id)
    {
        $course = Course::find($course_id);
        if (!$course) {
            $this->error('无数据');
        }
        $param  = $this->request->param();

        $lesson = CourseDocument::find($id);

        $param['course_id'] = $course_id;
        $param['user_id'] = $this->user_id;
        $lesson = $lesson->save($param);
        if (!$lesson) {
            $this->error('操作失败');
        }
        $this->success('操作成功', $lesson);
    }

    /**
     * @OA\Delete(path="/course/{course_id}/document/{id}",tags={"课程课件"},summary="删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="course_id", in="path", description="course_id", @OA\Schema(type="int")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($course_id, $id)
    {
        $course = Course::find($course_id);
        if (!$course) {
            $this->error('无数据');
        }
        $res = CourseDocument::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }
}