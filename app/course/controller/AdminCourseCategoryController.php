<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 课程分类
// +----------------------------------------------------------------------

namespace app\course\controller;

use app\BaseController;
use app\course\model\CourseCategory;
use think\facade\View;

class AdminCourseCategoryController extends BaseController
{
    /**
     * @OA\Get(path="/course_category",tags={"课程分类"},summary="列表",
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $request = $this->request->param();
            $where = [];
            if (!empty($request['id'])) {
                $where[] = ['id', '=', $request['id']];
            }
            if (!empty($request['name'])) {
                $where[] = ['name', '=', $request['username']];
            }
            $list = CourseCategory::where($where)->order('id desc')->paginate(10);
            return $this->result(0,'', $list->items(), ['count' => $list->total()]);
        }
        return View::fetch('admin/app/course/category');
    }

    public function create()
    {
        View::assign([
            'categories' => CourseCategory::categories(),
            'category'   => null
        ]);
        return View::fetch('admin/app/course/categoryform');
    }

    /**
     * @OA\Post(path="/course_category",tags={"课程分类"},summary="新增",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="文章名称", property="title", type="string", default="dd"),
     *           @OA\Property(description="文章内容", property="content", type="string"),
     *           required={"title", "content"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save()
    {
        CourseCategory::create($this->request->param());
        return $this->success('保存成功');
    }

    /**
     * @OA\Get(path="/course_category/{id}",tags={"课程分类"},summary="详情",
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function read($id)
    {
        $data = CourseCategory::find($id);
        $this->success('保存成功', $data);
    }

    public function edit($id)
    {
        View::assign([
            'categories' => CourseCategory::categories(),
            'category'   => CourseCategory::find($id)
        ]);
        return View::fetch('admin/app/course/categoryform');
    }

    /**
     * @OA\Put(path="/course_category/{id}",tags={"课程分类"},summary="编辑",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="文章名称", property="title", type="string"),
     *           @OA\Property(description="文章内容", property="content", type="string"),
     *           required={"title", "content"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($id)
    {
        $data = CourseCategory::update($this->request->param(), ['id' => $id]);
        $this->success('保存成功', $data);
    }

    /**
     * @OA\Delete(path="/course_category/{id}",tags={"课程分类"},summary="删除",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($id)
    {
        $result = CourseCategory::destroy($id);
        if ($result) {
            return $this->success('删除成功');
        }
        return $this->error('删除失败');
    }

    public function list()
    {
        $request = $this->request->param();
        $where = [];
        if (!empty($request['id'])) {
            $where[] = ['id', '=', $request['id']];
        }
        if (!empty($request['name'])) {
            $where[] = ['name', '=', $request['username']];
        }
        $list = CourseCategory::scope('field')->where($where)->order('id desc')->paginate(10);
        if ($list->isEmpty()) {
            return json('数据为空', 0);
        }
        return $this->result(0,'', $list->items(), ['count' => $list->total()]);
    }
}