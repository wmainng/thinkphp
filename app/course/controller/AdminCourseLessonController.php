<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 课程章节
// +----------------------------------------------------------------------

namespace app\course\controller;

use app\BaseController;
use app\course\model\Course;
use app\course\model\CourseLesson;
use think\facade\View;

class AdminCourseLessonController extends BaseController
{
    /**
     * @OA\Get(path="/course/{course_id}/lesson",tags={"课程章节"},summary="列表",
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="The User")
     * )
     */
    public function index($course_id)
    {
        if ($this->request->isAjax()) {
            $list = CourseLesson::where('course_id', $course_id)->order('id desc')->paginate(10);
            $data = [
                'code'    => 0,
                'message' => 'success',
                'count'   => $list->total(),
                'data'    => $list->items()
            ];
            return json($data);
        }
        return View::fetch('admin/app/course/lesson');
    }

    public function create()
    {
        View::assign('lesson');
        return View::fetch('admin/app/course/lessonform');
    }

    /**
     * @OA\Post(path="/course/{course_id}/lesson",tags={"课程章节"},summary="新增",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="course_id", in="path", description="course_id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="章节名称", property="title", type="string"),
     *           @OA\Property(description="章节封面", property="thumb", type="string"),
     *           @OA\Property(description="章节类型", property="type", type="integer"),
     *           @OA\Property(description="音视频URL", property="video_url", type="string"),
     *           @OA\Property(description="音视频时长", property="video_time", type="string"),
     *           @OA\Property(description="章节内容", property="content", type="string"),
     *           @OA\Property(description="试听章节", property="is_free", type="integer"),
     *           @OA\Property(description="试听时间", property="free_time", type="string"),
     *           @OA\Property(description="排序", property="sort", type="integer"),
     *           @OA\Property(description="状态", property="status", type="integer"),
     *           required={"title", "content"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function save($course_id)
    {
        $course = Course::find($course_id);
        if (!$course) {
            $this->error('无数据');
        }
        $param  = $this->request->param(['title', 'thumb', 'content', 'type', 'video_url', 'video_time', 'is_free',
                                         'free_time', 'sort', 'status']);
        $param['course_id'] = $course_id;
        $param['user_id'] = $this->user_id;
        $lesson = CourseLesson::create($param);
        $this->success('操作成功', $lesson);
    }

    public function edit($id)
    {
        $lesson = CourseLesson::find($id);
        View::assign('lesson', $lesson);
        return View::fetch('admin/app/course/lessonform');
    }

    /**
     * @OA\Put(path="/course/{course_id}/lesson/{id}",tags={"课程章节"},summary="编辑",
     *   @OA\Parameter(name="token", in="header", description="token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="course_id", in="path", description="course_id", @OA\Schema(type="int")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\RequestBody(
     *     @OA\MediaType(mediaType="content-type/json",
     *         @OA\Schema(
     *           @OA\Property(description="章节名称", property="title", type="string"),
     *           @OA\Property(description="章节封面", property="thumb", type="string"),
     *           @OA\Property(description="章节类型", property="type", type="integer"),
     *           @OA\Property(description="音视频URL", property="video_url", type="string"),
     *           @OA\Property(description="音视频时长", property="video_time", type="string"),
     *           @OA\Property(description="章节内容", property="content", type="string"),
     *           @OA\Property(description="试听章节", property="is_free", type="integer"),
     *           @OA\Property(description="试听时间", property="free_time", type="string"),
     *           @OA\Property(description="排序", property="sort", type="integer"),
     *           @OA\Property(description="状态", property="status", type="integer"),
     *           required={"title", "content"})
     *       )
     *     ),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function update($course_id, $id)
    {
        $course = Course::find($course_id);
        if (!$course) {
            $this->error('无数据');
        }
        $param  = $this->request->param(['title', 'thumb', 'content', 'type', 'video_url', 'video_time', 'is_free',
                                         'free_time', 'sort', 'status']);

        $lesson = CourseLesson::find($id);

        $param['course_id'] = $course_id;
        $param['user_id'] = $this->user_id;
        $lesson = $lesson->save($param);
        if (!$lesson) {
            $this->error('操作失败');
        }
        $this->success('操作成功', $lesson);
    }

    /**
     * @OA\Delete(path="/course/{course_id}/lesson/{id}",tags={"课程章节"},summary="删除",
     *   @OA\Parameter(name="authorization", in="header", description="Bearer token", @OA\Schema(type="string")),
     *   @OA\Parameter(name="course_id", in="path", description="course_id", @OA\Schema(type="int")),
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="successful operation")
     * )
     */
    public function delete($course_id, $id)
    {
        $course = Course::find($course_id);
        if (!$course) {
            $this->error('无数据');
        }
        $res = CourseLesson::destroy($id);
        if (!$res) {
            $this->error("删除失败");
        }
        $this->success('删除成功');
    }
}