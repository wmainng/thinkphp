<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 课程
// +----------------------------------------------------------------------

namespace app\course\controller;

use app\BaseController;
use app\course\model\Course;

class CourseController extends BaseController
{
    /**
     * @OA\Get(path="/course",tags={"课程"},summary="列表",
     *   @OA\Parameter(name="page", in="query", description="页码", @OA\Schema(type="int", default="1")),
     *   @OA\Parameter(name="limit", in="query", description="行数", @OA\Schema(type="int", default="10")),
     *   @OA\Response(response="200", description="The User")
     * )
     */
    public function index()
    {
        $param = $this->request->param();
        $where = [];
        if (!empty($param['keywords'])) {
            $where[] = ['title', 'like', '%' . $param['keywords'] . '%'];
        }
        if (!empty($param['category_id'])) {
            $where[] = ['category_id', '=', $param['category_id']];
        }
        $list = Course::with(['category' => function ($query) {
            $query->field('id,name');
        }])->where($where)->order('id desc')->paginate(10);
        $this->success('Success', $list);
    }

    /**
     * @OA\Get(path="/course/{id}",tags={"课程"},summary="详情",
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="The User")
     * )
     */
    public function read($id)
    {
        $data = Course::with(['user' => function ($query) {
            $query->field('id,nickname');
        }])->find($id);
        $this->success('获取成功', $data);
    }

    /**
     * @OA\Get(path="/course/top",tags={"课程"},summary="首页推荐",
     *   @OA\Parameter(name="id", in="path", description="id", @OA\Schema(type="int")),
     *   @OA\Response(response="200", description="The User")
     * )
     */
    public function top()
    {
        $where[] = ['status', '=', 1];
        $list = Course::with(['category' => function ($query) {
            $query->field('id,name');
        }])->where($where)->order('id', 'desc')->limit(5)->select();
        $this->success('Success', $list);
    }

    /**
     * 收藏
     */
    public function collection()
    {
        
    }
}