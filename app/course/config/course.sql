/*
 Navicat Premium Data Transfer

 Source Server         : 47.114.139.144
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : 47.114.139.144:3306
 Source Schema         : thinkphp

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 07/07/2022 09:08:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book`  (
    `id` int(0) NOT NULL AUTO_INCREMENT,
    `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
    `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
    `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识',
    `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
    `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
    `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文档' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for book_section
-- ----------------------------
DROP TABLE IF EXISTS `book_section`;
CREATE TABLE `book_section`  (
    `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `parent_id` int(0) NOT NULL DEFAULT 0 COMMENT '上级id',
    `book__id` int(0) NOT NULL DEFAULT 0 COMMENT '文档id',
    `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目录标题',
    `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件路径',
    `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '章节内容',
    `order` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排序',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '章节' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `category_id` int(0) NULL DEFAULT 0,
  `user_id` int(0) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thumb` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `stock` int(0) NOT NULL DEFAULT 0,
  `price` decimal(10, 2) NOT NULL DEFAULT 0.00,
  `type` tinyint(1) NOT NULL DEFAULT 0,
  `sort` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '课堂' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (1, 1, 1, '课程1', 'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-dc-site/094a9dc0-50c0-11eb-b680-7980c8a877b8.jpg', '<p>https://www.microsoft.com/zh-cn/software-download/windows10</p><p>F12 - 切换设备 - F5- 选择Win10版本</p><p><br/></p><p><img src=\"https://bengbu.link/storage/image/20211020/1634720200121297.jpg\" title=\"1634720200121297.jpg\" alt=\"huge.jpg\"/></p>', 0, 10.00, 0, '0', '1', '2021-09-27 19:59:47', '2021-09-27 19:59:47');
INSERT INTO `course` VALUES (2, 1, 1, '课程2', 'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-dc-site/094a9dc0-50c0-11eb-b680-7980c8a877b8.jpg', '<p>https://www.microsoft.com/zh-cn/software-download/windows10</p><p>F12 - 切换设备 - F5- 选择Win10版本</p><p><br/></p>', 0, 0.00, 0, '0', '1', '2021-09-27 19:59:47', '2021-09-27 19:59:47');

-- ----------------------------
-- Table structure for course_category
-- ----------------------------
DROP TABLE IF EXISTS `course_category`;
CREATE TABLE `course_category`  (
  `id` bigint unsigned NOT NULL,
  `parent_id` int(0) NOT NULL DEFAULT 0 COMMENT '上级ID',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '图标',
  `sort` int(0) NOT NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course_category
-- ----------------------------
INSERT INTO `course_category` VALUES (1, 0, '公务员', NULL, 0, 1, '2020-05-27 00:18:17', '2021-09-27 19:41:12', NULL);
INSERT INTO `course_category` VALUES (2, 0, '事业单位', NULL, 0, 1, '2020-05-27 00:18:48', '2021-09-27 19:41:28', NULL);
INSERT INTO `course_category` VALUES (3, 0, '教师考编', NULL, 0, 1, '2020-05-27 00:19:09', '2021-09-27 19:41:50', NULL);
INSERT INTO `course_category` VALUES (4, 0, '遴选', NULL, 0, 1, '2020-05-27 00:28:21', '2021-09-27 19:41:59', NULL);
INSERT INTO `course_category` VALUES (5, 1, '笔试', '', 0, 1, '2020-05-27 00:28:21', '2022-03-09 22:31:03', NULL);
INSERT INTO `course_category` VALUES (6, 1, '面试', '', 0, 1, '2020-05-27 00:28:21', '2022-03-09 22:30:52', NULL);
INSERT INTO `course_category` VALUES (7, 2, '笔试', '', 0, 1, '2021-09-27 20:13:57', '2021-09-27 20:13:57', NULL);
INSERT INTO `course_category` VALUES (8, 2, '面试', '', 0, 1, '2021-09-27 20:14:18', '2021-09-27 20:14:18', NULL);

-- ----------------------------
-- Table structure for course_comment
-- ----------------------------
DROP TABLE IF EXISTS `course_comment`;
CREATE TABLE `course_comment`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for course_document
-- ----------------------------
DROP TABLE IF EXISTS `course_document`;
CREATE TABLE `course_document`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `course_id` int(0) NOT NULL DEFAULT 0,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `filepath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sort` int(0) NOT NULL DEFAULT 0,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '课件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course_document
-- ----------------------------
INSERT INTO `course_document` VALUES (1, 1, '课件1', 'https://bengbu.link/storage/video/20210929/bfff7b94792848b7b3d6b641e281dcc4.flac', 0, '2021-10-20 14:51:37', '2021-10-20 14:51:39');
INSERT INTO `course_document` VALUES (2, 2, '课件', 'https://bengbu.link/storage/file/20211021/f63b841ca5633890faea934254924e4d.docx', 0, '2021-10-21 02:26:10', '2021-10-21 02:26:10');

-- ----------------------------
-- Table structure for course_lesson
-- ----------------------------
DROP TABLE IF EXISTS `course_lesson`;
CREATE TABLE `course_lesson`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `user_id` int(0) NULL DEFAULT NULL,
  `course_id` int(0) NOT NULL DEFAULT 0,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thumb` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` tinyint(0) NOT NULL DEFAULT 0 COMMENT '章节类型 0.图文章节 1.音频章节 2.视频章节',
  `video_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `video_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `is_free` tinyint(0) NOT NULL DEFAULT 0,
  `free_time` int(0) NOT NULL DEFAULT 0,
  `sort` int(0) NOT NULL DEFAULT 0,
  `status` tinyint(0) NOT NULL DEFAULT 0,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course_lesson
-- ----------------------------
INSERT INTO `course_lesson` VALUES (1, 1, 1, '测试1', '', 0, 'https://img.cdn.aliyun.dcloud.net.cn/guide/uniapp/%E7%AC%AC1%E8%AE%B2%EF%BC%88uni-app%E4%BA%A7%E5%93%81%E4%BB%8B%E7%BB%8D%EF%BC%89-%20DCloud%E5%AE%98%E6%96%B9%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B@20200317.mp4', '', '<p>1111111111</p>', 0, 0, 0, 0, '2021-09-27 21:55:59', '2021-09-28 11:17:59');
INSERT INTO `course_lesson` VALUES (2, 1, 2, '测试2', 'image/20210928/7251e174d59cd9569707dfb6e465f618.png', 0, 'https://img.cdn.aliyun.dcloud.net.cn/guide/uniapp/%E7%AC%AC1%E8%AE%B2%EF%BC%88uni-app%E4%BA%A7%E5%93%81%E4%BB%8B%E7%BB%8D%EF%BC%89-%20DCloud%E5%AE%98%E6%96%B9%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B@20200317.mp4', '', '<p>111111</p>', 0, 0, 0, 0, '2021-09-28 10:47:48', '2021-09-28 15:03:46');

-- ----------------------------
-- Table structure for course_order
-- ----------------------------
DROP TABLE IF EXISTS `course_order`;
CREATE TABLE `course_order`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(0) NOT NULL DEFAULT 0 COMMENT '用户id',
  `course_id` int(0) NOT NULL COMMENT '课程id',
  `course_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `pay_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pay_time` datetime(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course_order
-- ----------------------------
INSERT INTO `course_order` VALUES (1, NULL, 1, 1, NULL, 10.00, NULL, NULL, '2021-10-20 01:15:25', '2021-10-20 01:15:25');
INSERT INTO `course_order` VALUES (2, NULL, 1, 2, NULL, 10.00, NULL, NULL, '2021-10-20 02:25:24', '2021-10-20 02:25:24');

SET FOREIGN_KEY_CHECKS = 1;
