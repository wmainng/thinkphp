<?php

namespace app\course\model;

use helper\Arr;
use think\model;

class CourseCategory extends Model
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';

    // 一对一获取所属模型
    public function course()
    {
        return $this->belongsTo('Course','course_id');
    }

    // 查询范围
    public function scopeField($query)
    {
        $query->where('parent_id', 0)->where('status', 1)->field('id,name,icon');
    }

    /**
     * 获取分类
     * @return array
     */
    public static function categories()
    {
        $where[] = ['status', '=', 1];
        $data = self::field('id,parent_id,name')->where($where)->select()->toArray();
        return Arr::tree($data);
    }
}