<?php

namespace app\course\model;

use app\common\model\User;
use think\model;

class CourseOrder extends Model
{
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function course()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }
}