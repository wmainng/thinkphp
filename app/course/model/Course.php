<?php


namespace app\course\model;


use app\common\model\Base;
use app\common\model\User;

class Course extends Base
{
    protected $autoWriteTimestamp = 'datetime';

//    protected $schema = [
//        'id'          => 'int',
//        'name'        => 'string',
//        'status'      => 'int',
//        'score'       => 'float',
//        'create_time' => 'datetime',
//        'update_time' => 'datetime',
//    ];

    // 一对一
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function category()
    {
        return $this->hasOne(CourseCategory::class, 'id','category_id');
    }

    public function session()
    {
        return $this->hasMany(CourseLesson::class, 'course_id', 'id');
    }
}