<?php

namespace app\course\model;

use app\common\model\Base;

class CourseLesson extends Base
{
    protected $autoWriteTimestamp = 'datetime';

    protected $schema = [
        'id'          => 'int',
        'user_id'     => 'int',
        'course_id'   => 'int',
        'title'       => 'string',
        'thumb'       => 'string',
        'type'        => 'int',
        'video_url'   => 'string',
        'video_time'  => 'string',
        'content'     => 'string',
        'is_free'     => 'int',
        'free_time'   => 'int',
        'sort'        => 'int',
        'status'      => 'int',
        'create_time' => 'datetime',
        'update_time' => 'datetime',
    ];
}