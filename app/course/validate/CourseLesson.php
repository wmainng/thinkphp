<?php

namespace app\course\validate;

use think\Validate;

class CourseLesson extends Validate
{
    protected $rule = [
        'title'      => 'require',
        'thumb'      => 'string',
        'type'       => 'integer',
        'video_url'  => 'requireWith:type|url',
        'video_time' => 'requireWith:type|string',
        'content'    => 'string',
        'is_free'    => 'integer',
        'free_time'  => 'dateFormat:H:i'
    ];

    protected $message = [
        'title.require' => '章节名称必须',
    ];
}