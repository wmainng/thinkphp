<?php

namespace app\course\validate;

use think\Validate;

class CourseCategory extends Validate
{
    protected $rule = [
        'name|名称' => [
            'require' => 'require',
            'max'     => '20',
        ]
    ];
}