<?php

namespace app\course\validate;

use think\Validate;

class Course extends Validate
{
    protected $rule = [
        'category_id' => 'require|integer',
        'title'       => 'require',
        'thumb'       => 'require',
        'price'       => 'require',
    ];

    protected $message = [
        'category_id.require' => '课程分类必须',
        'title.require'       => '课程名称必须',
        'thumb.require'       => '课程封面必须',
        'price.require'       => '课程价格必须',
    ];
}