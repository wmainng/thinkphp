<?php

namespace app\common\middleware;

use helper\Auth;

class User
{
    public function handle($request, \Closure $next)
    {
        $auth = new Auth();
        $user = $auth->user();

        // 验证登录状态
        if (!$user) {
            return redirect('/login');
        }

        // 传递数据
        $request->user = $user;
        return $next($request);
    }
}
