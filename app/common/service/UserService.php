<?php

namespace app\common\service;

use app\common\model\Config;
use app\common\model\User;
use helper\ali\Ocr;

class UserService
{
    /**
     * 实名认证
     * @param array $post
     * @return bool
     */
    public static function authentication(array $post): bool
    {
        if (!empty($post['id_card_image'])) {
            $config = Config::config('ocr');
            if (!empty($config['id_card_switch'])) {
                $ocr = new Ocr($config);
                $response = $ocr->idCard($post['id_card_image'][0]['image']);
                if ($response['code']) {
                    return false;
                }
                $post['name']     = $response['data']['name'];
                $post['sex']      = $response['data']['sex'] == '男' ? 1 : 2;
                $post['id_card']  = $response['data']['num'];
                $post['birthday'] = $response['data']['birth'];
            }
        }
        $user = User::update($post);
        return (bool)$user;
    }
}