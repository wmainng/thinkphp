<?php

namespace app\common\validate;

use think\Validate;

class NavMenu extends Validate
{
    protected $rule = [
        'name' => 'require',
        'url'  => 'require',
        'icon' => 'require',
    ];

    protected $message = [
        'name.require' => '名称必须',
        'url.require'  => '地址必须',
        'icon.require' => '图标必须',
    ];
}