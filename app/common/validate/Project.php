<?php

namespace app\common\validate;

use think\Validate;

class Project extends Validate
{
    protected $rule = [
        'name'    => 'require',
        'user_id' => 'require|number',
    ];

    protected $message = [
        'name.require'    => '项目名称必须',
        'user_id.require' => '项目负责人必须',
        'user_id.number'  => '项目负责人必须是数字',
    ];
}