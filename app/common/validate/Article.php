<?php
/**
 * +----------------------------------------------------------------------
 * | 文章管理验证器
 * +----------------------------------------------------------------------
 */
namespace app\common\validate;

use think\Validate;

class Article extends Validate
{
    protected $rule = [
        'title|标题'  => 'require',
        'content|内容'  => 'require',
        'captcha|验证码' => 'require|captcha'
    ];
}