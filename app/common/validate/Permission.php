<?php


namespace app\common\validate;


use think\Validate;

class Permission extends Validate
{
    protected $rule = [
        'name'  => 'require',
        'rule'  => 'require',
        'route' => 'require',
    ];

    protected $message = [
        'name.require'  => '名称必须',
        'rule.require'  => '规则必须',
        'route.require' => '路由必须',
    ];
}