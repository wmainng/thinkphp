<?php

namespace app\common\validate;

use think\Validate;

class UserDevice extends Validate
{
    protected $rule = [
        'device_id'     => 'require',
        'platform_type' => 'require',
    ];

    protected $message = [
        'device_id.require'     => '设备唯一标识必须',
        'platform_type.require' => '设备平台必须',
    ];
}