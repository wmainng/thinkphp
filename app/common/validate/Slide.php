<?php

namespace app\common\validate;

use think\Validate;

class Slide extends Validate
{
    protected $rule = [
        'name|名称' => [
            'require' => 'require',
            'max'     => '255',
        ],
        'description|描述' => [
            'max' => '255'
        ],
        'sort|排序' => [
            'require' => 'require',
            'number'  => 'number',
        ]
    ];
}