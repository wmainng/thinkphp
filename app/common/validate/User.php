<?php
declare (strict_types = 1);

namespace app\common\validate;

use think\Validate;

class User extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
        'email|邮箱账号' => [
            'require' => 'require',
            'min'     => '5',
            'max'     => '100',
            'unique'  => 'users',
        ],
        'mobile|联系电话' => [
            'unique'  => 'users',
        ],
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [];
}
