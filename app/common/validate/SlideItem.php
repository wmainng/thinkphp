<?php

namespace app\common\validate;

use think\Validate;

class SlideItem extends Validate
{
    protected $rule = [
        'slide_id|广告位置' => [
            'require' => 'require',
        ],
        'title|名称' => [
            'require' => 'require',
            'max'     => '255',
        ],
        'description|标题' => [
            'max' => '255',
        ],
        'sort|排序' => [
            'require' => 'require',
            'number'  => 'number',
        ]
    ];
}