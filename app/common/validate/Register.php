<?php


namespace app\common\validate;

use think\Validate;

class Register extends Validate
{
    protected $rule =   [
        'username|昵称'  => 'require|max:25|unique:user',
        'email|邮箱'     => 'email|unique:user',
        'captcha|图形码' => 'require|captcha',
        'code|验证码'    => 'require',
        'password|密码'  => 'require|confirm|length:6,16',
        'accept|条款'    => 'accepted'
    ];
}