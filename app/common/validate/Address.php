<?php


namespace app\common\validate;


use think\Validate;

class Address extends Validate
{
    protected $rule = [
        'consignee'  => 'require',
        'mobile'     => 'require|mobile',
        'province'   => 'require|number',
        'city'       => 'require|number',
        'district'   => 'require|number',
        'street'     => 'number',
        'village'    => 'number',
        'address'    => 'require',
        'is_default' => 'in:0,1'
    ];

    protected $message = [
        'consignee.require' => '姓名必须',
        'mobile.require'    => '手机必须',
        'province.number'   => '省必须是数字',
        'city.number'       => '市必须是数字',
        'district.number'   => '区或县必须是数字',
        'street.number'     => '街道或乡镇必须是数字',
        'village.number'    => '街道或村必须是数字',
        'address.require'   => '详细地址必须',
        'is_default.in'     => '默认必须是0或1',
    ];
}