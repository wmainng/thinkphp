<?php

namespace app\common\validate;

use think\Validate;

class IdCard extends Validate
{
    protected $rule = [
        'card_name' => 'require',
        'card_num'  => 'require|number',
    ];

    protected $message = [
        'card_name.require' => '银行名称必须',
        'card_num.require'  => '银行卡号必须',
        'card_num.number'   => '银行卡号必须是数字',
    ];
}