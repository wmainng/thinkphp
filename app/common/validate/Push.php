<?php

namespace app\common\validate;

use think\Validate;

class Push extends Validate
{
    protected $rule = [
        'role_id|角色' => 'require|integer',
        'title|标题' => 'require',
        'content|内容' => 'require',
    ];
}