<?php


namespace app\common\validate;


use think\Validate;

class Transfer extends Validate
{
    protected $rule = [
        'account' => 'require|checkAccount',
        'name'    => 'require|chs',
        'amount'  => 'require|min:0.1',
        'payment' => 'require|between:1,5',
    ];

    protected $message = [
        'account.require'      => '账号必须',
        'account.checkAccount' => '账号必须邮箱或手机',
        'name.require'         => '姓名必须',
        'name.chs'             => '姓名只能是汉字',
        'amount.require'       => '金额必须',
        'amount.min'           => '金额最低0.1',
        'payment.require'      => '支付方式必须',
        'payment.between'      => '支付方式必须在1~5之间',
    ];

    // 自定义验证规则
    protected function checkAccount($value): bool
    {
        return preg_match("/^1[345789]\d{9}$/ims", $value) || filter_var($value, FILTER_VALIDATE_EMAIL);
    }
}