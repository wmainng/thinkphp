<?php


namespace app\common\validate;


use think\Validate;

class UserRank extends Validate
{
    protected $rule = [
        'name'       => 'require',
        'min_points' => 'require|number',
        'max_points' => 'require|number',
        'discount'   => '>:0|<=:100',
        'sort'       => 'number',
        'status'     => 'number'
    ];

    protected $message = [
        'name.require'       => '名称必须',
        'min_points.require' => '最低必须',
        'max_points.require' => '最高必须',
        'sort.number'        => '排序必须是数字',
        'status.number'      => '状态必须是数字'
    ];
}