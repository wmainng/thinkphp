<?php


namespace app\common\model;


use think\model;

class UserBalanceLog extends Model
{
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    // 搜索器
    public function searchUserIdAttr($query, $value, $data)
    {
        if ($value) {
            $query->where('user_id', '=', $value);
        }
    }

    public function searchCreateTimeAttr($query, $value, $data)
    {
        if ($value) {
            $value = is_string($value) ? explode(',', $value) : $value;
            $query->whereBetweenTime('create_time', $value[0] . ' 00:00:00', $value[1] . ' 23:59:59');
        }
    }

    /**
     * 获取列表
     * @param array $param
     * @return array
     */
    public static function getList(array $param): array
    {
        $field = $param['field'] ?? 'id';
        $order = $param['order'] ?? 'desc';
        $where = [];
        if (isset($param['user_id'])) {
            $where[] = ['user_id', '=', $param['user_id']];
        }
        if (isset($param['out_trade_no'])) {
            $where[] = ['out_trade_no', '=', $param['out_trade_no']];
        }
        if (isset($param['payment'])) {
            $where[] = ['payment', '=', $param['payment']];
        }
        if (isset($param['type'])) {
            $where[] = ['type', '=', $param['type']];
        }
        if (isset($param['status'])) {
            $where[] = ['status', '=', $param['status']];
        }
        $list  = self::where($where)->order($field, $order)->paginate(10);
        if ($list->isEmpty()) {
            return [
                'count' => 0,
                'data'  => [],
            ];
        }
        return [
            'count' => $list->total(),
            'data'  => $list->items()
        ];
    }
}