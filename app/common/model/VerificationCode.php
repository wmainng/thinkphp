<?php

namespace app\common\model;

use helper\facade\ISms;
use think\facade\Db;
use think\model;

class VerificationCode extends Model
{
    /**
     * 设置验证码
     * @param string $account 账号
     * @return array
     */
    public static function setCode(string $account): array
    {
        //$config = Config::getConf('test');
        //$keys   = array_keys($config);
        //if (in_array($mobile, $keys)) {
        //    $this->success($config[$mobile]);
        //}

        $is_test = 123456;
        $code = $is_test ?? rand(100000, 999999);
        self::create([
            'count'       => Db::raw('count+1'),
            'account'     => $account,
            'code'        => $code,
            'send_time'   => time(),
            'expire_time' => time() + 300
        ], [], true);
        if ($is_test) {
            return ['code' => 0, 'message' => '发送成功,验证码:' . $is_test];
        }
        return ISms::factory()->sendSms($account, 'code', ['code' => $code]);
    }

    /**
     * 获取验证码
     * @param string $account 手机号码
     * @return mixed
     */
    public static function getCode(string $account)
    {
        return self::where('account', $account)->where('expire_time', '>=', time())->value('code');
    }
}
