<?php

namespace app\common\model;

class Message extends Base
{
    public function users()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function u()
    {
        return $this->hasOne(User::class, 'id', 'uid');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'from');
    }

    public static function getList($param)
    {
        $field = $param['field'] ?? 'id';
        $order = $param['order'] ?? 'DESC';
        $limit = $param['limit'] ?? 10;
        $where = [];
        if (isset($param['type'])) {
            $where[] = ['type', '=' , $param['type']];
        }
        if (isset($param['status'])) {
            $where[] = ['status', '=' , $param['status']];
        }
        $list  = Message::where($where)->order($field, $order)->paginate($limit);
        $count = $list->total();
        $list  = $list->items();
        return compact('count', 'list');
    }
}
