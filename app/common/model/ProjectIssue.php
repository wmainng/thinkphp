<?php

namespace app\common\model;

class ProjectIssue extends Base
{
    public function project()
    {
        return $this->hasOne(Project::class, 'id', 'project_id')->bind(['name']);
    }
}
