<?php

namespace app\common\model;

use helper\Arr;

class ArticleCategory extends Base
{
    // 一对一获取所属模型
    public function article()
    {
        return $this->belongsTo('Article','article_id');
    }

    /**
     * 获取分类
     * @return array
     */
    public static function categories()
    {
        $where[] = ['status', '=', 1];
        $data = self::field('id,parent_id,name')->where($where)->select()->toArray();
        return Arr::tree($data);
    }
}