<?php

namespace app\common\model;

use think\model;

class Slide extends Model
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';

    // 一对多
    public function blocks()
    {
        return $this->hasMany('SlideItem','slide_id');
    }
}