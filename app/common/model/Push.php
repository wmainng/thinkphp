<?php


namespace app\common\model;

use think\model;

class Push extends Model
{
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}