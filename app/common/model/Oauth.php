<?php

namespace app\common\model;

use helper\ali\Alipay;

class Oauth extends Base
{
    public static function alipay()
    {
        $root_path = app()->getRootPath();
        $config = Payment::config('alipay');
        $config['notify_url'] = app()->request->domain() . '/notify/alipay';
        $config['app_cert_path'] = $root_path . $config['app_cert_path'];
        $config['alipay_cert_path'] = $root_path . $config['alipay_cert_path'];
        $config['root_cert_path'] = $root_path . $config['root_cert_path'];
        return new Alipay($config);
    }

    /**
     * 微信小程序
     * @param array $param
     * @return User
     */
    public static function miniProgram(array $param)
    {
        $oauth = Oauth::where('third_party', 'wechat')
            ->where('union_id',  $param['union_id'])
            ->find();
        if (!$oauth) {
            //用户信息
            $user              = User::create([
                'nickname' => 'weixin' . time(),
            ]);
            $param['user_id']  = $user->id;
            $param['third_party'] = 'wechat';
            Oauth::create($param);
        } else {
            //用户信息
            $user = User::find($oauth->user_id);
            $oauth->save($param);
        }
        return $user;
    }
}