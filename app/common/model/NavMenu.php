<?php

namespace app\common\model;

use think\model;

class NavMenu extends Model
{
    protected $rule = [
        'name' => 'require',
        'url'  => 'require',
        'icon' => 'require',
    ];

    protected $message = [
        'name.require' => '名称',
        'url.require'  => '地址',
        'icon.require' => '图标',
    ];
}