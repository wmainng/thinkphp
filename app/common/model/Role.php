<?php


namespace app\common\model;


class Role extends Base
{
    /**
     * 多对多相对关联
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * 多对多相对关联
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * 给角色添加一个权限
     * @param $permission
     */
    public function givePermissionTo($permission)
    {
        return $this->permissions()->save($permission);
    }

    /**
     * 给角色添加多个权限
     * @param $permissions
     */
    public function syncPermissions($permissions)
    {
        return $this->permissions()->saveAll($permissions);
    }

    /**
     * 角色有无权限
     * @param $permission
     */
    public function hasPermissionTo($permission)
    {
        $permissions = $this->permissions;
    }

    /**
     * 移除权限
     * @param $permission
     * @return int
     */
    public function revokePermissionTo($permission): int
    {
        return $this->permissions()->detach($permission);
    }
}
