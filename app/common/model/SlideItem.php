<?php

namespace app\common\model;

class SlideItem extends Base
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';

    // 一对一
    public function slide()
    {
        return $this->belongsTo('Slide', 'slide_id');
    }
}