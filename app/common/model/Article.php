<?php


namespace app\common\model;

use think\model\concern\SoftDelete;

class Article extends Base
{
    // 软删除
    use SoftDelete;

    // 设置json类型字段
    protected $json = ['more'];

    // 设置JSON数据返回数组
    protected $jsonAssoc = true;

    // 一对一
    public function category()
    {
        return $this->hasOne(ArticleCategory::class, 'id','category_id');
    }

    // 一对一
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * 获取所有针对文章的评论。
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}