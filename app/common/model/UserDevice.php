<?php

namespace app\common\model;

use think\model;

class UserDevice extends Model
{
    /**
     * 添加
     * @param int $user_id
     * @param array $post
     * @param string $version
     * @return UserDevice|bool|Model
     */
    public static function assign(int $user_id, array $post, string $version = 'v1')
    {
        if (!isset($post['device_id'])) {
            return true;
        }
        if ($post['device_id'] == 'null') {
            unset($post['device_id']);
        }
        $post['user_id'] = $user_id;
        $post['version'] = $version;
        return UserDevice::create($post, ['user_id','device_id', 'device_type', 'platform_type', 'version'], true);
    }
}