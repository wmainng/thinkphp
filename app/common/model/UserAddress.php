<?php


namespace app\common\model;


use think\model;

class UserAddress extends Model
{
    protected $json = ['ad_info'];

    protected $jsonAssoc = true;

    public function setAdcodeAttr($value)
    {
        $this->set('ad_info', Region::setAdInfo($value));
        return $value;
    }

    public static function syncDefault($user_id)
    {
        return self::update(['is_default' => 0], ['user_id' => $user_id]);
    }

    public static function getAddress($id, $user_id = 0)
    {
        $where[] = ['id', '=', $id];
        if ($user_id) {
            $where[] = ['user_id', '=', $user_id];
        }
        return self::where($where)
            ->field('consignee,mobile,province,city,district,street,village,address')
            ->find()
            ->toArray();
    }
}