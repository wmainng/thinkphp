<?php


namespace app\common\model;


class Comment extends Base
{
    // 一对多相对关联
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * 获取评论对应的多态模型。
     */
    public function commentable()
    {
        return $this->morphTo('commentable',[
            'book'	=> 'app\common\model\Book',
            'post'	=> 'app\common\model\Article',
            'reply'	=>	'app\model\Forum',
        ]);
    }
}