<?php


namespace app\common\model;


class UserSign extends Base
{
    protected $name = 'user_sign';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}