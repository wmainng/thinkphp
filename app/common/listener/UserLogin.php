<?php
// +----------------------------------------------------------------------
// | 登录日志
// +----------------------------------------------------------------------

namespace app\common\listener;

use app\common\model\User;
use think\facade\Request;

class UserLogin
{
    public function handle($user)
    {
        // 事件监听处理
        User::update(['last_time' => date('Y-m-d H:i:s'), 'last_ip' => Request::ip()], ['id' => $user->id]);
    }
}