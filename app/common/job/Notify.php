<?php
// +----------------------------------------------------------------------
// | 异步通知
// +----------------------------------------------------------------------

namespace app\common\job;

use think\queue\Job;

class Notify
{
    // 1.创建任务
    // job/Job名称
    // 2.发布任务
    // $job:完整类名,app\common\job\Notify; $data: 参数; $queue:队列名
    //think\facade\Queue::push($job, $data = '', $queue = null) 立即执行
    //think\facade\Queue::later($delay, $job, $data = '', $queue = null) 在$delay秒后执行
    // 3.监听任务并执行
    //&> php think queue:listen
    //&> php think queue:work

    public function fire(Job $job, $data)
    {

        // 执行具体的任务
        trace(date('Y-m-d H:i:s'), 'info');

        // 检查这个任务已经重试了几次
        $count = $job->attempts();
        if ($count > 8) {
            //如果任务执行成功后 记得删除任务，不然这个任务会重复执行，直到达到最大重试次数后失败后，执行failed方法
            $job->delete();
            exit();
        }

        // 执行任务


        // 通知的间隔频率: 4m,10m,10m,1h,2h,6h,15h
        $delays = [0, 0, 240, 600, 600, 3600, 7200, 21600, 54000];

        // 重新发布这个任务
        $job->release($delays[$count]); // 延迟时间
    }

    public function failed($data)
    {
        trace(date('Y-m-d H:i:s') . ' error', 'info');
        // ...任务达到最大重试次数后，失败了
    }
}