<?php
// +----------------------------------------------------------------------
// | 定时轮询查单
// +----------------------------------------------------------------------

namespace app\common\job;

use think\queue\Job;

class Transaction
{
    public function fire(Job $job, $data)
    {
        // 执行具体的任务
        trace(date('Y-m-d H:i:s'), 'info');

        // 检查这个任务已经重试了几次
        $count = $job->attempts();
        if ($count > 8) {
            // 微信支付关单接口



            //如果任务执行成功后 记得删除任务，不然这个任务会重复执行，直到达到最大重试次数后失败后，执行failed方法
            $job->delete();
            exit();
        }

        // 微信支付查单接口


        // 通知的间隔频率: 5秒/30秒/1分钟/3分钟/5分钟/10分钟/30分钟
        $delays = [0, 0, 5, 30, 60, 180, 300, 600, 1800];

        // 重新发布这个任务
        $job->release($delays[$count]); // 延迟时间
    }

    public function failed($data)
    {
        trace(date('Y-m-d H:i:s') . ' error', 'info');
    }
}