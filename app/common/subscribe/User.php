<?php
// +----------------------------------------------------------------------
// | 事件订阅
// +----------------------------------------------------------------------

namespace app\common\subscribe;


use think\Event;

class User
{
    public function onUserLogin($user)
    {
        // UserLogin事件响应处理
    }

    public function onUserLogout($user)
    {
        // UserLogout事件响应处理
    }

    // 自定义订阅方式
    public function subscribe(Event $event)
    {
        $event->listen('UserLogin', [$this,'onUserLogin']);
        $event->listen('UserLogout',[$this,'onUserLogout']);
    }
}