<?php
declare (strict_types = 1);
// +----------------------------------------------------------------------
// | 定义事件
// | 使用了观察者模式，提供了解耦应用的更好方式
// +----------------------------------------------------------------------

namespace app\common\event;

use app\common\model\User;

class UserLogin
{
    public User $user;

    // 1) 定义事件
    // 1.1) 定义事件类
    // 1.2) 事件触发 控制器中分发事件 助手函数 event('UserLogin');
    // 2) 事件监听
    // 2.1) 定义事件 方式1:手动注册; 方式2:事件监听类 (推荐)
    // 2.2) 注册事件 方式1:; 方式2:event.php (推荐)
    // 3) 事件订阅 (在一个监听器中监听多个事件)
    // 3.1) 事件订阅者类
    // 3.2) 注册事件订阅 event.php
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
