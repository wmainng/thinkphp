ThinkPHP 6.0
===============
# 宝塔面板
## 环境配置
1. Nginx 1.21.0
2. PHP-7.4
    安装扩展: fileinfo,redis
    禁用函数：putenv,proc_open,proc_get_status
3. MySQL 8.0.24
4. Redis 6.2.5
5. Pure-Ftpd 1.0.49
6. Supervisor管理器 2.2

## 网站设置
1) 网站目录--运行目录--/public
2) 伪静态--thinkphp
3) ssl--配置证书
4) 配置文件
~~~
   location /wss {
       proxy_pass http://域名:2348; #代理到上面的地址去，格式：http://域名:端口号，
       proxy_http_version 1.1;
       proxy_set_header Upgrade $http_upgrade;
       proxy_set_header Connection "Upgrade";
       proxy_set_header X-Real-IP $remote_addr;
       #proxy_connect_timeout 5s; #配置点1
       #proxy_read_timeout 60000s; #配置点2，如果没效，可以考虑这个时间配置长一点
       #proxy_send_timeout 60000s; #配置点3
   }
~~~
## 安全 
放行端口:20,21,22,3306,443,2348

## Workerman
> cd /www/wwwroot/域名
> php think worker:gateway -d

## 守护进程

# 技术架构

# Composer
1. aliyuncs
2. qcloud
3. qiniu
4. baidubce
5. phpmailer
6. swagger
7. Workerman
8. mpdf
9. elasticsearch
10. RabbitMQ

# 第三方API配置
1. 存储
2. 短信
3. 阿里云市场
4. 推送
5. 地图
6. 钉钉
7. 微信
8. 支付宝
9. 易联云

# 核心功能
- 会员管理系统 本站用户，第三分用户
- 权限管理系统 管理员，角色，权限
- 文件存储管理
- 门户管理系统
- 评论管理系统
- 项目管理系统
- 财务管理系统 充值 提现 
- 系统配置管理
- API接口

# 版本更新
本次更新:
- 解决了一些已知问题。

最近更新:
- 更新了若干功能。
